"""
A controller, i.e. the entry point for the DRK file conversion functionality.

The UI created using QtDesigner is generated using the following command:
    pyside-uic.exe ui/main.ui -o ui/gen/main.py

The i18n text .ts files are created using the following command:
    pyside-lupdate.exe controller.pro

The i18n binary .qm files are created using the following command:
    - to generate to the current directory:
        lrelease.exe controller.pro (generated to the same directory)
    - to generate to the custom directory:
        lrelease.exe i18n/pl_PL.ts -qm res/pl_PL.qm
        lrelease.exe i18n/en_US.ts -qm res/en_US.qm

The application is packaged using the following command:
    pyinstaller.exe --noupx -y -n hpz-konwerter -i res/icon.ico
    --add-data "res;res"
    --add-data "config;config"
    --add-data "%PYTHONHOME%/Lib/site-packages/docx/templates/default.docx;./docx/templates"
    controller.py

The Windows installer is built using the installer/windows/setup.iss [InnoSetup] script.
"""
import abc
import argparse
import glob
import os
import sys

from PySide import QtCore, QtGui
from PySide.QtCore import QThread, QCoreApplication
from PySide.QtCore import Signal
from PySide.QtGui import QApplication
from PySide.QtGui import QFileDialog

from ext import docxlauncherfactory
from ext.errorsupport import create_error_entry, ConversionInfo, handle_errors
from ext.win.tweaks import set_console_encoding
from src.config import PathKey, config_value
from src.converter import convert
from src.tools.text_tools import safe_input, safe_print, normalize
from ui.view import View

FILE_EXTENSION = ".drk"  # the input binary file extension
RESOURCE_DIR = "res"  # the directory to store the application resources
ICON_PATH = os.path.join(RESOURCE_DIR, "icon.ico")  # the window icon path
# the directory containing the Qt translations
QT_TRANSLATION_PATH = os.path.join(RESOURCE_DIR, "qt_translations")
ABORT_TIMEOUT = 5000  # the thread termination timeout [milliseconds]
CP = {'pl_PL': 1250}  # the dictionary mapping code pages to the locale identifiers


class Controller(object):
    """
    The abstract application controller.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, test_mode, pandoc_mode):
        """
        The constructor of the controller.
        :param test_mode: determine whether the application will be launched in the test mode
        :param pandoc_mode: an indicator, whether the conversion should use pandoc
        """
        self.test_mode = test_mode
        self.pandoc_mode = pandoc_mode

    @abc.abstractmethod
    def launch(self):
        """
        Launch the application.
        """
        pass


class UiController(Controller):
    """
    The UI application controller.
    Here the main window is initialized, as well the UI is bound with the actions
    and the parser data model.
    """

    def __init__(self, test_mode, pandoc_mode):
        """
        The constructor of the controller.
        The UI is initialized and bound with the actions.
        :param test_mode: determine whether the application will be launched in the test mode
        :param pandoc_mode: an indicator, whether the conversion should use pandoc
        """
        super(UiController, self).__init__(test_mode, pandoc_mode)
        self.view = View(extensions=[FILE_EXTENSION])
        self.button_convert_controller = ConvertButtonController(test_mode=test_mode,
                                                                 pandoc_mode=pandoc_mode)
        self.conversion_in_progress = False  # an indicator, whether the conversion is in progress
        self.docx_launcher = docxlauncherfactory.Factory(self.view).get()

    def launch(self):
        """
        Bind the UI elements with the actions and launch the application.
        """
        self.bind_view_actions()
        self.view.show()

    def bind_view_actions(self):
        """
        Bind the view elements with the actions.
        """
        self.button_convert_controller.conversion_progress.connect(self.conversion_progress)
        self.button_convert_controller.conversion_finished.connect(self.conversion_finished)
        self.view.button_convert.clicked.connect(self.button_convert_clicked)
        self.view.list_widget.files_dropped.connect(self.list_widget_files_dropped)
        self.view.button_list_item_remove.clicked.connect(self.button_remove_clicked)
        self.view.button_list_item_add.clicked.connect(self.button_add_clicked)
        self.view.list_widget.itemSelectionChanged.connect(self.list_widget_selection_changed)
        self.view.list_widget.content_changed.connect(self.list_widget_content_changed)
        self.view.window_closing.connect(self.window_closing)

    def conversion_finished(self, conversion_result):
        """
        Action executed when conversion is finished.
        Progress bar is hidden again, convert button is available again.
        :param conversion_result: the conversion result (a dictionary - file : error)
        """
        self.conversion_in_progress = False
        self.view.progress_bar_convert.setVisible(False)
        self.view.progress_bar_convert.setValue(0)
        if conversion_result.launch_path:
            self.docx_launcher.open(conversion_result.launch_path)
        else:
            handle_errors(conversion_result.errors, self.view)
        self._update_convert_button_state()

    def conversion_progress(self, value):
        """
        Action executed on the conversion progress.
        Update the progress bar with the given value.
        :param value: the progress value to set
        """
        self.view.progress_bar_convert.setValue(int(value))

    def button_convert_clicked(self):
        """
        Action executed when the convert button is clicked.
        The ConvertButtonController convert the input .DRK files to the output files.
        """
        self.conversion_in_progress = True
        self.view.progress_bar_convert.setVisible(True)
        self._update_convert_button_state()
        self.button_convert_controller.set_files(self.view.list_widget.all_texts())
        self.button_convert_controller.start()

    def list_widget_files_dropped(self, file_path_list):
        """
        Action executed when files are dropped onto the list widget.
        Each of the file paths is added to the list widget.
        :param file_path_list: the file path list
        """
        self.view.list_widget.addItems(file_path_list)

    def list_widget_selection_changed(self):
        """
        Action executed when list item selection changed.
        Remove button goes disabled in case no items are selected.
        """
        self._update_remove_button_state()

    def list_widget_content_changed(self):
        """
        Action executed when list items have been changed (added / removed).
        Convert button goes disabled in case the list is empty.
        """
        self._update_convert_button_state()

    def button_remove_clicked(self):
        """
        Action executed when the remove button is clicked.
        """
        for item in self.view.list_widget.selectedItems():
            self.view.list_widget.takeItem(self.view.list_widget.row(item))

    def button_add_clicked(self):
        """
        Action executed when the add button is clicked.
        """
        file_path = QFileDialog.getOpenFileName(self.view,
                                                filter="*%s" % FILE_EXTENSION)[0]
        if os.path.exists(file_path):
            self.view.list_widget.add_items_if_not_in_list(file_path)

    def window_closing(self):
        """
        Action executed when the window is about to close.
        The conversion is interrupted and the UI is waiting until the current iteration is done.
        """
        self.button_convert_controller.abort()
        if not self.button_convert_controller.wait(ABORT_TIMEOUT):
            try:
                self.button_convert_controller.terminate()  # terminate if not aborted in 5 seconds
                self.button_convert_controller.wait()
            finally:  # clean up
                for f_path in glob.glob(u"*.tmp"):
                    os.unlink(f_path)

    def _update_convert_button_state(self):
        """
        Determine whether the convert button is enabled or not and update the button state
        accordingly. The button can be enabled only in case the conversion is not in progress and
        there are any items to convert.
        """
        self.view.button_convert.setEnabled(
            len(self.view.list_widget.all_items()) and not self.conversion_in_progress)

    def _update_remove_button_state(self):
        """
        Determine whether the remove button is enabled or not and update the button state
        accordingly. The button can be enabled only in case any list elements are selected.
        """
        self.view.button_list_item_remove.setEnabled(len(self.view.list_widget.selectedItems()))


class ConvertButtonController(QThread):
    """
    The convert button controller.
    Converts the input data to the output documents and updates the progress bar.
    """

    # This is the signal that will be emitted during the processing.
    # By including int as an argument, it lets the signal know to expect
    # an integer argument when emitting.
    conversion_progress = Signal(int)
    conversion_finished = Signal(ConversionInfo)

    def __init__(self, test_mode, pandoc_mode):
        """
        Call the super constructor to start the thread.
        :param test_mode: determine whether the application will be launched in the test mode
        :param pandoc_mode: an indicator, whether the conversion should use pandoc
        """
        QThread.__init__(self)
        self.files = []
        self.test_mode = test_mode
        self.pandoc_mode = pandoc_mode
        self.is_aborted = False

    def set_files(self, files):
        """
        Set the file paths to convert.
        :param files: the file paths to convert
        """
        self.files = files

    def run(self):
        """
        Execute action associated to the button.
        A QThread is run by calling it's start() function, which calls this run()
        function in it's own "thread".
        """
        errors = {}  # store the conversion errors
        launch_path = ''  # store path of the file to launch in external application

        try:
            f_len = len(self.files)
            result = None
            for index, path in enumerate(self.files):

                # stop the conversion if the thread is aborted
                if self.is_aborted:
                    return

                # convert the file
                result = convert(path, config_value(PathKey.TEMPLATE), self.pandoc_mode,
                                 self.test_mode)
                if result.error:
                    errors[path] = create_error_entry(result.error.info, result.error.traceback)

                # increment progress
                self.conversion_progress.emit(int(100 * (index + 1) / f_len))

            # add launch path to information in case of a single file
            if f_len == 1 and not errors:
                launch_path = result.output

        finally:
            if not self.is_aborted:
                self.conversion_finished.emit(ConversionInfo(errors, launch_path))

    def abort(self):
        """
        Abort the thread.
        """
        self.is_aborted = True


class HeadlessController(Controller):
    """
    The controller of the headless application mode.
    """

    def __init__(self, file_paths, test_mode, pandoc_mode):
        """
        Instantiate the headless controller.
        :param file_paths: the paths of the files to convert
        :param test_mode: an indicator whether the application will be launched in the test mode
        :param pandoc_mode: an indicator, whether the conversion should use pandoc
        """
        super(HeadlessController, self).__init__(test_mode, pandoc_mode)
        self.files = file_paths

    def launch(self):
        """
        Launch the application in headless mode.
        """
        result = {}
        f_num = len(self.files)
        for index, file_path in enumerate(self.files):
            safe_print(QCoreApplication.translate("controller", "headless-process") % file_path)
            result[file_path] = convert(unicode(file_path), config_value(PathKey.TEMPLATE),
                                        self.pandoc_mode,
                                        self.test_mode)
            safe_print("%s: %s%%" % (QCoreApplication.translate("controller", "headless-progress"),
                                     (index + 1) * 100 / f_num))
        errors = dict(
            [(file_path,
              create_error_entry(result[file_path].error.info, result[file_path].error.traceback))
             for file_path in result if result[file_path].error])
        if not errors and f_num == 1:
            docxlauncherfactory.Factory().get().open(result.values()[0].output)
        handle_errors(errors)
        if errors:
            safe_input(QCoreApplication.translate("controller", "headless-press-key"))
            sys.exit()


def translate(input_locale):
    """
    Translate the application and return the translators.
    :return: the translator list
    """
    translator_local = QtCore.QTranslator()
    translator_local.load('%s/%s' % (RESOURCE_DIR, input_locale))
    translator_system = QtCore.QTranslator()
    translator_system.load('qt_%s' % input_locale, QT_TRANSLATION_PATH)
    return translator_local, translator_system


def create_app(translators):
    """
    Create the application and install the given translators.
    :param translators: the translators to install
    :return: the application
    """
    app = QApplication(sys.argv)
    for translator in translators:
        app.installTranslator(translator)
    app.setWindowIcon(QtGui.QIcon(ICON_PATH))
    return app


def get_controller(headless_mode, files, test_mode, pandoc_mode):
    """
    Get the controller according to the application run mode.
    :param headless_mode: True in case the app is run in the headless mode, False otherwise
    :param files: the input file list
    :param test_mode: an indicator whether the application will be launched in the test mode
    :param pandoc_mode: an indicator, whether the conversion should use pandoc
    :return: the applicable controller
    """
    return HeadlessController(files, test_mode, pandoc_mode) if headless_mode else UiController(
        test_mode, pandoc_mode)


def filter_files(files):
    """
    Filter only valid file paths from the given file path list.
    :param files: the input file list
    :return: the list of the valid file paths
    """
    return [f_path for f_path in files if
            os.path.exists(f_path) and os.path.splitext(f_path)[1].lower() == FILE_EXTENSION]


def map_to_full_paths(current_dir, files):
    """
    Map the file paths to full file paths.
    :param current_dir: the current directory (application launch directory)
    :param files: the input file path list
    :return: the full file path list
    """
    return [f_path if os.path.exists(f_path) else os.path.join(current_dir, f_path) for f_path in
            files]


def parse_arguments():
    """
    Wrapper function to parse arguments with the correct unicode data handling.
    :return: the parsed arguments
    """
    app_desc = QCoreApplication.translate("controller", "app-description")
    help_files = QCoreApplication.translate("controller", "app-help-files")
    help_headless = QCoreApplication.translate("controller", "app-help-headless")
    try:
        parser = argparse.ArgumentParser(description=app_desc)
        parser.add_argument('files', type=str, nargs='*', help=help_files)
        parser.add_argument('--headless', dest='headless', action='store_true', help=help_headless)
        return parser.parse_args()
    except UnicodeEncodeError:
        parser = argparse.ArgumentParser(description=normalize(app_desc))
        parser.add_argument('files', type=str, nargs='*', help=normalize(help_files))
        parser.add_argument('--headless', dest='headless', action='store_true',
                            help=normalize(help_headless))
        return parser.parse_args()


def start(input_locale=QtCore.QLocale.system().name(), test_mode=False, pandoc_mode=True):
    """
    Start the app.
    :param input_locale: the locale to install the translations for (by default: the system locale)
    :param test_mode: an indicator whether the application will be launched in the test mode
            (False by default)
    :param pandoc_mode: an indicator, whether the conversion should use pandoc
            (True by default)
    """
    # change the current directory to the app directory
    current_dir = os.path.abspath(os.curdir)
    if sys.argv:
        os.chdir(os.path.dirname(sys.argv[0]) or os.curdir)

    # create the application instance
    translators = translate(input_locale)
    app = create_app(translators)
    args = parse_arguments()

    # launch in headless mode if specified, otherwise launch an empty UI
    files = filter_files(map_to_full_paths(current_dir, args.files))
    controller = get_controller(args.headless, files, test_mode, pandoc_mode)
    controller.launch()
    if not args.headless:
        sys.exit(app.exec_())


if __name__ == '__main__':
    LOCALE = QtCore.QLocale.system().name()
    if sys.platform == 'win32' and LOCALE in CP:
        set_console_encoding(CP[LOCALE])
    start(LOCALE)
