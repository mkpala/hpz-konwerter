# -*- coding: utf-8 -*-
"""
Module to read the configuration file.
"""
import json
import os

from aenum import Enum

CONFIG_DIR = "config"  # the directory to store the application configuration
CONFIG_FILE = os.path.join(CONFIG_DIR, "config.txt")  # the application configuration file


class Measurement(Enum):
    """
    The measurement type enumeration.
    """
    _init_ = "measurement_value measurement_name"

    # measurement types
    MEASUREMENT_ELECTRICAL_LIGHTING = "OŚWIETLENIE ELEKTRYCZNE", "oswietlenie"
    MEASUREMENT_DUST = "PYŁY", "pyly"
    MEASUREMENT_CHEMICALS = "SUBSTANCJE CHEMICZNE", "substancje"
    MEASUREMENT_DUST_CHEMICALS = "PYŁY I SUBSTANCJE CHEMICZNE", "pylysubstancje"
    MEASUREMENT_NOISE = "HAŁAS", "halas"
    MEASUREMENT_VIBRATION = "DRGANIA", "drgania"
    MEASUREMENT_MICROCLIMATE = "MIKROKLIMAT", "mikroklimat"

    def get_name(self):
        """
        Return the measurement name.
        :return: the measurement name
        """
        return self.measurement_name

    def get_value(self):
        """
        Return the measurement value.
        :return: the measurement value
        """
        return self.measurement_value

    @staticmethod
    def from_value(value):
        """
        Return the measurement type(s) based on the given value.
        In case the enumeration value is part of the given value, it is added to the result list.
        :param value: the measurement type value
        :return: the matching measurement items
        """
        items = []
        encoded_value = value.encode('utf8')
        for item in Measurement.__members__.values():
            if item.measurement_value in encoded_value:
                items.append(item)
        return items


class PathKey(Enum):
    """
    The enum to denote those keys, which refer to the file paths.
    """
    TEMPLATE = 'wzor'  # the template file config key
    INTRO = 'wstep'  # the introduction file template config key
    LIGHTING = Measurement.MEASUREMENT_ELECTRICAL_LIGHTING.get_name()
    DUST = Measurement.MEASUREMENT_DUST.get_name()
    CHEMICALS = Measurement.MEASUREMENT_CHEMICALS.get_name()
    NOISE = Measurement.MEASUREMENT_NOISE.get_name()
    VIBRATION = Measurement.MEASUREMENT_VIBRATION.get_name()
    MICROCLIMATE = Measurement.MEASUREMENT_MICROCLIMATE.get_name()

    @staticmethod
    def from_value(value):
        """
        Return the path key based on the given value.
        :param value: the path key
        :return: the path key enumeration item
        """
        encoded_value = value.encode('utf8')
        for item in PathKey.__members__.values():
            if item.value == encoded_value:
                return item


def config_value(key):
    """
    Retrieve the configuration file entry value by the given key.
    If the key is an instance of PathKey, the path relative to the config directory is returned.
    Otherwise the single value is returned.
    :param key: the configuration file entry key
    :return: the configuration entry file value or None if not found
    """
    with open(CONFIG_FILE) as config_json:
        try:
            config_content = json.load(config_json)
            config_key = key.value
            if config_key not in config_content:
                return

            value = config_content[config_key]
            if not isinstance(key, PathKey):
                return value

            path = os.path.join(CONFIG_DIR, value)
            if os.path.exists(path) and os.path.isfile(path):
                return path
        except ValueError:
            pass
