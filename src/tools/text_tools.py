# -*- coding: utf-8 -*-

"""
Module to store the common functions to simplify working with texts,including encoding & decoding.
"""

import unicodedata

POLISH_UPPERCASE_LETTERS = u"ĄĆĘŁŃÓŚŻŹ"  # the polish uppercase characters
POLISH_LOWERCASE_LETTERS = u"ąćęłńóśżź"  # the polish lowercase characters
POLISH_CHARS = POLISH_UPPERCASE_LETTERS + POLISH_LOWERCASE_LETTERS  # the polish characters
NON_POLISH_CHARS = "qvx"  # valid ASCII characters, which are not used in the polish language
SPECIAL_CHARS = u"±"  # special valid characters


def is_ascii(character):
    """
    Determine whether the given character is an ASCII character.
    :param character: the character to check
    :return: True if the character is an ASCII character, False otherwise
    """
    try:
        character.decode('ascii')
        return True
    except (UnicodeDecodeError, UnicodeEncodeError):
        return False


def is_valid_char(character):
    """
    Determine whether the given character is a valid character, i.e.
    whether it is either an ASCII character or a polish character.
    :param character: the character to check
    :return: True if the character is either an ASCII character or a polish character,
            False otherwise
    """
    return is_ascii(character) or character in POLISH_CHARS or character in SPECIAL_CHARS


def unicode_decode(char_sequence, encoding='utf-8'):
    """
    Decode the given string to a list of unicode characters.
    :param char_sequence: the string (character sequence) to decode
    :param encoding: the encoding of the characters (utf-8 by default)
    :return: the list of unicode characters
    """
    return [character for character in unicode(char_sequence, encoding=encoding)]


def unicode_encode(char_list, encoding='utf-8'):
    """
    Encode the given unicode character list to a string.
    :param char_list: the unicode character list
    :param encoding: the target encoding (utf-8 by default)
    :return: the encoded string
    """
    return ''.join(char_list).encode(encoding)


def split(text, split_chars, encoding='utf-8'):
    """
    Split the given string (text) by the given characters (split_chars),
    so that every even-indexed item contains a sequence of valid (ascii or polish)
    characters right after the split_chars, and every odd-indexed item
    contains the characters in between. Example:
    Split_chars: "ő"
    Char_sequence: "Eőśrůaaa"
    Result: [E, śr, aaa]
    :param text: the string to split
    :param split_chars: the characters to split the input string by
    :param encoding: the encoding of the string (by default: utf-8)
    :return: a list of a structure described above
    """

    # split the string by the given split_chars
    split_result = text.split(split_chars)

    # initialize the result
    result = [split_result[0]]

    # iterate over all the groups
    for group in split_result[1:]:

        # extract valid (ascii or polish) characters right after split_chars
        # (before the first space or invalid character)
        chars = unicode_decode(group, encoding)
        index = 0
        valid_chars = []
        while index < len(chars) and chars[index] != ' ' and is_valid_char(chars[index]):
            valid_chars.append(chars[index])
            index += 1
        result.append(unicode_encode(valid_chars))

        # append remaining remaining characters starting from the first valid character
        while index < len(chars) and not is_valid_char(chars[index]):
            index += 1
        result.append(unicode_encode(chars[index:]))

    return result


def starts_with_uppercase(text):
    """
    Determine whether the given text starts with the uppercase character.
    Note that if a character is not a valid character (ascii / polish), True is returned.
    :param text: the text to investigate
    :return: True in case the text starts with the uppercase character, False otherwise
    """
    first_char = unicode_decode(text)[0]

    if first_char in POLISH_CHARS:
        return first_char in POLISH_UPPERCASE_LETTERS

    if is_ascii(first_char):
        return 'Z' >= first_char >= 'A'

    return True


def is_polish_letter(character):
    """
    Determine whether the given character is a polish letter.
    :param character: the character to investigate
    :return: True in case a character is a polish letter, False otherwise
    """
    if character.lower() in NON_POLISH_CHARS:
        return False

    return character in POLISH_CHARS or 'Z' >= character >= 'A' or 'z' >= character >= 'a'


def normalize(string):
    """
    Normalize the given string, i.e. replace all non-ascii characters with ascii equivalents.
    :param string: the unicode string to normalize
    :return: the normalized string
    """
    # characters, which are not supported by unicode normalization
    special_chars = {u'ł': 'l',
                     u'Ł': 'L'}
    return ''.join(
        [_normalize(char) if char not in special_chars else special_chars[char] for char in string])


def _normalize(string):
    """
    Normalize the given string, i.e. replace all non-ascii characters with ascii equivalents.
    Pure unicodedata.normalize is applied, i.e. some characters might not be supported (=skipped).
    :param string: the unicode string to normalize
    :return: the normalized string
    """
    return unicodedata.normalize('NFKD', u'%s' % string).encode('ascii', 'ignore')


def safe_print(message):
    """
    Substitute for built-in print function, to handle unicode encoding issues.
    :param message: message to print
    """
    try:
        print message
    except UnicodeEncodeError:
        print normalize(message)


def safe_input(message):
    """
    Substitute for built-in raw_input function, to handle unicode encoding issues.
    :param message: message to display
    :return: the input variable
    """
    try:
        return raw_input(message)
    except UnicodeEncodeError:
        return raw_input(normalize(message))


def escape_special_chars(text, characters=('*', '-', '~', '`', '_', '<', '>')):
    """
    Escape special characters in the given line.
    :param text: the input text
    :param characters: special characters to escape, by default: * - ~ ` _ < >
    :return: the text with escaped characters
    """
    for character in characters:
        text = text.replace(character, r'\%s' % character)
    return text
