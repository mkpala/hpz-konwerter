"""
Module to store the common functions to simplify working with files.
"""

import os
import stat


def save_file_if(requested, path, content):
    """
    Save the test file if requested or immediately return.
    :param requested: indicator, whether the file is to be saved
    :param path: the target file path
    :param content: the target file contents
    """
    if not requested:
        return

    if os.path.exists(path):
        os.chmod(path, stat.S_IWRITE)
    with open(path, "w") as output_file:
        output_file.write(content)
