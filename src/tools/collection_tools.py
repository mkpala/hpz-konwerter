"""
Module to store the common functions to manipulate collections.
"""

import collections
from itertools import groupby


def namedtuple_with_defaults(typename, field_names, default_values=()):
    """
    Wrapper for collections.namedtuple to enable default value initialization.
    :param typename: the name of the type
    :param field_names: the field names
    :param default_values: the default values of the fields (list for all, dictionary for selected)
    :return: the named tuple with initialized defaults
    """
    namedtuple_cust = collections.namedtuple(typename, field_names)
    namedtuple_cust.__new__.__defaults__ = (None,) * len(namedtuple_cust._fields)
    if isinstance(default_values, collections.Mapping):
        prototype = namedtuple_cust(**default_values)
    else:
        prototype = namedtuple_cust(*default_values)
    namedtuple_cust.__new__.__defaults__ = tuple(prototype)
    return namedtuple_cust


def filter_empty(collection):
    """
    Filter empty elements out of the given collection.
    :param collection: the collection to filter
    :return: the collection without empty elements
    """
    return [elem for elem in collection if len(elem)]


def filter_none(collection):
    """
    Filter None elements out of the given collection.
    :param collection: the collection to filter
    :return: the collection without None elements
    """
    return [elem for elem in collection if elem is not None]


def group_by_predicate(predicate, collection):
    """
    Group the given collection by the given predicate.
    Return a dictionary, where the keys are the group indexes, and the values
    are the actual groups.
    :param predicate: the predicate to group by (a lambda returning True / False)
    :param collection: the collection to group
    :return: the indexed groups
    """
    groups = [list(group) for key, group in
              groupby(enumerate(collection), lambda (x, y): predicate(y)) if key]
    return dict(
        [(group[0][0], [elem[1] for elem in group]) for group in groups])


def list_rstrip(input_list):
    """
    Remove all the right-handside list elements, which are empty strings after applying rstrip().
    :param input_list: the input list
    :return: the right-stripped input list
    """
    i = -1
    list_len = len(input_list)
    while -i <= list_len and isinstance(input_list[i], basestring) and not input_list[i].rstrip():
        i -= 1
    return input_list[:i + 1 or list_len]
