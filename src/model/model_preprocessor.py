# -*- coding: utf-8 -*-

"""
Module to store the model preprocessor, which is responsible for manipulating the model
before it gets serialized.
"""
import re

from src.common.model import TableCell
from src.common.model_iterators import TextContentWalker
from src.common.model_visitor import ModelVisitor

TABLE_REPLACEMENT_RULES = {r"O K O L I C Z N O Ś C I P O M I A R Ó W": r"OKOLICZNOŚCI POMIARÓW"}


class ModelPreprocessor(ModelVisitor):
    """
    Class defining visitor methods to preprocess the model.
    """

    def visit_table_cell(self, table_cell):
        """
        Preprocess the table cell.
        :param table_cell: the table cell to preprocess
        """
        if not isinstance(table_cell, TableCell):
            return

        table_cell.text = self.new_walker(table_cell.text).update(self.__replace_table_text).get()

    def __replace_table_text(self, text):
        """
        Replace the given text using TABLE_REPLACEMENT_RULES.
        :param text: the text to apply replacements on
        :return: the text with applied replacements
        """
        return self.replace_text_based_on_rules(text, TABLE_REPLACEMENT_RULES)

    @staticmethod
    def replace_text_based_on_rules(text, *replacement_rules):
        """
        Replace all matched words from replacement_rules in the given text.
        :param text: the text to modify
        :param replacement_rules: the replacement rules in form of a number of dictionaries,
                where keys denote the character sequences to replace and values
                - their replacements
        :return text with applied replacements
        """
        output_text = text
        for rules in replacement_rules:
            for rule in rules:
                output_text = re.sub(rule, rules[rule], output_text)

        return output_text

    @staticmethod
    def new_walker(text):
        """
        Instantiate new text walker.
        :param text: text to iterate over
        :return: new TextContentWalker instance
        """
        return TextContentWalker(text)
