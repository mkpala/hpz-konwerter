"""
Module to convert the model to the DOCX document.
"""
import os
import stat
import time
from collections import OrderedDict, namedtuple

import pypandoc

from src.common.model import Paragraph, Text, List, TableCell, TableAlignment, TableVAlignment
from src.common.model_parser import ModelParser
from src.model.model_preprocessor import ModelPreprocessor
from src.tools.file_tools import save_file_if
from src.tools.text_tools import normalize, escape_special_chars

LS = os.linesep


def model2docx(model, file_path, template, test_mode):
    """
    Entry point to convert the given model to the DOCX file.
    :param model: the input model
    :param file_path: the input file path (without file extension)
    :param template: the DOCX template file path
    :param test_mode: the indicator whether the app is in the test mode
    :return: output DOCX file path
    """
    # convert from DRK to text
    # convert from model to markdown
    markdown = Model2MdExtendedParser().process_document(model)

    # save markdown in test mode
    save_file_if(test_mode, "%s.md" % file_path, markdown)

    # convert from markdown to docx
    output_docx = normalize("%s.docx" % file_path)
    if os.path.exists(output_docx):
        os.chmod(output_docx, stat.S_IWRITE)
    Md2BinPandocConverter().convert_markdown(markdown, output_docx, template)

    # return output DOCX file path
    return output_docx


class Model2MdParser(ModelParser):
    """
    Class to convert the model to the markdown format.
    """

    HeaderItem = namedtuple('HeaderItem', 'cell separator')

    def parse_header(self, header):
        text = header.text
        if text is None or text == '':
            return ''

        return LS + '%s %s' % (min(6, max(header.level, 1)) * '#', text) + LS

    def parse_paragraph(self, paragraph):
        parsed_text = self.process_text(paragraph.text)
        if not parsed_text:
            return ''

        return parsed_text + (int(paragraph.separate) + 1) * LS

    def parse_block_quote(self, block_quote):
        result = LS.join(
            ['> %s' % line for line in self.process_text(block_quote.text).split(LS)
             if len(line)])
        if not result:
            return ''

        return LS + result + LS

    def parse_text(self, text):
        texts = text.texts
        start_tags = self.__get_text_start_tags(text)
        end_tags = self.__get_text_end_tags(text)
        if isinstance(texts, basestring):
            return start_tags + escape_special_chars(texts) + end_tags

        return start_tags + ''.join(
            [self._process_item(item, self.process_text) for item in texts]) + end_tags

    @staticmethod
    def __get_text_start_tags(text):
        """
        Retrieve the text format opening tags from the given text.
        :param text: the text to retrieve the tags from
        :return: the opening tags to format the text
        """
        decorator = ''
        if text.italics:
            decorator += '_'
        if text.bold:
            decorator += '**'
        if text.strikethrough:
            decorator += '~~'
        if text.code:
            decorator += '`'
        if text.subscript:
            decorator += '<sub>'
        if text.superscript:
            decorator += '<sup>'
        return decorator

    @staticmethod
    def __get_text_end_tags(text):
        """
        Retrieve the text format closing tags from the given text.
        :param text: the text to retrieve the tags from
        :return: the closing tags to format the text
        """
        decorator = ''
        if text.superscript:
            decorator += '</sup>'
        if text.subscript:
            decorator += '</sub>'
        if text.code:
            decorator += '`'
        if text.strikethrough:
            decorator += '~~'
        if text.bold:
            decorator += '**'
        if text.italics:
            decorator += '_'
        return decorator

    def parse_list(self, items):
        return LS + ''.join(items)

    def parse_list_item(self, item, level, ordered):

        # determine decorator based on the ordered attribute
        decorator = '1. ' if ordered else '* '

        # calculate indentation
        indent = 2 * level * ' '

        # for each list item: indent, fill with decorator and label and parse the contents
        return indent + decorator + self.process_text(item.label) + LS + ''.join(
            [self.__parse_list_item_content_elem(elem, level) for elem in item.contents])

    def __parse_list_item_content_elem(self, content_elem, level):
        """
        Helper method to parse the single list item content element.
        :param content_elem: the list item content element to parse
        :param level: the list nesting level
        :return: the parsed list item content element
        """

        # handle inner list - increase nesting level
        if isinstance(content_elem, List):
            return self.process_list(content_elem, level + 1)

        # handle text as an inner paragraph - indent it correctly and ensure trailing spaces
        indent = (2 * level + 3) * ' '
        if isinstance(content_elem, (basestring, Text)):
            return ('  ' + LS).join([indent + line for line in
                                     self.process_text(content_elem).split(LS)])

        # handle paragraph as a separate paragraph - ensure empty lines
        if isinstance(content_elem, Paragraph):
            return LS + LS.join([(indent if line else '') + line for line in
                                 self.process_paragraph(content_elem).split(
                                     LS)]) + LS
        return ''

    def parse_link(self, link):
        text = link.text
        target = link.target
        if not text or not target:
            return ''

        return '[%s][%s]' % (text, target)

    def parse_image(self, image):
        source = image.source
        text = image.text
        if not source or not text:
            return ''

        tooltip = image.tooltip
        target = '(%s "%s")' % (source, tooltip) if tooltip else '(%s)' % source
        return '![%s]%s' % (text, target)

    def parse_code(self, code):
        text = code.text
        if not text:
            return ''

        return '%(break)s```%(lang)s%(break)s%(content)s%(break)s```%(break)s' % {'break': LS,
                                                                                  'lang': code.lang,
                                                                                  'content': text}

    def parse_table(self, header, content):

        # determine the column number based on header length and return in case of no columns
        column_num = len(header)
        if not column_num:
            return ''

        # render the table
        return '%(break)s%(header)s%(break)s%(contents)s%(break)s' % {
            'header': self.__parse_header(column_num, header),
            'break': LS,
            'contents': LS.join(
                [self.__parse_table_row(column_num, row) for row in content])}

    def __parse_header(self, columns, header):
        """
        Parse table header.
        :param header: the header to parse
        :return: string representation of the table header
        """
        header_cells = [item.cell for item in header]
        header_separators = [item.separator for item in header]
        return '%(header)s%(break)s%(separator)s' % {
            'header': self.__parse_table_row(columns, header_cells),
            'break': LS,
            'separator': self.__parse_table_row(columns, header_separators)
        }

    @staticmethod
    def __parse_table_row(columns, row):
        """
        Helper method to parse the single table row.
        :param columns the column number
        :param row: the row to parse
        :return: the parsed row
        """

        # render the cells within a row; if there are too many, ignore redundant cells;
        # if there are not enough cells, append empty cells
        for _inc in range(columns - len(row)):
            row.append('')
        return ' | '.join(row[:columns])

    def parse_table_header_cell(self, cell, alignment):
        return self.HeaderItem(self.__parse_table_cell(cell), self.__get_separator(alignment))

    def parse_table_cell(self, cell, alignment):
        return self.__parse_table_cell(cell)

    def __parse_table_cell(self, cell):
        """
        Helper method to parse the table cell.
        :param cell: the cell to parse
        :return: the parsed cell
        """
        text = cell.text if isinstance(cell, TableCell) else cell
        return self._process_item(text, self.process_text).replace(LS, "<br/>")

    @staticmethod
    def __get_separator(alignment):
        left = ''
        right = ''
        if alignment in (TableAlignment.LEFT, TableAlignment.CENTER):
            left = ':'
        if alignment in (TableAlignment.RIGHT, TableAlignment.CENTER):
            right = ':'
        return '%s---%s' % (left, right)

    def parse_horizontal_rule(self, rule):
        return LS + '---' + LS


class Model2MdExtendedParser(ModelParser):
    """
    Class to convert the model to the markdown format.
    Supports full-blown table conversion, as tables are converted to the pure HTML.
    """

    def __init__(self, parser_delegate=Model2MdParser()):
        """
        Instantiate the parser.
        ModelPreprocessor will be used as a model visitor.
        :param parser_delegate: parser used to execute most of this parser's methods.
        """
        super(Model2MdExtendedParser, self).__init__(ModelPreprocessor())
        self.parser = parser_delegate

    def parse_header(self, header):
        return self.parser.parse_header(header)

    def parse_paragraph(self, paragraph):
        return self.parser.parse_paragraph(paragraph)

    def parse_block_quote(self, block_quote):
        return self.parser.parse_block_quote(block_quote)

    def parse_text(self, text):
        return self.parser.parse_text(text)

    def parse_list(self, items):
        return self.parser.parse_list(items)

    def parse_list_item(self, item, level, ordered):
        return self.parser.parse_list_item(item, level, ordered)

    def parse_link(self, link):
        return self.parser.parse_link(link)

    def parse_image(self, image):
        return self.parser.parse_image(image)

    def parse_code(self, code):
        return self.parser.parse_code(code)

    def __parse_text_to_html(self, text):
        """
        Parse the given text to the pure HTML output.
        :param text: the input text
        :return: the text formatted to HTML
        """
        texts = text if isinstance(text, basestring) \
            else text.texts if isinstance(text, Text) else ''

        if isinstance(texts, basestring):
            texts = escape_special_chars(texts)
        elif isinstance(texts, (list, tuple)):
            texts = ''.join([self.__parse_text_to_html(txt) for txt in texts])
        else:
            texts = ''

        start_tags = self.__get_text_start_tags(text)
        end_tags = self.__get_text_end_tags(text)
        return start_tags + texts.replace(LS, '<br/>') + end_tags

    @staticmethod
    def __get_text_start_tags(text):
        """
        Retrieve the HTML opening tags from the given text.
        :param text: the text to retrieve the tags from
        :return: the opening tags to format the text
        """
        decorator = ''
        if not isinstance(text, Text):
            return decorator

        if text.italics:
            decorator += '<em>'
        if text.bold:
            decorator += '<strong>'
        if text.strikethrough:
            decorator += '<s>'
        if text.code:
            decorator += '<code>'
        if text.subscript:
            decorator += '<sub>'
        if text.superscript:
            decorator += '<sup>'
        return decorator

    @staticmethod
    def __get_text_end_tags(text):
        """
        Retrieve the HTML closing tags from the given text.
        :param text: the text to retrieve the tags from
        :return: the closing tags to format the text
        """
        decorator = ''
        if not isinstance(text, Text):
            return decorator

        if text.superscript:
            decorator += '</sup>'
        if text.subscript:
            decorator += '</sub>'
        if text.code:
            decorator += '</code>'
        if text.strikethrough:
            decorator += '</s>'
        if text.bold:
            decorator += '</strong>'
        if text.italics:
            decorator += '</em>'
        return decorator

    def parse_table(self, header, content):
        contents = ''.join([self.__parse_table_row(row) for row in content])
        if not contents:
            return ''
        return LS + '<table><thead>%(header)s</thead><tbody>%(contents)s</tbody></table><br>' % {
            'header': self.__parse_table_row(header),
            'contents': contents}

    @staticmethod
    def __parse_table_row(row):
        """
        Helper method to parse the single table row.
        :param row: the row to parse
        :param tag: the cell tag
        :param alignment: the table alignment
        :return: the parsed row
        """
        if not row:
            return ''

        return '<tr>' + ''.join(row) + '</tr>'

    def parse_table_header_cell(self, cell, alignment):
        return self.__parse_table_cell(cell, alignment, 'th')

    def parse_table_cell(self, cell, alignment):
        return self.__parse_table_cell(cell, alignment, 'td')

    def __parse_table_cell(self, cell, alignment, tag):
        """
        Helper method to parse the single table cell.
        :param cell: the cell content
        :param alignment: the cell alignment
        :param tag: the HTML tag identifying the cell
        :return: the parsed cell
        """

        # prepare the start tag with modifiers: column span, row span, vertical alignment
        start_tag = "<%s" % tag
        style = None
        if isinstance(cell, TableCell):
            rowspan = cell.rowspan
            colspan = cell.colspan
            if rowspan > 1:
                start_tag += ' rowspan=%s' % rowspan
            if colspan > 1:
                start_tag += ' colspan=%s' % colspan
            style = cell.style
        style = self.__get_cell_style(style, alignment)
        if style:
            start_tag += ' style="%s"' % style
        start_tag += ">"

        # prepare the end tag
        end_tag = "</%s>" % tag

        # render the HTML cell
        return start_tag + self.__parse_text_to_html(cell.text) + end_tag

    @staticmethod
    def __get_cell_alignment(align):
        """
        Helper method to retrieve the HTML cell alignment.
        :param align: the cell alignment
        :return: the cell alignment translated to HTML or None, if no alignment matched
        """
        if align == TableAlignment.CENTER:
            return "center"
        if align == TableAlignment.LEFT:
            return "left"
        if align == TableAlignment.RIGHT:
            return "right"

        return None

    @staticmethod
    def __get_cell_v_align(v_align):
        """
        Helper method to retrieve the HTML vertical cell alignment.
        :param v_align: the vertical cell alignment
        :return: the cell vertical alignment translated to HTML or None, if no alignment matched
        """
        if v_align == TableVAlignment.BASELINE:
            return "baseline"
        if v_align == TableVAlignment.BOTTOM:
            return "bottom"
        if v_align == TableVAlignment.MIDDLE:
            return "middle"
        if v_align == TableVAlignment.TOP:
            return "top"

        return None

    def __get_cell_style(self, cell_style, col_align):
        """
        Serialize the single cell style.
        At the moment horizontal and vertical alignment is supported.
        :param cell_style: the table cell style
        :param col_align: the column alignment
        :return: the serialized style string
        """
        output = OrderedDict()

        # handle horizontal text alignment
        align = self.__get_cell_alignment(cell_style.align)
        if not align:
            align = self.__get_cell_alignment(col_align)
        if align:
            output['text-align'] = align

        # handle vertical text alignment
        valign = self.__get_cell_v_align(cell_style.valign)
        if valign:
            output['vertical-align'] = valign

        # combine all style attributes together
        return ';'.join(['%s:%s' % (item[0], item[1]) for item in output.items()])

    def parse_horizontal_rule(self, rule):
        return self.parser.parse_horizontal_rule(rule)


class Md2BinPandocConverter(object):
    """
    A class responsible for converting the given markdown text to the binary file,
    of which the format is determined based on the file extension of the given output file path.
    Pandoc is responsible for the conversion.
    """

    def __init__(self):
        """
        Instantiate the converter.
        """
        self.input_format = "markdown"  # the input format
        self.intermediate_format = "html"  # the intermediate output format

    def convert_markdown(self, markdown, output_path, template=""):
        """
        Convert markdown to any supported output format, which is determined based on
        the output_path.
        The conversion will be performed in two steps:
        1) Markdown will be converted to the INTERMEDIATE_FORMAT (HTML).
        2) The INTERMEDIATE_FORMAT (HTML) will be converted to the calculated output format.
        This is applied due to the fact that markdown contains HTML snippets.
        :param markdown: the input markdown-formatted string
        :param output_path: the output file path
        :param template: the template document to merge the output document with
        :return: the output document
        """

        # save markdown in a temporary file first - otherwise the generated HTML is erroneous
        tmp_file_name = os.path.join(os.environ.get("TEMP", ""), "%s.tmp" % time.time())
        with open(tmp_file_name, "w") as tmp_file:
            tmp_file.write(markdown)

        # perform conversion
        try:
            # convert markdown to HTML (intermediate format)
            intermediate = pypandoc.convert(tmp_file_name, self.intermediate_format,
                                            format=self.input_format)

            # convert HTML to DOCX (final output format)
            extra_args = []
            if template and os.path.exists(template):
                extra_args.append("--reference-docx=%s" % template)
            return pypandoc.convert_text(intermediate,
                                         os.path.splitext(output_path)[1][1:].lower(),
                                         outputfile=output_path,
                                         format=self.intermediate_format,
                                         extra_args=extra_args)

        finally:
            os.unlink(tmp_file_name)
