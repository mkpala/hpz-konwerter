"""
Model to customize the generated DOCX document (i.e. do all those things, which pandoc cannot).
"""
from datetime import date

from docx import Document
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.enum.text import WD_BREAK
from docxtpl import DocxTemplate

from src.config import Measurement, config_value, PathKey
from src.tools.collection_tools import filter_none

# the docx customizing settings
DOCX_TABLE_STYLE = "Table Grid"  # the table style
DOCX_TABLE_TEXT_STYLE = "Tekst Tabeli"  # the table text style
DOCX_TABLE_ALIGNMENT = WD_TABLE_ALIGNMENT.CENTER  # the table alignment
DOCX_TABLE_HEADER_ALIGNMENT = WD_TABLE_ALIGNMENT.CENTER  # the table header alignment
DOCX_HEADER_STYLE_NAME = "Heading "  # the generic name of the header style name

# the docx template context constants
TMP_DOCUMENT = "document"  # the sub document constant
TMP_IDENTIFIER = "doc_num"  # the file identifier constant
TMP_DATE = "date"  # the document creation date constant
TMP_INTRO = "intro"  # the document introduction constant


def customize_docx(file_path, identifier, template):
    """
    Entry point to customize the DOCX file.
    :param file_path: the DOCX file path
    :param identifier: the DRK file identifier
    :param template: the DOCX template file path
    """
    return DocXProcessor().apply(file_path, template, identifier)


class DocXProcessor(object):
    """
    Class to customize the generated docx.
    """

    def apply(self, file_path, template, identifier):
        """
        Customize the created binary (.docx) file.
        All the tables will be set the style defined in DOCX_TABLE_STYLE
        and the content style defined in DOCX_TABLE_TEXT_STYLE.
        The page breaks will be added before all relevant headers.
        :param file_path: the binary file path
        :param template: the template document file path
        :param identifier: the unique file identifier
        """
        document = Document(file_path)
        self.customize_tables(document)
        self.add_page_breaks(document)
        document = self.apply_template(document, template, identifier)
        document.save(file_path)

    @staticmethod
    def customize_tables(document):
        """
        Customize the tables.
        """
        for table in document.tables:
            table.style = DOCX_TABLE_STYLE
            table.alignment = DOCX_TABLE_ALIGNMENT
            # access to a protected members, as table.rows[int].cells returns an empty list
            for cell in table._cells:
                if cell.paragraphs:
                    cell.paragraphs[0].style = DOCX_TABLE_TEXT_STYLE
                    if not cell._element._tr_idx:
                        cell.paragraphs[0].alignment = DOCX_TABLE_HEADER_ALIGNMENT

    @staticmethod
    def add_page_breaks(document):
        """
        Add page breaks to the document according to the following rules:
        - (not the first) header level 1 always prepend with the page break
        - (not the first) header level 3 always prepend with the page break
        :param document: the document to update
        """
        num_headers_level_three = 0  # number of consecutive headers level 3

        # iterate over all paragraphs
        for index, paragraph in enumerate(document.paragraphs):
            style_name = paragraph.style.name
            if not style_name.startswith(DOCX_HEADER_STYLE_NAME):
                continue

            is_header_level_one = style_name == DOCX_HEADER_STYLE_NAME + "1"
            is_header_level_three = style_name == DOCX_HEADER_STYLE_NAME + "3"

            # add page break in case of header level one or (not the first) header level three
            if index and (is_header_level_one or is_header_level_three and num_headers_level_three):
                document.paragraphs[index - 1].add_run().add_break(WD_BREAK.PAGE)

            # update number of headers level three
            num_headers_level_three = num_headers_level_three + 1 if is_header_level_three else 0

    def apply_template(self, document, template, identifier):
        """
        Merge the given document with the template document.
        :param document: the document to merge with the template
        :param template: the template document file path
        :param identifier: the unique file identifier
        :return the new document
        """

        # create document based on template
        result_document = DocxTemplate(template)

        # render the document and return
        context = {TMP_IDENTIFIER: (identifier or "").decode("utf8"),
                   TMP_DATE: date.today().strftime('%d.%m.%Y'),
                   TMP_INTRO: self.create_subdocument(result_document,
                                                      self.create_introduction(document)),
                   TMP_DOCUMENT: self.create_subdocument(result_document, document)}
        result_document.render(context)
        return result_document

    @staticmethod
    def create_subdocument(main_document, partial_document):
        """
        Attach the partial_document to the main_document using the subdocument functionality.
        :param main_document: the main document
        :param partial_document: the partial document to attach
        :return: the created subdocument or an empty string, if partial document does not exist
        """
        if not partial_document:
            return ''

        sub_doc = main_document.new_subdoc()
        sub_doc.subdocx = partial_document
        return sub_doc

    def create_introduction(self, document):
        """
        Create the introduction document based on the generated document content.
        :param document: the generated document
        :return: the introduction document or an empty string, if no introductions are needed
        """

        # return if no introduction template is found
        intro_path = config_value(PathKey.INTRO)
        if not intro_path:
            return ''

        # collect all headers level three (measurement types)
        headers = self.get_headers_level_three(document)

        # collect all applicable introduction document paths
        paths = self.get_document_paths(headers)

        # return if there are no applicable paths
        if not paths:
            return ''

        # render the introduction template and return
        introduction = DocxTemplate(intro_path)
        context = {PathKey.CHEMICALS.value: '',
                   PathKey.DUST.value: '',
                   PathKey.LIGHTING.value: '',
                   PathKey.MICROCLIMATE.value: '',
                   PathKey.NOISE.value: '',
                   PathKey.VIBRATION.value: ''}
        for path_key in paths:
            context[path_key] = self.create_subdocument(introduction, Document(paths[path_key]))
        introduction.render(context)
        return introduction

    @staticmethod
    def get_headers_level_three(document):
        """
        Retrieve all headers level three from the given document.
        :param document: the document
        :return: the set of all headers level three
        """
        headers = set()
        for paragraph in document.paragraphs:
            style_name = paragraph.style.name
            if style_name != DOCX_HEADER_STYLE_NAME + "3":
                continue
            headers.add(paragraph.text)
        return headers

    @staticmethod
    def get_document_paths(headers):
        """
        Map the given measurement headers to the introduction document paths matched
        in the configuration file.
        :param headers: the headers level three
        :return: the introduction document paths, as dictionary: {PathKey.value: path}
        """
        measurements = set()
        for header in headers:
            measurements.update(Measurement.from_value(header))
        path_keys = filter_none([PathKey.from_value(value.get_name()) for value in measurements])
        paths = {}
        for key in path_keys:
            value = config_value(key)
            if value:
                paths[key.value] = value
        return paths
