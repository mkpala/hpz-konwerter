"""
Module to store the iterators related to the selected model items.
"""
import abc

from src.common.model import Text


class TextIterator(object):
    """
    TextIterator is an iterator over all `Text` object contents. The iterator
    provides all TextItem instances preserving the order of items.
    """

    def __init__(self, text):
        """
        Instantiate the iterator. The TextIterator creates PlainTextIterator instance
        for each nested Text object and collects them in a chronological LIFO queue.
        The next item is then retrieved from the latest available PlainTextIterator.
        :param text: the text instance to create an iterator upon
        """
        self.iterators = [PlainTextIterator(text)]

    def __next__(self):
        """
        Provide the next text item as `TextItem` instance.
        :return: the next item or StopIteration error, if there are no more items
        """

        if not self.iterators:
            raise StopIteration

        iterator = self.iterators[-1]

        next_element = None
        try:
            next_element = next(iterator)
        except StopIteration:
            pass

        if isinstance(next_element, TextItem):
            return next_element

        if isinstance(next_element, PlainTextIterator):
            self.iterators.append(next_element)
            return self.__next__()

        self.iterators.pop()
        return self.__next__()

    def next(self):
        """
        Method providing the next item required by Python 2.x.
        :return: the next item
        """
        return self.__next__()

    def __iter__(self):
        """
        Provide the iterator instance.
        :return: the iterator instance
        """
        return self


class PlainTextIterator(object):
    """
    PlainTextIterator is an iterator over the single level of the `Text` object contents.
    The next item is calculated as follows:
        1. In case text.texts is a string item and the pointer is set at the beginning (zero index),
           it is wrapped in TextItem and returned.
        2. In case text.texts is a string item and the pointer is already incremented,
           StopIteration is raised.
        3. In case text.texts is a collection, the next collection item is investigated:
            a. If it is a string, it is wrapped in TextItem and returned.
            b. If it is a Text instance, a PlainTextIterator instance is created and returned.
            c. If it is any other type, the iteration is continued.
        4. In case text.texts is neither a string nor a collection or the pointer already exceeds
           the collection length, StopIteration is raised.
    """

    def __init__(self, text):
        """
        Instantiate PlainTextIterator.
        The pointer is the current iteration index, which is either an index pointing
        to an element of text.texts collection, or an indicator denoting the end of the iteration
        if text.texts is a string.
        :param text: the Text instance to iterate over
        """
        self.text = text if isinstance(text, Text) else Text(str(text))
        self.pointer = 0

    def __next__(self):
        """
        Retrieve the next text item.
        Note that None is never returned - instead, StopIteration is raised.
        Note that the iterator makes an underlying text collection mutable
        (i.e. converts a tuple to a list).
        :return: the next text item
        """

        # not a collection - wrap in TextItem and return
        if isinstance(self.text.texts, basestring):
            if self.pointer:
                raise StopIteration
            self.pointer += 1
            return TextStringItem(self.text)

        # a tuple - replace by a list to make the collection mutable
        if isinstance(self.text.texts, tuple):
            self.text.texts = list(self.text.texts)

        # a list - check each item
        # if a string - wrap in TextItem and return, if Text, return another iterator
        if isinstance(self.text.texts, list):
            for text_item in self.text.texts[self.pointer:]:
                self.pointer += 1
                if isinstance(text_item, basestring):
                    return TextListItem(self.text.texts, self.text.texts.index(text_item))
                if isinstance(text_item, Text):
                    return PlainTextIterator(text_item)

        raise StopIteration

    def next(self):
        """
        Method providing the next item required by Python 2.x.
        :return: the next item
        """
        return self.__next__()

    def __iter__(self):
        """
        Provide the iterator instance.
        :return: the iterator instance
        """
        return self


class TextItem(object):
    """
    The text item is a wrapper for Text instance string contents, to make them easily
    reachable (get()) and modifiable (set(), update()).
    """

    @abc.abstractmethod
    def update(self, updater):
        """
        Update the text instance by assigning the result of calling `updater` on its content.
        :param updater: the updater function, which takes a string as an argument
        """
        pass

    @abc.abstractmethod
    def get(self):
        """
        Retrieve the item value.
        :return: the item value
        """
        pass

    def set(self, value):
        """
        Set the new item value.
        :param value: new value to set
        """
        self.update(lambda x: value)


class TextStringItem(TextItem):
    """
    TextStringItem is an item, which is supposed to hold the Text instance
    directly containing a string.
    Note that a string item is supposed to point to a Text instance containing a string,
    otherwise a ValueError is raised.
    """

    def __init__(self, text):
        """
        Instantiate the item.
        :param text: the text instance
        """
        if not isinstance(text, Text) or not isinstance(text.texts, basestring):
            raise ValueError('Text instance containing a string is required')
        self.text = text

    def update(self, updater):
        if not callable(updater):
            return

        self.text.texts = updater(self.get())

    def get(self):
        return self.text.texts


class TextListItem(TextItem):
    """
    TextListItem is an item, which is supposed to hold an array of the Text instance contents.
    Note that a list item is supposed to point to a string, otherwise ValueError is raised.
    """

    def __init__(self, input_list, index):
        """
        Instantiate the item.
        :param input_list: a list of the Text instance contents
        :param index: an index pointing to an arbitrary text item, which is supposed to be a string
        """
        if index < 0 or index >= len(input_list):
            raise IndexError("List index out of range")

        list_item = input_list[index]
        if not isinstance(list_item, basestring):
            raise ValueError('A string is required')

        self.input_list = input_list
        self.index = index

    def update(self, updater):
        list_item = self.get()
        if not callable(updater):
            return

        self.input_list[self.index] = updater(list_item)

    def get(self):
        return self.input_list[self.index]


class TextContentWalker(object):
    """
    Utility class to iterate over the given Text and perform some operations.
    """

    def __init__(self, text):
        """
        Instantiate TextContentWalker.
        :param text: the Text instance; if anything else is given, it is wrapped in Text
        """
        self.text = text if isinstance(text, Text) else Text(str(text))

    def update(self, updater=None):
        """
        Intermediate operation to update all text items using the given updater.
        :param updater: the updater to apply
        :return: the walker instance, for convenience
        """
        for text_item in TextIterator(self.text):
            text_item.update(updater)
        return self

    def get(self):
        """
        Terminal operation to return an underlying Text instance.
        :return: the underlying Text instance
        """
        return self.text

    def get_as_string(self):
        """
        Terminal operation to return a string representation of an underlying Text instance.
        :return: string representation of the Text
        """
        return "".join([text_item.get() for text_item in TextIterator(self.text)])
