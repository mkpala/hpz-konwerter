"""
Module to store the abstract model, which is the middle tier between the input and
the output data.
"""

import abc

from src.tools.collection_tools import namedtuple_with_defaults

Document = namedtuple_with_defaults('Document', 'parts', {'parts': ()})


class Item(object):
    """
    Abstract item with `accept` method being an entry point for a model visitor.
    """

    @abc.abstractmethod
    def accept(self, visitor):
        """
        Accept the given visitor.
        :param visitor: visitor to accept
        :return: result of accepting a visitor
        """
        pass


class Header(Item):
    """
    Document header.
    A top-level item denoting a header of any level.
    """

    def __init__(self, text='', level=1):
        """
        Instantiate the header.
        Header contains its text and a level usually indicating
        a (sub)section header level. Level 1 describes a top-level header.

        :param text: header text; empty string by default; any given object is converted to string
        :param level: header level; level 1 by default; if a number is not given, it is set to 1
        """
        self.text = str(text)
        self.level = level if isinstance(level, (int, long)) else 1

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the header instance and returns it.
        :param visitor: visitor to accept
        :return: this header instance
        """
        visitor.visit_header(self)
        return self


class Paragraph(Item):
    """
    Document paragraph.
    A top-level item denoting a separate text range.
    """

    def __init__(self, text='', separate=False):
        """
        Instantiate the paragraph.
        Paragraph contains its text and an indicator, whether it should be separated from
        the rest of the document or not.

        :param text: paragraph text; empty string by default;
                string or Text instance is accepted, any other given object is converted to string
        :param separate: indicate, whether a paragraph should be separated from
                the rest of document; False by default
        """
        self.text = text if isinstance(text, Text) else str(text)
        self.separate = separate

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the paragraph instance and returns it.
        :param visitor: visitor to accept
        :return: this paragraph instance
        """
        visitor.visit_paragraph(self)
        return self


class BlockQuote(Item):
    """
    A block quote.
    A top-level item denoting a citation displayed as a separate block.
    """

    def __init__(self, text=''):
        """
        Instantiate the block quote.
        Block quote contains its text content.

        :param text: block quote text; empty string by default;
                string or Text instance is accepted, any other given object is converted to string
        """
        self.text = text if isinstance(text, Text) else str(text)

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the block quote instance and returns it.
        :param visitor: visitor to accept
        :return: this block quote instance
        """
        visitor.visit_block_quote(self)
        return self


class Text(Item):
    """
    A text element.
    Lower-level item holding a collection of lower-level items, including Text itself.
    """

    def __init__(self, texts=(), italics=False, bold=False, strikethrough=False, code=False,
                 subscript=False, superscript=False):
        """
        Instantiate the text item.
        Text contains a collection of items representing its text contents, each formatted
        in a way described by the following boolean parameters: italics, bold, strikethrough,
        code, subscript, superscript.

        :param texts: a collection (tuple) of any objects; an empty tuple by default;
                designated to contain any lower-level items or strings or a single string,
                in case of the lowest-level text element
        :param italics: a boolean parameter indicating, whether the text contents should be
                displayed in italics; False by default
        :param bold: a boolean parameter indicating, whether the text contents should be
                displayed in bold; False by default
        :param strikethrough: a boolean parameter indicating, whether the text contents should be
                displayed in a strikethrough style; False by default
        :param code: a boolean parameter indicating, whether the text contents should be
                displayed in a code (monotyped) style; False by default
        :param subscript: a boolean parameter indicating, whether the text contents should be
                displayed as a subscript; False by default
        :param superscript: a boolean parameter indicating, whether the text contents should be
                displayed as a superscript; False by default
        """
        self.texts = texts
        self.italics = italics
        self.bold = bold
        self.strikethrough = strikethrough
        self.code = code
        self.subscript = subscript
        self.superscript = superscript

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the text instance and returns it.
        :param visitor: visitor to accept
        :return: this text instance
        """
        visitor.visit_text(self)
        return self


class List(Item):
    """
    A list.
    A top- or lower-level item holding a collection of list items.
    """

    def __init__(self, items=(), ordered=False):
        """
        Instantiate the list.
        List contains a collection of ordered or unordered list items.

        :param items: a collection (tuple) of any objects; an empty tuple by default;
                designated to contain list items or strings; in case not a collection is given,
                it is converted to one
        :param ordered: a boolean parameter indicating, whether the list is ordered or not;
                False by default
        """
        self.items = items if isinstance(items, (list, tuple)) else (items,)
        self.ordered = ordered

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the list instance and returns it.
        :param visitor: visitor to accept
        :return: this list instance
        """
        visitor.visit_list(self)
        return self


class ListItem(Item):
    """
    A list item.
    Lower-level element representing a list item.
    """

    def __init__(self, label='', contents=()):
        """
        Instantiate the list item.
        List item consists of a text label and its contents, which may include any item,
        including the list itself.

        :param label: text assigned to a list, being Text or string; empty string by default
        :param contents: a collection (tuple) of any objects; an empty tuple by default;
                designated to contain any items, including a list itself, or strings;
                in case not a collection is given, it is converted to one
        """
        self.label = label if isinstance(label, Text) else str(label)
        self.contents = contents if isinstance(contents, (list, tuple)) else (contents,)

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the list item instance and returns it.
        :param visitor: visitor to accept
        :return: this list item instance
        """
        visitor.visit_list_item(self)
        return self


class Link(Item):
    """
    A link.
    Lower-level element representing a hyperlink.
    """

    def __init__(self, text='', target=''):
        """
        Instantiate the link.
        Link consists of its alias and a hyperlink target.

        :param text: a hyperlink alias; empty string by default;
                any given object is converted to string
        :param target: a hyperlink target; empty string by default;
                any given object is converted to string
        """
        self.text = str(text)
        self.target = str(target)

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the link instance and returns it.
        :param visitor: visitor to accept
        :return: this link instance
        """
        visitor.visit_link(self)
        return self


class Image(Item):
    """
    An image.
    Lower-level element representing an image.
    """

    def __init__(self, source='', text='', tooltip=''):
        """
        Instantiate the image.
        Image consists of its source path, label and a tooltip.

        :param source: an image source path; empty string by default;
                any given object is converted to string
        :param text: an image label; empty string by default;
                any given object is converted to string
        :param tooltip: an image tooltip; empty string by default;
                any given object is converted to string
        """
        self.source = str(source)
        self.text = str(text)
        self.tooltip = str(tooltip)

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the image instance and returns it.
        :param visitor: visitor to accept
        :return: this image instance
        """
        visitor.visit_image(self)
        return self


class Code(Item):
    """
    A code item.
    Any-level element representing a code-style formatted block.
    """

    def __init__(self, text='', lang=''):
        """
        Instantiate the code item.
        Code item consists of its text content and a programming language, in which it is written.

        :param text: the code text content; empty string by default;
                any given object is converted to string
        :param lang: the programming language, in which a code is implemented;
                empty string by default; any given object is converted to string
        """
        self.text = str(text)
        self.lang = str(lang)

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the code instance and returns it.
        :param visitor: visitor to accept
        :return: this code item instance
        """
        visitor.visit_code(self)
        return self


class Table(Item):
    """
    A table.
    A top-level element representing a table.
    """

    def __init__(self, rows=(), alignment=()):
        """
        Instantiate the table.
        Table consists of a collection of rows and a collection of alignments assigned
        to its columns.

        :param rows: the collection (tuple) of table rows; an empty tuple by default;
                designated to contain a collection of table rows, each being a collection of cells;
                in case not a collection of collections is given, it is converted to one
        :param alignment: the collection (tuple) of objects representing alignment of
                the table columns; an empty tuple by default;
                designated to contain a collection of ordered TableAlignment instances
                in the order corresponding to the column order; in case not a collection is given,
                it is converted to one
        """
        self.rows = rows if isinstance(rows, (list, tuple)) else (rows,)
        self.rows = [row if isinstance(row, (list, tuple)) else (row,) for row in self.rows]
        self.alignment = alignment if isinstance(alignment, (list, tuple)) else (alignment,)

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the table instance and returns it.
        :param visitor: visitor to accept
        :return: this table instance
        """
        visitor.visit_table(self)
        return self


class TableCell(Item):
    """
    A table cell.
    A low-level element representing a single table cell.
    """

    def __init__(self, text='', style=None, rowspan=1, colspan=1):
        """
        Instantiate the table cell.
        Table cell contains its textual content and an information about its style
        and span across a number of columns and rows.

        :param text: the table cell text content; empty string by default;
                any given object but Text is converted to string
        :param style: the table cell style; if any other object but TableCellStyle is provided,
                a default TableCellStyle is provided (TableAlignment.NONE and TableVAlignment.NONE)
        :param rowspan: table cell span across table rows; 1 by default;
                if a number is not given, it is set to 1
        :param colspan: table cell span across table columns; 1 by default;
                if a number is not given, it is set to 1
        """
        self.text = text if isinstance(text, Text) else str(text)
        self.style = style if isinstance(style, TableCellStyle) else TableCellStyle(
            TableAlignment.NONE, TableVAlignment.NONE)
        self.rowspan = rowspan if isinstance(rowspan, (int, long)) else 1
        self.colspan = colspan if isinstance(colspan, (int, long)) else 1

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the table cell instance and returns it.
        :param visitor: visitor to accept
        :return: this table cell instance
        """
        visitor.visit_table_cell(self)
        return self


class HorizontalRule(Item):
    """
    A horizontal rule.
    A top-level element representing a horizontal separator.
    """

    def accept(self, visitor):
        """
        Accept visitor, which preprocesses the horizontal rule instance and returns it.
        :param visitor: visitor to accept
        :return: this horizontal rule instance
        """
        visitor.visit_horizontal_rule(self)
        return self


TableCellStyle = namedtuple_with_defaults('TableCellStyle', 'align valign')


class TableAlignment(object):
    """
    The table alignment enum.
    """
    NONE, LEFT, RIGHT, CENTER = range(4)


class TableVAlignment(object):
    """
    The table vertical alignment enum.
    """
    NONE, TOP, MIDDLE, BOTTOM, BASELINE = range(5)
