"""
Module to read the input binary file and provide the byte and string contents of the input file.
"""

import abc
import struct


class BinaryReader(object):
    """
    Base class to read any binary file. Please subclass to support the specific binary format.
    """
    __metaclass__ = abc.ABCMeta

    TYPE_NAMES = {
        'int8': 'b',
        'uint8': 'B',
        'int16': 'h',
        'uint16': 'H',
        'int32': 'i',
        'uint32': 'I',
        'int64': 'q',
        'uint64': 'Q',
        'float': 'f',
        'double': 'd',
        'char': 's'
    }

    def __init__(self, file_path, start_type_names=(), type_name='uint8'):
        """
        The constuctor. The byte_array (byte contents of the given file) will be initialized here.
        :param file_path: the input file path
        :param start_type_names: the bytes to read upfront, in form of a collection (tuple)
         of types, for which the byte chunk will be extracted from the input file
        :param type_name: the type to determine how the remaining bytes will be read from the file
        """
        self.file = open(file_path, 'rb')
        self.bytes = self.__read(start_type_names, type_name)

    def __del__(self):
        """
        The destructor.
        """
        self.file.close()

    @property
    def byte_array(self):
        """
        The getter of the byte_array property.
        :return: the byte_array property
        """
        return self.bytes

    @byte_array.setter
    def byte_array(self, value):
        """
        The setter of the byte_array property.
        :param value: the byte array to set
        """
        self.bytes = value

    def __read_bytes(self, type_name):
        """
        Private method to read a byte chunk from the input file, based on the given type name.
        :param type_name: the type name to determine how and which bytes will be extracted
        :return: the byte chunk
        """
        type_format = BinaryReader.TYPE_NAMES[type_name.lower()]
        type_size = struct.calcsize(type_format)
        value = self.file.read(type_size)
        if type_size != len(value):
            return None
        return struct.unpack(type_format, value)[0]

    def __read(self, start_type_names, type_name):
        """
        Private method to read the whole contents of the file.
        :param start_type_names:
        :param type_name:
        :return:
        """
        result = []
        for start_type_name in start_type_names:
            result.append(self.__read_bytes(start_type_name))
        while True:
            byte_repr = self.__read_bytes(type_name)
            if byte_repr is None:
                break
            result.append(byte_repr)
        return result

    def as_string(self):
        """
        Provide the string representation of the file contents.
        :return: the string representation of the binary file contents
        """
        return ''.join(map(self.byte_to_char, self.bytes))

    def as_byte_to_char_map(self):
        """
        Provide representation of the file contents in form of tuples: (decimal_byte_representation,
         string_byte_representation).
        :return: the mapped representation of the binary file contents
        """
        return map(lambda num: (num, self.byte_to_char(num)), self.bytes)

    @abc.abstractmethod
    def byte_to_char(self, byte):
        """
        The abstract method to provide the string representation of a byte.
        :param byte: the byte to convert
        :return: the string representation of the given byte
        """
        return
