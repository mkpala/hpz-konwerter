"""
Module to store the abstract model visitor, which is responsible for processing the model.
"""


class ModelVisitor(object):
    """
    Base class defining visitor methods to access the model.
    By default, none of the visit methods manipulates the model.
    """

    def visit_header(self, header):
        """
        Visit the header.
        :param header: the header
        """
        pass

    def visit_paragraph(self, paragraph):
        """
        Visit the paragraph.
        :param paragraph: the paragraph
        """
        pass

    def visit_block_quote(self, block_quote):
        """
        Visit the block quote.
        :param block_quote: the block quote
        """
        pass

    def visit_text(self, text):
        """
        Visit the text.
        :param text: the text
        """
        pass

    def visit_list(self, list_obj):
        """
        Visit the list.
        :param list_obj: the list
        """
        pass

    def visit_list_item(self, list_item):
        """
        Visit the list item.
        :param list_item: the list item
        """
        pass

    def visit_link(self, link):
        """
        Visit the link.
        :param link: the link
        """
        pass

    def visit_image(self, image):
        """
        Visit the image.
        :param image: the image
        """
        pass

    def visit_code(self, code):
        """
        Visit the code.
        :param code: the code
        """
        pass

    def visit_table(self, table):
        """
        Visit the table.
        :param table: the table
        """
        pass

    def visit_table_cell(self, table_cell):
        """
        Visit the table cell.
        :param table_cell: the table cell
        """
        pass

    def visit_horizontal_rule(self, horizontal_rule):
        """
        Visit the horizontal rule.
        :param horizontal_rule: the horizontal rule
        """
        pass
