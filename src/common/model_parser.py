"""
Module to store the abstract model parser, which is responsible for parsing the model to
any kind of textual output.
"""

import abc

from src.common.model import Header, Paragraph, BlockQuote, Text, List, Link, Image, Code, Table, \
    HorizontalRule, Document, TableCell, ListItem
from src.common.model_visitor import ModelVisitor


class ModelParser(object):
    """
    The base class for all the model parsers. Please subclass to support the specific output format.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, visitor=ModelVisitor()):
        """
        Instantiate the parser.
        :param visitor: a model visitor instance; by default, an instance of ModelVisitor
        """

        self.parser_dict = {
            Header: self.process_header,
            Paragraph: self.process_paragraph,
            BlockQuote: self.process_block_quote,
            Text: self.process_text,
            List: self.process_list,
            Link: self.process_link,
            Image: self.process_image,
            Code: self.process_code,
            Table: self.process_table,
            HorizontalRule: self.process_horizontal_rule
        }
        self.visitor = visitor

    def process_document(self, document):
        """
        The entry point of the parser. Converts the input model to an arbitrary text format.
        :return: the text representation of the model
        """
        if not isinstance(document, Document):
            return ''

        return ''.join([self._process_item(part) for part in document.parts
                        if type(part) in self.parser_dict])

    def _process_item(self, item, process_method_fallback=None):
        """
        Helper method to process the single item.
        In order to process an item, it searches self.parser_dict for the `process_` method
        corresponding to the type of an item, and, if not found,
        tries to invoke process_method_fallback. In case of no success, an empty string is returned.
        :param item: the item to process
        :param process_method_fallback: a fallback method in case self.parser_dict
                does not contain a `process_` method corresponding to the type of an item
        :return: a result of calling the corresponding `process_` method
        """
        process_method = self.parser_dict[type(item)] if type(
            item) in self.parser_dict else process_method_fallback
        return process_method(item) if process_method else ''

    def __parse_item(self, item, parse_method, *args):
        """
        Helper method to apply the given `parse_method` on an `item`.
        :param item: the item to parse
        :param parse_method: the method to apply
        :param *args: method arguments
        :return: a result of applying `parse_method` on an `item`
        """
        return parse_method(item.accept(self.visitor), *args)

    def process_header(self, header):
        """
        Process the header.
        In case the header is not an instance of `Header`, an empty string is returned.
        :param header: the header
        :return: the string representation of the header in an arbitrary format
        """
        if not isinstance(header, Header):
            return ''

        return self.__parse_item(header, self.parse_header)

    @abc.abstractmethod
    def parse_header(self, header):
        """
        Parse the given Header instance.
        :param header: the header
        :return: the string representation of the header in an arbitrary format
        """
        pass

    def process_paragraph(self, paragraph):
        """
        Process the paragraph.
        In case the paragraph is not an instance of `Paragraph`, an empty string is returned.
        :param paragraph: the paragraph
        :return: the string representation of the paragraph in an arbitrary format
        """
        if not isinstance(paragraph, Paragraph):
            return ''

        return self.__parse_item(paragraph, self.parse_paragraph)

    @abc.abstractmethod
    def parse_paragraph(self, paragraph):
        """
        Parse the given Paragraph instance.
        :param paragraph: the paragraph
        :return: the string representation of the paragraph in an arbitrary format
        """
        pass

    def process_block_quote(self, block_quote):
        """
        Process the block quote.
        In case the block_quote is not an instance of `BlockQuote`, an empty string is returned.
        :param block_quote: the block quote
        :return: the string representation of the block quote in an arbitrary format
        """
        if not isinstance(block_quote, BlockQuote):
            return ''

        return self.__parse_item(block_quote, self.parse_block_quote)

    @abc.abstractmethod
    def parse_block_quote(self, block_quote):
        """
        Parse the given BlockQuote instance.
        :param block_quote: the block quote
        :return: the string representation of the block quote in an arbitrary format
        """
        pass

    def process_text(self, text):
        """
        Process the text.
        For convenience, in case the text is a string, it is automatically converted to Text.
        In case the text is neither an instance of `Text` nor a string, an empty string is returned.
        :param text: the text
        :return: the string representation of the texts in an arbitrary format
        """
        if not isinstance(text, (Text, basestring)):
            return ''

        text_to_parse = text if isinstance(text, Text) else Text(text)
        return self.__parse_item(text_to_parse, self.parse_text)

    @abc.abstractmethod
    def parse_text(self, text):
        """
        Parse the given Text instance.
        :param text: the text
        :return: the string representation of the texts in an arbitrary format
        """
        pass

    def process_list(self, list_obj, level=0):
        """
        Process the list:
            1. Apply visitor on the list.
            2. Retrieve list items and apply process_list_item on each item.
            3. Apply parse_list on a list of processed list items.
        In case the list_obj is not an instance of `List`, an empty string is returned.
        :param list_obj: the list to process
        :param level: the list level; level 0 denotes the top-level list, any bigger number
                stands for a nested list
        :return: the string representation of the list in an arbitrary format
        """
        if not isinstance(list_obj, List):
            return ''

        list_to_parse = list_obj.accept(self.visitor)
        return self.parse_list(
            [self.process_list_item(item, level, list_to_parse.ordered) for item in
             list_to_parse.items])

    @abc.abstractmethod
    def parse_list(self, items):
        """
        Parse the given list of items, each being a result of
        calling parse_list_item on the initial list item.
        :param items: the list of strings
        :return: the string representation of the list items in an arbitrary format
        """
        pass

    def process_list_item(self, list_item, level, ordered):
        """
        Process the list item.
        For convenience, in case the list_item is a string,
        it is automatically converted to ListItem.
        In case the list_item is neither an instance of `List` nor a string,
        an empty string is returned.
        :param list_item: the list item to parse
        :param level: a parameter indicating how deep is the list item nested;
                0 stands for a top-level item
        :param ordered: a parameter indicating, whether a list is ordered or not
        :return: the string representation of the list item in an arbitrary format
        """
        if not isinstance(list_item, (ListItem, basestring)):
            return ''

        list_item_to_parse = list_item if isinstance(list_item, ListItem) else ListItem(list_item)
        return self.__parse_item(list_item_to_parse, self.parse_list_item, level, ordered)

    @abc.abstractmethod
    def parse_list_item(self, item, level, ordered):
        """
        Parse the given ListItem instance.
        :param item: the list item to parse
        :param level: a parameter indicating how deep is the list item nested;
                0 stands for a top-level item
        :param ordered: a parameter indicating, whether a list is ordered or not
        :return: the string representation of an item in an arbitrary format
        """
        pass

    def process_link(self, link):
        """
        Process the link.
        In case the link is not an instance of `Link`, an empty string is returned.
        :param link: the link
        :return: the string representation of the link in an arbitrary format
        """
        if not isinstance(link, Link):
            return ''

        return self.__parse_item(link, self.parse_link)

    @abc.abstractmethod
    def parse_link(self, link):
        """
        Parse the given Link instance.
        :param link: the link
        :return: the string representation of the link in an arbitrary format
        """
        pass

    def process_image(self, image):
        """
        Process the image.
        In case the image is not an instance of `Image`, an empty string is returned.
        :param image: the image
        :return: the string representation of the image in an arbitrary format
        """
        if not isinstance(image, Image):
            return ''

        return self.__parse_item(image, self.parse_image)

    @abc.abstractmethod
    def parse_image(self, image):
        """
        Parse the given Image instance.
        :param image: the image
        :return: the string representation of the image in an arbitrary format
        """
        pass

    def process_code(self, code):
        """
        Process the code.
        In case the code is not an instance of `Code`, an empty string is returned.
        :param code: the code
        :return: the string representation of the code in an arbitrary format
        """
        if not isinstance(code, Code):
            return ''

        return self.__parse_item(code, self.parse_code)

    @abc.abstractmethod
    def parse_code(self, code):
        """
        Parse the given Code instance.
        :param code: the code
        :return: the string representation of the code in an arbitrary format
        """
        pass

    def process_table(self, table_to_process):
        """
        Process the table:
            1. Apply visitor on the table.
            2. Check, if at least two rows are given. If not, return an empty string.
            3. Retrieve the header and apply process_table_cell on each header cell.
            4. Retrieve the remaining rows and apply process_table_cell on each cell of each row.
            5. Apply parse_table on processed header and rows.
        In case the table_to_process is not an instance of `Table`, an empty string is returned.
        :param table_to_process: the table
        :return: the string representation of the table in an arbitrary format
        """

        def __get_row_alignment(align, columns):
            """
            Helper method to fill the given table alignment collection with None instances
            until the given column count is reached.
            :param align: a collection of column alignments
            :param columns: a number of columns in the table
            :return: a collection of column alignments filled with Nones to reach number of columns
            """
            return [align[column] if column < len(align) else None for column in range(columns)]

        def __process(row_to_process, table_alignment, parse_method):
            """
            Helper method to process a single row, by calling process_table_cell on its cells.
            :param row_to_process: the row to process
            :param table_alignment: the table alignment
            :param parse_method: method, which will be applied on each cell to parse it
            :return: table row, which now contains processed cells
            """
            row_alignment = __get_row_alignment(table_alignment, len(row_to_process))
            return [self.process_table_cell(cell, row_alignment[index], parse_method) for
                    index, cell in enumerate(row_to_process)]

        if not isinstance(table_to_process, Table):
            return ''
        if len(table_to_process.rows) < 2:
            return ''

        table = table_to_process.accept(self.visitor)
        rows = table.rows
        header = __process(rows[0], table.alignment, self.parse_table_header_cell)
        content = [__process(row, table.alignment, self.parse_table_cell) for row in rows[1:]]
        return self.parse_table(header, content)

    @abc.abstractmethod
    def parse_table(self, header, content):
        """
        Parse the given table given in form of its header and content.
        :param header: the collection of processed header cells
        :param content: the collection of table rows, each being a collection of processed cells
        :return: the string representation of the table in an arbitrary format
        """
        pass

    def process_table_cell(self, cell, alignment, parse_method):
        """
        Process the table cell.
        For convenience, in case the cell is a string or a `Text` instance,
        it is automatically converted to TableCell.
        In case the cell is neither an instance of `TableCell` nor a string or `Text`,
        an empty string is returned.
        :param cell: the table cell to process
        :param alignment: the table cell alignment
        :param parse_method: method to parse the cell
        :return: the string representation of the table cell in an arbitrary format
        """
        cell = cell if isinstance(cell, TableCell) else TableCell(
            cell if isinstance(cell, (Text, basestring)) else '')

        return self.__parse_item(cell, parse_method, alignment)

    @abc.abstractmethod
    def parse_table_header_cell(self, cell, alignment):
        """
        Parse the given TableCell instance, which is contained within the table header.
        :param cell: the table header cell
        :param alignment: the cell alignment
        :return: the string representation of the table cell in an arbitrary format
        """
        pass

    @abc.abstractmethod
    def parse_table_cell(self, cell, alignment):
        """
        Parse the given TableCell instance, which is contained within the table content.
        :param cell: the table content cell
        :param alignment: the cell alignment
        :return: the string representation of the table cell in an arbitrary format
        """
        pass

    def process_horizontal_rule(self, horizontal_rule):
        """
        Process the horizontal rule.
        In case the horizontal_rule is not an instance of `HorizontalRule`,
        an empty string is returned.
        :param horizontal_rule: the horizontal rule
        :return: the string representation of the horizontal rule in an arbitrary format
        """
        if not isinstance(horizontal_rule, HorizontalRule):
            return ''

        return self.__parse_item(horizontal_rule, self.parse_horizontal_rule)

    @abc.abstractmethod
    def parse_horizontal_rule(self, rule):
        """
        Parse the given HorizontalRule instance.
        :param rule: the horizontal rule
        :return: the string representation of the horizontal rule in an arbitrary format
        """
        pass
