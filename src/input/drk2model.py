# -*- coding: utf-8 -*-
"""
Module to convert the input DRK binary file to the model.
"""

import os
import re
from collections import namedtuple

from src.common.binaryreader import BinaryReader
from src.common.model import Document, Paragraph, Header, Text, Table, TableCell, \
    HorizontalRule
from src.config import Measurement
from src.tools.collection_tools import namedtuple_with_defaults, filter_empty, group_by_predicate, \
    filter_none, list_rstrip
from src.tools.file_tools import save_file_if
from src.tools.text_tools import unicode_decode, unicode_encode, split, is_valid_char, \
    starts_with_uppercase, is_polish_letter

# number of useless header lines
HEADER_SIZE = 4  # number of non-empty lines, which are not needed in the output document

# column separators
COLUMN_SEP = 'É'  # the table column separator
COLUMN_BORDER = 'Ě'  # the table column border

# subscript / superscript separators
SUBSCRIPT_SEP = ('ő', 'ú')  # the subscript separators
SUPERSCRIPT_SEP = ('^', 'ô')  # the superscript separators
SCRIPT_SEPARATORS = SUBSCRIPT_SEP + SUPERSCRIPT_SEP  # the tuple combining both separator tuples

# regex patterns
PATTERN_TRIM_LINE_BEGIN = re.compile(ur"^\x0f*\x12*")  # pattern to be trimmed from the line begin
PATTERN_BOLD_RAW = u"\xc5\xb0"
PATTERN_BOLD = re.compile(r"^%s" % PATTERN_BOLD_RAW)  # the pattern to match the bold line
PATTERN_TABLE_BEGIN = re.compile(ur"^\xc4\x8c\xc3\x89")  # the pattern to match the table begin
PATTERN_TABLE_END = re.compile(ur"^\xc4\x90\xc3\x89")  # the pattern to match the table end
PATTERN_HEADER_LEVEL_THREE = re.compile(r"\*+(.+?)\*+")  # the header level three pattern
PATTERN_SPACE = re.compile(r" .")  # the pattern indicating the space with a redundant character
PATTERN_PAGE_NUM = re.compile(r"- [0-9]+ -")  # the pattern indicating the page number
PATTERN_H_RULE = re.compile(r"^-+$")  # the pattern indicating the horizontal rule
PATTERN_IDENTIFIER = re.compile(r"^(.+/.+/[0-9]{2}).?")  # the file identifier pattern

# invalid line patterns
PATTERNS_INVALID_LINES = (re.compile(r"[x]+"),)

# newline characters
CHARS_NEWLINE = ('*',)  # the characters indicating a new line

# measurement type colspan rules (dictionary: {measurement type : indices to span by one column})
COLSPAN_RULES = {Measurement.MEASUREMENT_ELECTRICAL_LIGHTING.get_value(): (5, 7),
                 Measurement.MEASUREMENT_DUST.get_value(): (3,),
                 Measurement.MEASUREMENT_DUST_CHEMICALS.get_value(): (3,)}

# helper types to model the input file text
Block = namedtuple_with_defaults('Block', 'type lines', {'type': type(Paragraph)})
HeaderLevelOne = namedtuple('HeaderLevelOne', '')
HeaderLevelTwo = namedtuple('HeaderLevelTwo', '')
HeaderLevelThree = namedtuple('HeaderLevelThree', '')

# table separator and continuation indicator types
RowSep = namedtuple('RowSep', '')
TableSep = namedtuple('TableSep', '')


def drk2model(file_path, pandoc_mode, test_mode):
    """
    Entry point to convert the given DRK file to the model.
    :param file_path: the input file path (without file extension)
    :param pandoc_mode: the indicator, whether the parser runs in pandoc mode
    :param test_mode: the indicator, whether the app is in the test mode
    :return: tuple: (output model, DRK file identifier)
    """
    # convert DRK to text
    reader = DrkReader("%s.drk" % file_path)
    text_output = reader.as_string()

    # save text in test mode
    save_file_if(test_mode, "%s.txt" % file_path, text_output)

    # convert text to model
    return Drk2ModelParser(pandoc_mode=pandoc_mode).parse(text_output)


class DrkReader(BinaryReader):
    """
    Class to read any DRK binary file.
    """

    def byte_to_char(self, byte):
        return chr(byte).decode('windows-1250').encode('utf-8')


class Drk2ModelParser(object):
    """
    Parser to parse the DRK-formatted text to the model.
    Use parse method as an entry point.
    """

    def __init__(self, pandoc_mode=False):
        """
        Instantiate the parser.
        :param pandoc_mode: an indicator, whether the created model should be pandoc-compatible
        (False by default)
        """
        self.pandoc_mode = pandoc_mode
        self.file_identifier = None  # the identifier of the file (indicates headers level 1 & 2)
        self.last_measurement_type = None  # the measurement type used to customize the tables

    def parse(self, text):
        """
        Convert the input text to the model.
        :param text: the input text
        :return: tuple: (the model representation of the text, file identifier)
        """

        # return if there is no text
        if not text:
            return Document()

        # first, remove empty lines and trim each of the remaining lines from invalid characters
        lines = self.remove_header(
            [self.trim_line_begin(line) for line in self.remove_empty_lines(text)])

        # then, divide the lines into semantic blocks
        blocks = self.get_blocks(lines)

        # at last, convert all the blocks to model elements and append to the output document
        return Document(self.parse_blocks(blocks)), self.file_identifier

    @staticmethod
    def remove_empty_lines(text):
        """
        Remove every second line, provided it is empty, in order to get rid of redundant lines.
        :param text: the input text
        :return: the input text lines without redundant empty lines
        """
        lines = text.splitlines()

        # determine index of first non-empty line to know, which every-second line to consider
        first_non_empty_line_index = 0
        while not lines[first_non_empty_line_index] and first_non_empty_line_index < len(lines):
            first_non_empty_line_index += 1

        # append every second line and every non-empty line to the result
        result = []
        for line in enumerate(lines):
            if line[0] % 2 == first_non_empty_line_index % 2 or line[1]:
                result.append(line[1])

        return result

    @staticmethod
    def trim_line_begin(line):
        """
        Trim the characters from TRIM_LINE_BEGIN from the beginning of the given line.
        :param line: the line to trim
        :return: the trimmed line
        """
        return re.sub(PATTERN_TRIM_LINE_BEGIN, "", line)

    @staticmethod
    def remove_header(lines):
        """
        Remove the header of the file (first five non-empty lines).
        :param lines: the input text
        :return: the input text lines without the first five non-empty lines
        """
        non_empty_lines_count = 0
        index = 0
        while non_empty_lines_count < HEADER_SIZE:
            if lines[index]:
                non_empty_lines_count += 1
            index += 1
        while not lines[index]:
            index += 1
        return lines[index:]

    def get_blocks(self, lines):
        """
        Iterate over all the lines and determine, to which blocks (text or table) do they belong.
        Return the list of all blocks, which are determined using the following indicators:

        TABLE BLOCK
        - begins with PATTERN_TABLE_BEGIN
        - ends with PATTERN_TABLE_END

        HEADER BLOCK
        - first bold line after the file identifier (the fifth file line) (level 1) OR
        - single bold line in form: ** <HEADER_LABEL> ** (level 2)

        HORIZONTAL RULE BLOCK
        - inserted right before header level 1
        - single line containing only hyphens

        PARAGRAPH BLOCK
        - begins after any other block ends
        - ends before any other block begins

        :param lines: the input text
        :return: the text blocks
        """

        # first line (after file header is removed) is a file identifier - set it instance-wise
        self.file_identifier = self._get_file_identifier(lines[0])

        # prepare the indices list, which looks as follows: [(start_index, type), end_index, ...]
        indices = self._get_block_indices(lines)

        # convert indices to blocks and return
        return self._map_indices_to_blocks(lines, indices)

    @staticmethod
    def _get_file_identifier(line, identifier=None):
        """
        Determine whether the given line is a file identifier and return it if so.
        :param line: the line to investigate
        :return: the file identifier or None, if not found
        """
        pattern = PATTERN_IDENTIFIER if identifier is None else re.compile(r"(%s).?" % identifier)
        result = re.match(pattern, line)
        if result is not None:
            return result.group(1)

        return result

    def _get_block_indices(self, lines):
        """
        Prepare the indices list, which looks as follows: [(start_index, type), end_index, ...],
        based on the given lines.
        The indices will be later used to separate the lines into typed blocks.
        :param lines: the lines to analyze
        :return: the indices list
        """
        indices = []
        len_lines = len(lines)

        for (index, line) in enumerate(lines):
            is_table_closed = not len(indices) % 2

            # horizontal rule
            if is_table_closed and self._is_horizontal_rule(line):
                indices.extend([(index, HorizontalRule), index + 1])

            # headers level 1 & 2
            elif is_table_closed and index + 1 < len_lines and \
                    self._is_header_level_one(line, lines[index + 1]):
                # header level 1
                indices.extend([(index + 1, HeaderLevelOne), index + 2])
                # header level 2
                if index + 2 < len_lines and self._is_bold(lines[index + 2]):
                    indices.extend([(index + 2, HeaderLevelTwo), index + 3])

            # header level 3
            elif is_table_closed and self._get_header_level_three(line):
                indices.extend([(index, HeaderLevelThree), index + 1])

            # table start
            elif is_table_closed and re.match(PATTERN_TABLE_BEGIN, line):
                indices.append((index, Table))

            # table end
            elif not is_table_closed and re.match(PATTERN_TABLE_END, line):
                indices.append(index + 1)

        return indices

    @staticmethod
    def _map_indices_to_blocks(lines, indices):
        """
        Convert the indices list in a list of tuples and for each tuple append two blocks:
        - one paragraph
        - one typed block from indices list.
        Add additional paragraph in the end.
        :param lines: the lines, which are associated to the typed blocks
        :param indices: the indices list, based on which the blocks will be created
        :return: the list of typed blocks
        """
        blocks = []

        cursor = 0
        for (start_index, end_index) in [(a, b) for a, b in zip(indices[::2], indices[1::2])]:
            if start_index[0] - cursor:  # append paragraph if it contains any lines
                blocks.append(Block(Paragraph, lines[cursor:start_index[0]]))
            blocks.append(Block(start_index[1], lines[start_index[0]:end_index]))
            cursor = end_index
        if len(lines) - cursor:  # append paragraph if it contains any lines
            blocks.append(Block(Paragraph, lines[cursor:]))

        return blocks

    def _is_horizontal_rule(self, line):
        """
        Determine whether the given line is a horizontal rule.
        :param line: the line to check
        :return: True if a line is a horizontal rule
        """
        return re.match(PATTERN_H_RULE, self._strip(line)) is not None

    def _is_header_level_one(self, line, next_line):
        """
        Determine whether the given lines indicate a header level 1.
        :param line: the line to investigate
        :param next_line: the forthcoming line
        :return: True if the forthcoming line is a header level 1, False otherwise
        """
        return self._get_file_identifier(line, self.file_identifier) and self._is_bold(next_line) \
               and not self._get_header_level_three(next_line)

    @staticmethod
    def _get_header_level_three(line):
        """
        Determine whether the given line is a header level 3 and return its text if so.
        It is such a header provided it starts and ends with two consecutive stars.
        :param line: the line to investigate
        :return: The header text in case the line is a header level 3, an empty string otherwise
        """
        header = PATTERN_HEADER_LEVEL_THREE.match(line[len(PATTERN_BOLD_RAW):])
        return header.group(1) if header is not None else ""

    @staticmethod
    def _is_bold(line):
        """
        Determine whether the given line is bold.
        :param line: the line to investigate
        :return: True in case the line is bold, False otherwise
        """
        return re.match(PATTERN_BOLD, line) is not None

    def parse_blocks(self, blocks):
        """
        Iterate over all retrieved blocks and parse them to the corresponding model elements.
        :param blocks: the text blocks
        :return: the model elements
        """
        funcs = {Paragraph: self.parse_paragraph,
                 Table: self.parse_table,
                 HeaderLevelOne: self.parse_header_level_one,
                 HeaderLevelTwo: self.parse_header_level_two,
                 HeaderLevelThree: self.parse_header_level_three,
                 HorizontalRule: self.parse_horizontal_rule}

        return filter_none([funcs[block.type](block) for block in blocks if block.type in funcs])

    @staticmethod
    def parse_horizontal_rule(horizontal_rule):
        """
        Parse the horizontal rule block to the output horizontal rule model.
        :param horizontal_rule: the horizontal rule block
        :return: the horizontal rule model
        """
        if not (isinstance(horizontal_rule, Block) and horizontal_rule.type == HorizontalRule):
            return None

        return HorizontalRule()

    def parse_header_level_one(self, header):
        """
        Parse the header level one block to the output header model.
        :param header: the header level one block
        :return: the header model
        """
        if not (isinstance(header, Block) and header.type == HeaderLevelOne) or not header.lines:
            return None

        # calculate header text & header level
        header_text = self._extract_text(header.lines[0])

        # return header if header_text is set
        if not header_text:
            return None
        return Header(header_text, level=1)

    def parse_header_level_two(self, header):
        """
        Parse the header level two block to the output header model.
        :param header: the header level two block
        :return: the header model
        """
        if not (isinstance(header, Block) and header.type == HeaderLevelTwo) or not header.lines:
            return None

        # calculate header text & header level
        header_text = self._extract_text(header.lines[0])

        # return header if header_text is set
        if not header_text:
            return None
        return Header(header_text, level=2)

    def parse_header_level_three(self, header):
        """
        Parse the header level three block to the output header model.
        :param header: the header level three block
        :return: the header model
        """
        if not (isinstance(header, Block) and header.type == HeaderLevelThree) or not header.lines:
            return None

        # calculate header text & header level
        header_text = self._fix_spaces(self._get_header_level_three(header.lines[0])).strip()

        # update last measurement text to know, which measurement unit is the current one
        self.last_measurement_type = header_text

        # return header if header_text is set
        if not header_text:
            return None
        return Header(header_text, level=3)

    @staticmethod
    def _fix_spaces(text):
        """
        Clear the spaces between the words from the characters specified in the SPACE_PATTERN.
        :param text: the text to handle
        :return: the text without the exclamation marks
        """
        return re.sub(PATTERN_SPACE, " ", text)

    def parse_paragraph(self, paragraph):
        """
        Iterate over text block lines and parse them filling the output paragraph model.
        :param paragraph: the paragraph to parse
        :return: the paragraph model
        """
        if not (isinstance(paragraph, Block) and paragraph.type == Paragraph and paragraph.lines):
            return None

        texts = []
        for line in paragraph.lines:

            # continue if line is a page number or an identifier
            if self._is_page_number(line) or self._get_file_identifier(line, self.file_identifier):
                continue

            # determine whether the line is bold and strip it from invalid characters
            is_bold = self._is_bold(line)
            text = self._extract_text(line)

            # continue if line is not valid
            if not self._is_line_valid(text):
                continue

            # determine whether the line is a new line and replace all -scripts
            is_new_line = self._is_new_line(text)
            text = self._split_by_s_scripts(text)  # replace subscripts & superscripts
            if text:
                if texts:
                    texts.append(os.linesep if is_new_line else " ")
                texts.append(Text(text, bold=is_bold))

        # return paragraph if texts are set
        if not texts:
            return None
        return Paragraph(Text(texts))

    @staticmethod
    def _is_page_number(text):
        """
        Check whether the given text matches the page number pattern.
        :param text: the input text
        :return: True in case the text is a page number, False otherwise
        """
        return re.match(PATTERN_PAGE_NUM, text.strip())

    def _strip(self, line):
        """
        Strip the line from the non-ASCII characters, which are not polish characters.
        :param line: the line to strip
        :return: the line stripped from the non-ASCII non-polish characters
        """
        char_list = unicode_decode(line)
        start_index = self._get_first_valid_index(char_list)
        end_index = len(char_list) - self._get_first_valid_index(list(reversed(char_list)))
        return unicode_encode(char_list[start_index:end_index])

    def _extract_text(self, text):
        """
        Helper function to extract the clean text from the given text.
        This function consolidates calls of the following functions:
         - self._strip()
         - self._fix_spaces()
         - basestring.strip()
        :param text: the text to clean up
        :return: the extracted text
        """
        return self._fix_spaces(self._strip(text)).strip()

    @staticmethod
    def _get_first_valid_index(char_list):
        """
        Get the index of the first ASCII or polish character in the given character list.
        :param char_list: the character list
        :return: the index of the first ASCII or polish character
        """
        length = len(char_list)
        idx = 0
        while idx < length and not is_valid_char(char_list[idx]):
            idx += 1
        return idx

    @staticmethod
    def _is_line_valid(line):
        """
        Determine whether the given line is valid.
        It is considered to be invalid in case it contains only an element listed in
        INVALID_LINES or is an empty line.
        :param line: the line to investigate
        :return: True in case the line is valid, False otherwise
        """
        return line and not any([re.match(pattern, line) for pattern in PATTERNS_INVALID_LINES])

    @staticmethod
    def _is_new_line(line):
        """
        Determine whether the given line is a new line.
        It is considered to be a new line in case it starts with an uppercase character
        or with any of the characters listed in CHARS_NEWLINE.
        :param line: the line to investigate
        :return: True in case the line is considered to be a new line, False otherwise
        """

        # any character from CHARS_NEWLINE indicates a new line
        if any([line.startswith(char_newline) for char_newline in CHARS_NEWLINE]):
            return True

        # if the first character is not uppercase, a line is not a new line
        is_uppercase = starts_with_uppercase(line)
        if not is_uppercase:
            return False

        # if the first character is not a polish letter, a line is not a new line
        characters = unicode_decode(line)
        if not is_polish_letter(characters[0]):
            return False

        # if the first word is not a one-letter-word, a line is a new line
        if len(characters) >= 2 and is_polish_letter(characters[1]):
            return True

        # if the first word is a one-letter-word and the second word starts with a polish letter,
        # a line is a new line
        if len(characters) >= 3 and characters[1] == ' ' and is_polish_letter(characters[2]):
            return True

        return False

    def _split_by_s_scripts(self, text, index=0):
        """
        Split the given text by all the subscript and superscript characters.
        Note that this method does not support subscript / superscript nesting!
        The method is called recursively for each given separator.
        :param text: the text to replace the subscripts & superscripts in
        :param index: the current separator index
        :return: a list of texts, every second of which being a subscript / superscript text
        """
        if index >= len(SCRIPT_SEPARATORS):
            return [text]

        separator = SCRIPT_SEPARATORS[index]
        is_superscript = separator in SUPERSCRIPT_SEP
        tmp_result = split(text, separator)
        result = []
        for idx, item in enumerate(tmp_result):
            if item:
                if idx % 2:
                    result.append(
                        Text([item], superscript=is_superscript, subscript=not is_superscript))
                else:
                    result += self._split_by_s_scripts(item, index + 1)

        return result

    def parse_table(self, table):
        """
        Iterate over the table block lines and parse them filling the output table model.
        :param table: the table block
        :return: the table model
        """
        if not (isinstance(table, Block) and table.type == Table and table.lines):
            return None

        # create the matrix containing either the string cell contents or the row separator markers
        rows = [self._split_to_cells(line) for line in table.lines[1:-1]]

        # ignore the table continuation lines
        rows = self._exclude_table_continuation(rows)
        if not rows:
            return None

        # adjust length of each row to the longest row
        max_row_size = max(len(row) for row in rows)
        rows = [self._adjust_row_size(row, max_row_size) for row in rows]

        # merge each two consecutive rows, which do not contain row separator markers
        rows = self._merge_rows(rows)

        # map the matrix to the actual table containing the TableCell instances
        return self._map_to_table(rows)

    def _split_to_cells(self, line):
        """
        Split the given table line to get the list of cell contents.
        The cell contents are already cleaned up, i.e. the invalid characters are removed
        as well as the superscripts are replaced.
        :return: the list of cell contents
        """

        # split cells by the given separator pattern
        cells = line.split(COLUMN_BORDER)

        # create table separator (continuation) row in case of a split table
        if cells and self._is_page_number(cells[0]):
            return [[TableSep()]]

        # remove redundant empty cells (as the line starts and ends with COLUMN_BORDER)
        cells = cells[1:] if cells and not self._strip(cells[0]) else cells
        cells = cells[:-1] if cells and not self._strip(cells[-1]) else cells

        # exclude & save those cells, which haven't been split, as they contain row sep char(s)
        cells = self._create_separator_cells(cells, line.startswith(COLUMN_BORDER))
        index_to_separators = group_by_predicate(lambda cell: cell == RowSep(), cells)
        cells = [cell for cell in cells if cell != RowSep()]

        # clean up the cell contents
        cells = [self._strip(self._fix_spaces(cell).strip()) for cell in cells]

        # replace superscripts & subscripts
        cells = [self._split_by_s_scripts(cell) for cell in cells]

        # append separator cells
        for index in sorted(index_to_separators, reverse=True):
            for elem in sorted(index_to_separators[index], reverse=True):
                cells.insert(index, [elem])

        # return
        return cells

    def _create_separator_cells(self, row, starts_with_column_border):
        """
        For the given table row, replace the COLUMN_SEP pattern with the RowSep marker instance.
        :param row: the table row to replace
        :param starts_with_column_border: an indicator, whether the line starts with
                the column border character
        :return: the table rows with row separators
        """
        new_row = []
        for cell in row:
            sep_count = self._get_column_sep_count(cell)
            if not sep_count:
                new_row.append(cell)
            else:
                # add an additional empty cell in case line starts with the column border
                if starts_with_column_border:
                    new_row.append("")
                # append separators for each column separator character
                for _ in range(sep_count):
                    new_row.append(RowSep())
        return new_row

    @staticmethod
    def _get_column_sep_count(contents):
        """
        Get the table column separator count based on the given table contents.
        :param contents: the table contents to investigate
        :return: the number of detected table columns
        """
        return contents.count(COLUMN_SEP)

    @staticmethod
    def _exclude_table_continuation(rows):
        """
        In case the rows contain the table continuation indicator (which is the file identifier),
        exclude it including the first following row (which is the header duplication).
        :param rows: the rows to investigate
        :return: the rows without the table continuation lines
        """
        result = []
        iterator = iter(rows)
        for row in iterator:
            if row and row[0] and isinstance(row[0][0], TableSep):
                # skip all rows until the second separator row
                for _ in range(2):
                    row = iterator.next()
                    try:
                        while not row or not all([RowSep() in cell for cell in row]):
                            row = iterator.next()
                    except StopIteration:
                        break
            else:
                result.append(row)
        return result

    def _adjust_row_size(self, row, row_size):
        """
        Adjust row so that its length is equal to the given row_size.
        In case the row consists RowSep markers, try to put empty cells around them.
        :param row: the row to adjust
        :param row_size: the expected row size
        :return: the row with the adjusted length
        """

        # calculate number of missing cells
        missing_cells_num = row_size - len(row)
        if not missing_cells_num:
            return row

        # add empty cells for each measurement type available in COLSPAN_RULES
        if self.last_measurement_type in COLSPAN_RULES:
            for col_index in COLSPAN_RULES[self.last_measurement_type]:
                if missing_cells_num and col_index < row_size:
                    row.insert(col_index, [""])
                    missing_cells_num -= 1

        # add empty cells on the end of the row if there are still missing cells
        for _ in range(missing_cells_num):
            row.append([""])

        return row

    def _merge_rows(self, table):
        """
        In case any two consecutive table rows do not contain a single RowSeparator marker,
        merge them into a single row.
        :param table: the input table (a list of rows)
        :return: the table with merged rows
        """
        if len(table) < 2:
            return table

        if self._check_rows_for_row_sep(table[:2]):
            return [table[0]] + self._merge_rows(table[1:])

        merged_rows = [self._merge_cell_contents(first, second) for first, second in
                       zip(table[0], table[1])]
        return self._merge_rows([merged_rows] + table[2:])

    def _check_rows_for_row_sep(self, rows):
        """
        Check if any given row contains a RowSep instance.
        :param rows: the rows to check
        :return: True if any of the rows contains RowSep instance, False otherwse
        """
        return any([self._check_row_for_row_sep(row) for row in rows])

    @staticmethod
    def _check_row_for_row_sep(row):
        """
        Check if the given row contains a RowSep instance.
        :param row: the row to check
        :return: True if the row contains RowSep instance, False otherwise
        """
        return any([RowSep() in cell for cell in row])

    @staticmethod
    def _merge_cell_contents(*cells):
        """
        Merge the given cell contents by wrapping them into a list
        and adding line separators in between.
        If any of the input cells contains an empty string, ignore it.
        :param cells: the cells to merge
        :return: the single cell with merged contents
        """
        result = []
        for cell in cells:
            if result:
                result.append(os.linesep)
            result += cell
        return result

    def _map_to_table(self, rows):
        """
        Map each row of the given table to the collection of TableCell instances
        and return the Table instance.
        :param rows: the list of rows
        :return: the Table instance
        """

        # initialize output
        output = [[] for _index in range(len(rows))]

        # iterate column-wise to merge those cells, which are row-spanned
        for column in [[row[index] for row in rows] for index in range(len(rows[0]))]:
            groups = group_by_predicate(lambda cell: RowSep() not in cell, column)
            for index in groups:
                group_val = groups[index]
                rows_without_row_sep = self._get_rows_without_row_sep(
                    rows[index:index + len(group_val)])
                self._insert_cell(output, list_rstrip(self._merge_cell_contents(*group_val)),
                                  len(rows_without_row_sep),
                                  index, rows_without_row_sep)

        # filter out empty rows and return the table
        return Table(filter_empty(output))

    def _insert_cell(self, table, content, rowspan, row_index, row_indices_wo_row_sep):
        """
        Insert the cell with the given content and rowspan to the given table.
        Execution of this function depends on whether PANDOC_MODE is on or off.
        In case it is off, the cell with the given rowspan is inserted,
        otherwise the empty cells are inserted instead.
        :param table: the table to insert the cells to
        :param content: the cell content to apply
        :param rowspan: the rowspan of the cell
        :param row_index: the current row index
        :param row_indices_wo_row_sep: list of indices of those rows, which do not contain RowSep
        """
        if self.pandoc_mode:  # in pandoc mode insert empty cells instead of rowspan
            table[row_index].append(TableCell(Text(texts=content)))
            for rowspan_index in range(1, rowspan):
                table[row_index + row_indices_wo_row_sep[rowspan_index]].append(TableCell())
        else:
            table[row_index].append(TableCell(Text(texts=content), rowspan=rowspan))

    def _get_rows_without_row_sep(self, rows):
        """
        Return the indices of rows of those rows, which do not contain a RowSep instance.
        :param rows: the rows to check
        :return: the indices of rows not containing a RowSep instance
        """
        return [index for index, row in enumerate(rows) if not self._check_row_for_row_sep(row)]
