"""
Module to bring all the parsers together.
The convert function creates the requested document based on the given input binary document.
"""
import os
import sys
from collections import namedtuple

from src.input.drk2model import drk2model
from src.model.model2docx import model2docx
from src.output.docxprocessor import customize_docx
from src.tools.collection_tools import namedtuple_with_defaults

# object to store the conversion result
ConversionResult = namedtuple_with_defaults("ConversionResult", "output error", {"error": None})
ConversionError = namedtuple("ConversionError", "info traceback")


def convert(file_path, template=None, pandoc_mode=False, test_mode=False):
    """
    Convert the given file from the input format to the output binary format.
    :param file_path: the input DRK file path
    :param template: the DOCX template document file path
    :param pandoc_mode: an indicator, whether to use pandoc to create the output binary document
    :param test_mode: an indicator, whether the app runs in the test mode
    """
    output_path = ""
    try:
        raw_path = os.path.splitext(file_path)[0]
        model, identifier = parse_drk_to_model(raw_path, pandoc_mode, test_mode)
        output_path = parse_model_to_docx(model, raw_path, template, test_mode)
        process_docx(output_path, identifier, template)
    except:
        _, exc_info, exc_traceback = sys.exc_info()
        return ConversionResult(output_path, ConversionError(exc_info, exc_traceback))

    return ConversionResult(output_path)


def parse_drk_to_model(file_path, pandoc_mode, test_mode):
    """
    Factory method to call the parser, which converts the input DRK file to the model.
    :param file_path: the input file path (without file extension)
    :param pandoc_mode: the indicator, whether pandoc will be applied during conversion
    :param test_mode: the indicator, whether the app runs in the test mode
    :return: the tuple: (model, DRK file identifier)
    """
    return drk2model(file_path, pandoc_mode, test_mode)


def parse_model_to_docx(model, file_path, template, test_mode):
    """
    Factory method to call the parser, which converts the input model to the DOCX file.
    :param model: the input model
    :param file_path: the input file path (without file extension)
    :param template: the DOCX template file path
    :param test_mode: the indicator, whether the app runs in the test mode
    :return: output DOCX file path
    """
    return model2docx(model, file_path, template, test_mode)


def process_docx(file_path, identifier, template):
    """
    Factory method to call the generated DOCX file postprocessor.
    :param file_path: the input file path (without file extension)
    :param identifier: the DRK file identifier
    :param template: the DOCX template file path
    """
    customize_docx(file_path, identifier, template)
