<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="pl_PL">
<context>
    <name>controller</name>
    <message>
        <location filename="controller.py" line="402"/>
        <source>app-description</source>
        <translation>Program do konwersji plików DRK do formatu DOCX.</translation>
    </message>
    <message>
        <location filename="controller.py" line="403"/>
        <source>app-help-files</source>
        <translation>
            Lista plików do konwertowania.
            Nie ma znaczenia w przypadku uruchamiania interfejsu użytkownika. W przeciwnym razie
            należy podać ścieżki plików DRK do konwersji.
        </translation>
    </message>
    <message>
        <location filename="controller.py" line="404"/>
        <source>app-help-headless</source>
        <translation>Parametr do uruchamiania aplikacji bez interfejsu użytkownika.</translation>
    </message>
    <message>
        <location filename="controller.py" line="323"/>
        <source>headless-progress</source>
        <translation>Postęp</translation>
    </message>
    <message>
        <location filename="controller.py" line="333"/>
        <source>headless-press-key</source>
        <translation>Naciśnij dowolny klawisz...</translation>
    </message>
    <message>
        <location filename="controller.py" line="319"/>
        <source>headless-process</source>
        <translation>Przetwarzam %s ...</translation>
    </message>
</context>
<context>
    <name>docxlauncher</name>
    <message>
        <location filename="ext/docxlauncher.py" line="35"/>
        <source>cannot-open-file</source>
        <translation>Błąd podczas otwierania pliku</translation>
    </message>
    <message>
        <location filename="ext/docxlauncher.py" line="36"/>
        <source>file-missing</source>
        <translation>Plik %s nie istnieje.</translation>
    </message>
    <message>
        <location filename="ext/docxlauncher.py" line="40"/>
        <source>file-invalid</source>
        <translation>Plik %s nie jest plikiem z rozszerzeniem .docx.</translation>
    </message>
</context>
<context>
    <name>errorsupport</name>
    <message>
        <location filename="ext/errorsupport.py" line="39"/>
        <source>result-title</source>
        <translation>Konwersja zakończona</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="41"/>
        <source>result-success</source>
        <translation>Operacja zakończona sukcesem!</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="47"/>
        <source>result-errors-check-log</source>
        <translation>(pełna lista plików znajduje się w wygenerowanym pliku .log)</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="49"/>
        <source>result-errors</source>
        <translation>Niestety pojawiły się błędy podczas konwersji następujących plików:</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="52"/>
        <source>result-errors-advice</source>
        <translation>Zamknij wszystkie pliki .docx, które mają zostać nadpisane oraz upewnij się, że masz prawa zapisu do docelowych katalogów.</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="101"/>
        <source>error</source>
        <translation>WYSTĄPIŁ BŁĄD!</translation>
    </message>
</context>
<context>
    <name>msword</name>
    <message>
        <location filename="ext/win/msword.py" line="42"/>
        <source>word-invalid</source>
        <translation>Nie wykryto programu Microsoft Word w wersji nie starszej niż 2007.</translation>
    </message>
    <message>
        <location filename="ext/win/msword.py" line="43"/>
        <source>cannot-launch</source>
        <translation>Błąd podczas otwierania Microsoft Word</translation>
    </message>
</context>
<context>
    <name>view</name>
    <message>
        <location filename="ui/view.py" line="65"/>
        <source>btn-convert</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="ui/view.py" line="67"/>
        <source>btn-list-item-remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="ui/view.py" line="68"/>
        <source>btn-list-item-add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="ui/view.py" line="69"/>
        <source>window-title</source>
        <translation>Konwerter raportów programu HPZ</translation>
    </message>
</context>
<context>
    <name>widgets</name>
    <message>
        <location filename="ui/widgets.py" line="251"/>
        <source>dnd-too-many-files</source>
        <translation>Duża ilość plików</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="252"/>
        <source>dnd-too-many-files-content</source>
        <translation>Przeciągasz dużą liczbę plików, to może zająć trochę czasu.</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="254"/>
        <source>prompt-proceed</source>
        <translation>Czy chcesz kontynuować?</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="270"/>
        <source>dnd-processing-files-content</source>
        <translation>Przetwarzam pliki...</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="271"/>
        <source>btn-abort</source>
        <translation>Zrezygnuj</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="273"/>
        <source>dnd-processing-files</source>
        <translation>Operacja w toku</translation>
    </message>
</context>
</TS>
