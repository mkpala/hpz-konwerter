<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>controller</name>
    <message>
        <location filename="controller.py" line="402"/>
        <source>app-description</source>
        <translation>Application to convert DRK files to the DOCX format.</translation>
    </message>
    <message>
        <location filename="controller.py" line="403"/>
        <source>app-help-files</source>
        <translation>
            List of the files to convert.
            It does not matter in case of launching the User Interface. Otherwise it is required
            to provide the input DRK file paths.
        </translation>
    </message>
    <message>
        <location filename="controller.py" line="404"/>
        <source>app-help-headless</source>
        <translation>Parameter to launch the application without the User Interface.</translation>
    </message>
    <message>
        <location filename="controller.py" line="323"/>
        <source>headless-progress</source>
        <translation>Progress</translation>
    </message>
    <message>
        <location filename="controller.py" line="333"/>
        <source>headless-press-key</source>
        <translation>Press any key...</translation>
    </message>
    <message>
        <location filename="controller.py" line="319"/>
        <source>headless-process</source>
        <translation>Processing %s ...</translation>
    </message>
</context>
<context>
    <name>docxlauncher</name>
    <message>
        <location filename="ext/docxlauncher.py" line="35"/>
        <source>cannot-open-file</source>
        <translation>Cannot open file</translation>
    </message>
    <message>
        <location filename="ext/docxlauncher.py" line="36"/>
        <source>file-missing</source>
        <translation>File %s does not exist.</translation>
    </message>
    <message>
        <location filename="ext/docxlauncher.py" line="40"/>
        <source>file-invalid</source>
        <translation>File %s is not a file with the .docx extension.</translation>
    </message>
</context>
<context>
    <name>errorsupport</name>
    <message>
        <location filename="ext/errorsupport.py" line="39"/>
        <source>result-title</source>
        <translation>Conversion finished</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="41"/>
        <source>result-success</source>
        <translation>Operation successful!</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="47"/>
        <source>result-errors-check-log</source>
        <translation>(check error log for the full file list)</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="49"/>
        <source>result-errors</source>
        <translation>Unfortunately errors occurred while converting the following files:</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="52"/>
        <source>result-errors-advice</source>
        <translation>Please close all corresponding .docx files and make sure the target directories are not read-only.</translation>
    </message>
    <message>
        <location filename="ext/errorsupport.py" line="101"/>
        <source>error</source>
        <translation>ERROR OCCURRED!</translation>
    </message>
</context>
<context>
    <name>msword</name>
    <message>
        <location filename="ext/win/msword.py" line="42"/>
        <source>word-invalid</source>
        <translation>Microsoft Word in version not older than 2007 not detected.</translation>
    </message>
    <message>
        <location filename="ext/win/msword.py" line="43"/>
        <source>cannot-launch</source>
        <translation>Error while launching Microsoft Word</translation>
    </message>
</context>
<context>
    <name>view</name>
    <message>
        <location filename="ui/view.py" line="65"/>
        <source>btn-convert</source>
        <translation>Convert</translation>
    </message>
    <message>
        <location filename="ui/view.py" line="67"/>
        <source>btn-list-item-remove</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="ui/view.py" line="68"/>
        <source>btn-list-item-add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="ui/view.py" line="69"/>
        <source>window-title</source>
        <translation>HPZ Report Converter</translation>
    </message>
</context>
<context>
    <name>widgets</name>
    <message>
        <location filename="ui/widgets.py" line="251"/>
        <source>dnd-too-many-files</source>
        <translation>Too many files</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="252"/>
        <source>dnd-too-many-files-content</source>
        <translation>You want to drop a lot of files, it might take some time.</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="254"/>
        <source>prompt-proceed</source>
        <translation>Do you want to proceed?</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="270"/>
        <source>dnd-processing-files-content</source>
        <translation>Processing files...</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="271"/>
        <source>btn-abort</source>
        <translation>Abort</translation>
    </message>
    <message>
        <location filename="ui/widgets.py" line="273"/>
        <source>dnd-processing-files</source>
        <translation>Operation in progress</translation>
    </message>
</context>
</TS>
