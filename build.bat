@echo off

echo ************************
echo *** BUILD EXECUTABLE ***
echo ************************
pyinstaller --noupx -y -n hpz-konwerter -i res/icon.ico --add-data "res;res" --add-data "config;config" --add-data "%PYTHONHOME%/Lib/site-packages/docx/templates/default.docx;./docx/templates" controller.py

echo ***********************
echo *** BUILD INSTALLER ***
echo ***********************
iscc installer/windows/setup.iss
