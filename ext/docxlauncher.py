"""
Factory for the external .docx launchers.
"""
import abc
import os

from PySide.QtCore import QCoreApplication

from ext.errorsupport import show_error

EXT_DOCX = r".docx"  # MS Word file extension


class DocXLauncher(object):
    """
    Abstract .docx launcher.
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, widget):
        """
        Instantiate the class.
        :param widget: the parent UI widget, to display the message boxes for
        """
        self.widget = widget

    def open(self, path):
        """
        Validate and open the given .docx file.
        :param path: the .docx file path
        :return: the launched subprocess
        """
        title = QCoreApplication.translate("docxlauncher", "cannot-open-file")

        if not os.path.exists(path):
            content = QCoreApplication.translate("docxlauncher", "file-missing") % path
            return show_error(content, title, self.widget)

        if os.path.splitext(path)[1].lower() != EXT_DOCX:
            content = QCoreApplication.translate("docxlauncher", "file-invalid") % path
            return show_error(content, title, self.widget)

        return self.launch(path)

    @abc.abstractmethod
    def launch(self, path):
        """
        Launch the given .docx file in the external application.
        :param path: the .docx file path
        :return: the launched subprocess
        """
        pass
