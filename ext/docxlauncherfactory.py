"""
The docx launcher factory module.
"""

from ext.win.msword import MSWordLauncher


class Factory(object):
    """
    Factory class for the .docx file launcher.
    """

    def __init__(self, widget=None):
        """
        Instantiate the class.
        :param widget: the parent UI widget, to display the message boxes for
        """
        self.widget = widget
        self.launcher = None

    def get(self):
        """
        Create or get the launcher.
        :return: the launcher instance
        """
        if not self.launcher:
            self.launcher = self.create()
        return self.launcher

    def create(self):
        """
        Create the launcher.
        :return: the new launcher instance
        """
        return MSWordLauncher(self.widget)
