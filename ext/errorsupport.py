"""
Module for the error support.
"""

# conversion result will be stored here
import os
import traceback
from collections import namedtuple
from pprint import pprint
from time import strftime

from PySide.QtCore import QCoreApplication
from PySide.QtGui import QMessageBox

from src.tools.text_tools import safe_print

LOG_DIR = os.path.join(os.environ.get("USERPROFILE", ""), ".hpz", "log")
ConversionInfo = namedtuple("ConversionInfo", "errors launch_path")  # store the conversion result


def handle_errors(conversion_result, widget=None):
    """
    Handle the application errors, i.e. display the result and save error log if applicable.
    :param conversion_result: the conversion result
    :param widget: the parent UI widget
    """
    display_conversion_result(conversion_result, widget)
    save_error_log(conversion_result)


def display_conversion_result(conversion_result, widget):
    """
    Display the conversion result as a message box.
    :param conversion_result: the conversion result
    :param widget: the parent UI widget
    """
    title = QCoreApplication.translate("errorsupport", "result-title")

    if not conversion_result:
        content = QCoreApplication.translate("errorsupport", "result-success")
        return show_info(content, title, widget)

    max_len = 15
    file_list = sorted(conversion_result.keys()[:max_len])
    if len(conversion_result) > max_len:
        file_list.append(QCoreApplication.translate("errorsupport",
                                                    "result-errors-check-log"))
    content = (2 * os.linesep).join([QCoreApplication.translate("errorsupport",
                                                                "result-errors"),
                                     os.linesep.join(file_list),
                                     QCoreApplication.translate("errorsupport",
                                                                "result-errors-advice")])

    show_error(content, title, widget)


def save_error_log(conversion_result):
    """
    Save the file with the errors.
    The file errors_timestamp.log will be created in case the input result is not empty.
    :param conversion_result: the conversion result (a dictionary)
    """
    if not conversion_result:
        return

    timestamp = strftime("%Y%m%d_%H%M%S")
    filename = os.path.join(LOG_DIR, "errors_%s.log" % timestamp)
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError:  # guard against race condition
            print "Cannot create a log directory"
            return

    with open(filename, "w") as log_file:
        pprint(conversion_result, log_file)


def create_error_entry(info, trace):
    """
    Create the single readable error entry based on the given exception information and traceback.
    :param info: the exception information
    :param trace: the exception traceback
    :return: the readable exception converted to string
    """
    return [info] + traceback.format_tb(trace)


def show_error(content, title=None, widget=None):
    """
    Display the error in a message box in case the parent widget is provided or
    print it in the console otherwise.
    :param title: the error title
    :param content: the error content
    :param widget: the parent UI widget
    """
    if widget:
        QMessageBox.critical(widget, title, content)
    else:
        safe_print(QCoreApplication.translate("errorsupport", "error"))
        safe_print(_create_message(title, content))


def show_info(content, title=None, widget=None):
    """
    Display the information in a message box in case the parent widget is provided or
    print it in the console otherwise.
    :param title: the information title
    :param content: the information content
    :param widget: the parent UI widget
    """
    if widget:
        QMessageBox.information(widget, title, content)
    else:
        safe_print(_create_message(title, content))


def _create_message(title, content):
    """
    Combine title and content to the single message. If title is given, title: content is printed.
    Otherwise only the content is printed.
    :param title: the message title
    :param content: the message content
    :return: the single message
    """
    return ("%s: " % title if title else "") + content
