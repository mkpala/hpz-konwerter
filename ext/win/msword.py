"""
Module to handle operations with the Microsoft Word application.
"""

import os
import subprocess
import _winreg as reg

from PySide.QtCore import QCoreApplication

from ext.docxlauncher import DocXLauncher
from ext.errorsupport import show_error


class MSWordLauncher(DocXLauncher):
    """
    Class responsible for opening a file in MS Word.
    """
    REG_PATH_OFFICE = r"Software\Microsoft\Office"  # registry path for Microsoft Office
    REG_PATH_WORD = r"%s\Word\Options"  # registry path for MS Word options (%: version placeholder)
    REG_KEY_WORD = r"PROGRAMDIR"  # registry key for MS Word executable directory
    WORD_EXEC = r"WINWORD.EXE"  # MS Word executable file name
    WORD_MIN_VERSION = 12  # minimal acceptable MS Word version (as integer)

    def __init__(self, widget):
        """
        Instantiate the class.
        :param widget: the parent UI widget, to display the message boxes for
        """
        super(MSWordLauncher, self).__init__(widget)
        self.word_exec = ''  # the MS Word executable path (saved, once found)

    def launch(self, path):
        """
        Open the MS Word file.
        :param path: the path of the file to open
        """
        if not self.word_exec:
            self.word_exec = os.path.join(self._get_word_dir() or "", MSWordLauncher.WORD_EXEC)

        if not os.path.exists(self.word_exec):
            return show_error(QCoreApplication.translate("msword", "word-invalid"),
                              QCoreApplication.translate("msword", "cannot-launch"), self.widget)

        subprocess.Popen([self.word_exec, path])

    def _get_word_dir(self):
        """
        Retrieve the MS Word directory from the Windows registry.
        :return: the MS Word directory or None, if not found
        """
        try:
            registry = reg.ConnectRegistry(None, reg.HKEY_CURRENT_USER)
            key_office = reg.OpenKey(registry, MSWordLauncher.REG_PATH_OFFICE)
            office_versions = self._get_office_versions(key_office)
            for version in sorted(office_versions, reverse=True):
                word_dir = self._get_word_dir_for_version(key_office, office_versions[version])
                if word_dir:
                    return word_dir
        except EnvironmentError:
            return

    @staticmethod
    def _get_word_dir_for_version(reg_key_office, version):
        """
        Retrieve the MS Word directory for the given MS Office version.
        :param reg_key_office: the MS Office registry key
        :param version: the MS Office version
        :return: the MS Word directory or None, if not found
        """
        try:
            key_word = reg.OpenKey(reg_key_office, MSWordLauncher.REG_PATH_WORD % version)
            value, _ = reg.QueryValueEx(key_word, MSWordLauncher.REG_KEY_WORD)
            return value
        except EnvironmentError:
            return

    @staticmethod
    def _get_office_versions(reg_key_office):
        """
        Retrieve all MS Office versions from the Windows registry.
        :param reg_key_office: the MS Office registry key
        :return: the dictionary of all detected MS Office versions, where keys are
                 the versions as integers, and values contain the original version strings
        """
        office_versions = {}
        for index in range(1024):
            try:
                key_office_version = reg.EnumKey(reg_key_office, index)
                version_as_int = int(float(key_office_version))
                if version_as_int >= MSWordLauncher.WORD_MIN_VERSION:
                    office_versions[version_as_int] = key_office_version
            except ValueError:
                continue
            except EnvironmentError:
                break

        return office_versions
