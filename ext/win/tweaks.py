"""
Module for the Windows tweaks.
"""
import win32console


def set_console_encoding(code_page):
    """
    Set the console encoding to UTF-8.
    Return True in case the operation is successful or False otherwise.
    """
    win32console.SetConsoleCP(code_page)
    if win32console.GetConsoleCP() != code_page:
        return False
    win32console.SetConsoleOutputCP(code_page)
    if win32console.GetConsoleOutputCP() != code_page:
        return False
    return True
