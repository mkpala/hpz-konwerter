"""
The module to define a UI view (a layer between the Qt-generated UI and the UI controller).
"""

from PySide.QtCore import QCoreApplication
from PySide.QtCore import QEvent
from PySide.QtCore import Signal
from PySide.QtGui import QWidget
from ui.gen.main import Ui_Dialog

from ui.widgets import ListWidgetWithDropSupport


class View(QWidget, Ui_Dialog):
    """
    The main window class.
    """
    window_closing = Signal(QEvent)  # the signal emitted when a window is about to be closed

    def __init__(self, extensions=()):
        """
        The constructor of MainWindow. The UI is initialized and launched here.
        :param extensions: the file extensions to support (all by default)
        """

        # initialize and setup the UI
        super(View, self).__init__()
        self.setupUi(self)

        # redeclare the widgets to get independent from the generated code
        self.window_layout = self.gridLayout
        self.list_widget = self.itemList
        self.button_list_item_remove = self.removeItemButton
        self.button_list_item_add = self.addItemButton
        self.button_convert = self.convertButton
        self.progress_bar_convert = self.convertProgressBar

        # customize the UI
        self.setup_ui_customization(self, extensions)

    def setup_ui_customization(self, widget, extensions):
        """
        Customize the UI.
        The list widget is replaced with the one with drop support.
        Internationalization is applied.
        :param widget: the entry-point widget
        :param extensions: the file extensions to support
        """

        # apply custom list widget
        self.list_widget.destroy()
        self.list_widget = ListWidgetWithDropSupport(widget, extensions)
        self.window_layout.addWidget(self.list_widget, 0, 0, 1, 1)

        # ensure progress bar is not visible
        self.progress_bar_convert.setVisible(False)

        # setup internationalization
        self.setup_i18n()

    def setup_i18n(self):
        """
        Internationalize the UI labels.
        """
        self.button_convert.setText(QCoreApplication.translate("view", "btn-convert"))
        self.button_list_item_remove.setText(
            QCoreApplication.translate("view", "btn-list-item-remove"))
        self.button_list_item_add.setText(QCoreApplication.translate("view", "btn-list-item-add"))
        self.setWindowTitle(QCoreApplication.translate("view", "window-title"))

    def closeEvent(self, event):
        """
        Handle the window close event by sending an additional window_closing signal.
        :param event: the window close event
        """
        self.window_closing.emit(event)
