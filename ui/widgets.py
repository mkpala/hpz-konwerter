"""
A module containing the custom UI widgets.
"""

import os

import scandir
from PySide.QtCore import QCoreApplication
from PySide.QtCore import QSize
from PySide.QtCore import Qt
from PySide.QtCore import Signal
from PySide.QtGui import QAbstractItemView
from PySide.QtGui import QListWidget
from PySide.QtGui import QMessageBox
from PySide.QtGui import QProgressDialog


class ListWidgetWithDropSupport(QListWidget):
    """
    The list widget to support dropping of the files with the given extension(s)
    or directories containing such files.
    """

    # a signal containing dropped URLs, which are valid in terms of supported extensions
    # and which are not yet in the list
    files_dropped = Signal(list)

    # a signal indicating that the list content has changed
    # sent by: addItem, takeItem
    content_changed = Signal()

    def __init__(self, parent=None, extensions=(), threshold_dir_size=1000):
        """
        Initialize the list widget.
        By default, only drop is supported and its default action is the CopyAction.
        Additionally, multiselection and sorting is enabled by default.
        :param parent: the parent container (widget)
        :param extensions: the supported file extensions (all by default)
        :param threshold_dir_size: the maximum directory file number acceptable without
                prompting the user (1000 by default)
        """

        # initialize
        super(ListWidgetWithDropSupport, self).__init__(parent)
        self.extensions = [ext.lower() for ext in extensions]
        self.threshold_dir_size = threshold_dir_size

        # customize QListWidget
        self.setAcceptDrops(True)
        self.setIconSize(QSize(72, 72))
        self.setDragDropMode(QAbstractItemView.DropOnly)
        self.setDefaultDropAction(Qt.CopyAction)
        self.setSelectionMode(QAbstractItemView.MultiSelection)
        self.setSortingEnabled(True)
        self.setAutoFillBackground(False)

        # cache the URLs (key: dragged URL, value: the files located under the dragged URL)
        # the cache is to avoid processing input files on every drag'n'drop related event
        self.url_cache = {}

    def dragEnterEvent(self, event):
        """
        The drag enter event action support.
        :param event: the drag enter event
        """
        valid_urls = self.get_urls_from_cache(event.mimeData().urls(), update=True)
        if not self.filter_existing_urls(valid_urls):
            return event.ignore()

        event.accept()

    def dragMoveEvent(self, event):
        """
        The drag move event action support.
        :param event: the drag move event
        """
        valid_urls = self.get_urls_from_cache(event.mimeData().urls())
        if not self.filter_existing_urls(valid_urls):
            return event.ignore()

        event.setDropAction(Qt.CopyAction)
        event.accept()

    def dropEvent(self, event):
        """
        The drop event action support.
        :param event: the drop event
        """
        valid_urls = self.filter_existing_urls(self.get_urls_from_cache(event.mimeData().urls()))
        if not valid_urls:
            return event.ignore()

        event.setDropAction(Qt.CopyAction)
        event.accept()
        self.files_dropped.emit(valid_urls)
        self.content_changed.emit()

    def addItem(self, *args, **kwargs):
        """
        Add a new item and emit the "content changed" signal.
        :param args: the input arguments
        :param kwargs: the input named arguments
        """
        super(ListWidgetWithDropSupport, self).addItem(*args, **kwargs)
        self.content_changed.emit()

    def add_items_if_not_in_list(self, *urls):
        """
        Add the given URLs to the list provided they are not in the list yet.
        The given URL is normalized (using os.path.normpath).
        :param urls: the item URLs to add
        """
        items = self.all_items()
        for url in urls:
            url = os.path.normpath(url)
            if not self.has_item(url, items):
                self.addItem(url)

    def takeItem(self, *args, **kwargs):
        """
        Remove an item and emit the "content changed" signal.
        :param args: the input arguments
        :param kwargs: the input named arguments
        """
        super(ListWidgetWithDropSupport, self).takeItem(*args, **kwargs)
        self.content_changed.emit()

    def all_items(self):
        """
        Retrieve all list widget items.
        :return: a list of all list items
        """
        return [self.item(i) for i in range(self.count())]

    def all_texts(self, items=None):
        """
        Retrieve all list widget item texts.
        :param items: the list items, from which the texts are retrieved;
                      if None, all_items is called
        :return: a list of all list item texts
        """
        if items is None:
            items = self.all_items()
        return [item.text() for item in items]

    def filter_existing_urls(self, urls):
        """
        Remove the URLs already existing in the list from the given URLs.
        :param urls: the input URLs
        :return: the list of those URLs, which are not yet in the list
        """
        return list(set(urls) - set(self.all_texts()))

    def get_urls_from_cache(self, urls, update=False):
        """
        Retrieve the URLs from cache.
        If the URLs are not in cache yet, cache them first.
        :param urls: the dragged URLs
        :param update: indicator whether the cache should be updated or not
        :return: the valid file URLs from cache
        """
        if update:
            new_urls = self.get_valid_urls(urls)
            if new_urls is not None:
                self.url_cache.update(new_urls)

        result = []
        for url in urls:
            cache_key = self.create_cache_key(url)
            if cache_key in self.url_cache:
                result += self.url_cache[cache_key]
        return result

    @staticmethod
    def create_cache_key(url):
        """
        Create the cache key for the given dragged URL.
        :param url: the URL being the drag and drop subject
        :return: the corresponding cache key
        """
        return url.path()

    def get_valid_urls(self, urls):
        """
        Retrieve the list of valid URLs from the given list of the dragged URLs.
        :param urls: the dragged URLs
        :return: the list of valid file URLs
        """

        # create a dictionary containing dragged files, each associated with the dragged URL
        dragged_files = {}
        for url in urls:
            dragged_files[self.create_cache_key(url)] = self.get_all_file_paths(url)

        # count the files and return, if there are no files to process
        f_num = sum([len(val) for val in dragged_files.values()])
        if not f_num:
            return dragged_files

        # prompt a user if they want to proceed
        if not self.is_user_about_to_continue(f_num):
            return

        # display progress dialog if many files are about to be processed
        progress = self.create_progress_dialog(f_num)

        # check all the file paths and append only those, which are valid
        result = {}
        index = 0
        for cache_key in dragged_files:
            result[cache_key] = []
            for f_path in dragged_files[cache_key]:
                index += 1
                if self.is_url_valid(f_path):
                    result[cache_key].append(f_path)

                # increment the progress bar and return, if canceled
                progress.setValue(index)
                if progress.wasCanceled():
                    return

        # set progress dialog to done and return the result
        progress.setValue(f_num)
        return result

    @staticmethod
    def get_all_file_paths(url):
        """
        Retrieve all file paths from the given dragged URL.
        :param url: the URLs being the subject of drag and drop
        :return: the list of paths of all the dragged files
        """
        result = []
        url_path = os.path.normpath(url.toLocalFile())
        if os.path.isfile(url_path):
            result.append(url_path)
        else:
            result.extend([os.path.normpath(os.path.join(root, f_name)) for root, _, files in
                           scandir.walk(url_path) for f_name in files])
        return result

    def is_user_about_to_continue(self, file_number):
        """
        Prompt a user in case a file number to process exceeds the threshold (THRESHOLD_DIR_SIZE).
        :param file_number: the number of files to process
        :return: True in case a user wants to continue of the number of files does not exceed
                the threshold, False otherwise
        """
        if file_number > self.threshold_dir_size:
            title = QCoreApplication.translate("widgets", "dnd-too-many-files")
            content = os.linesep.join((QCoreApplication.translate("widgets",
                                                                  "dnd-too-many-files-content"),
                                       QCoreApplication.translate("widgets",
                                                                  "prompt-proceed")))
            return QMessageBox.warning(self, title, content,
                                       buttons=QMessageBox.Yes | QMessageBox.No,
                                       defaultButton=QMessageBox.No) == QMessageBox.Yes

        return True

    def create_progress_dialog(self, size):
        """
        Create the progress dialog to show the progress of processing dragged files.
        Show the dialog in case the file number exceeds the threshold
        (THRESHOLD_DIR_SIZE divided by 10).
        :param size: the size of the progress dialog (= the number of the files to process)
        :return: the progress dialog instance
        """
        progress = QProgressDialog(
            QCoreApplication.translate("widgets", "dnd-processing-files-content"),
            QCoreApplication.translate("widgets", "btn-abort"), 0,
            size, self)
        progress.setWindowTitle(QCoreApplication.translate("widgets", "dnd-processing-files"))
        progress.setValue(0)
        if size > self.threshold_dir_size / 10:
            progress.setWindowModality(Qt.WindowModal)
            progress.show()
        return progress

    def is_url_valid(self, url):
        """
        Check whether the given URL is valid, i.e. whether it is a URL of an existing file,
        it is a file with supported extension and it is not already in the item list.
        :param url: the URL to check
        :return: True in case all the conditions mentioned above are met, False otherwise
        """
        if not url or not os.path.exists(url):
            return False

        if not self.extensions:
            return True

        return os.path.splitext(url)[1].lower() in self.extensions

    def has_item(self, url, items):
        """
        Check whether the given URL is already contained within the list.
        :param url: the URL to check
        :param items: the list items
        :return: True in case the URL is already contained within the list, False otherwise
        """
        return any([url == os.path.normpath(text) for text in self.all_texts(items)])
