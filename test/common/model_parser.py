"""
Module for the unit tests covering the src.common.model_parser module.
"""
import unittest
from collections import namedtuple

from testfixtures import compare

from src.common.model import Document, Paragraph
from src.model.model2docx import Model2MdParser
from test.tools.mocks import MockItem


def parse_mock(data):
    """
    The parse method mock.
    :param data: the input data
    :return: the input data
    """
    return data.data


FooType = namedtuple('FooType', 'data')


class Model2MdParserTest(unittest.TestCase):
    """
    The test case class.
    """

    def setUp(self):
        """
        Set up each test.
        """
        self.parser = Model2MdParser()

    def should_process_document_return_empty_string_for_none(self):
        """
        Test that process_document returns empty string in case None is given.
        """

        # given
        document = None

        # when
        result = self.parser.process_document(document)

        # then
        expected = ''
        compare(expected, result)

    def should_process_document_return_empty_string_for_other_object_than_document(self):
        """
        Test that process_document returns empty string in case not a Document is given.
        """

        # given
        document = Paragraph('foo')

        # when
        result = self.parser.process_document(document)

        # then
        expected = ''
        compare(expected, result)

    def should_process_document_join_processed_document_parts(self):
        """
        Test that process_document returns a result of processing given model items
        if all of the given items are contained within a parser dictionary.
        """

        # given
        input_items = ('foo', 'bar', '    ha! ')
        document = Document(map(MockItem, input_items))
        self.parser.parser_dict = {MockItem: parse_mock}

        # when
        result = self.parser.process_document(document)

        # then
        expected = ''.join(input_items)
        compare(expected, result)

    def should_process_document_exclude_invalid_document_parts(self):
        """
        Test that process_document returns a result of processing given model items
        if any of the given items are contained within a parser dictionary.
        All items not contained in the dictionary will be excluded from the result.
        """

        # given
        input_items = ('foo', 'bar', '    ha! ')
        document = Document(
            (MockItem(input_items[0]), MockItem(input_items[1]), FooType('foo data'),
             MockItem(input_items[2])))
        self.parser.parser_dict = {MockItem: parse_mock}

        # when
        result = self.parser.process_document(document)

        # then
        expected = ''.join(input_items)
        compare(expected, result)


def main():
    """
    The main function to run the tests.
    """
    unittest.defaultTestLoader.testMethodPrefix = 'should'
    unittest.main()


if __name__ == '__main__':
    main()
