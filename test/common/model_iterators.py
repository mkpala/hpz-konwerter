"""
Module for the unit tests covering the src.common.model module.
"""
import os
import unittest
from copy import deepcopy

from testfixtures import compare

from src.common.model import Text, Document, Paragraph
from src.common.model_iterators import TextContentWalker, TextStringItem, TextListItem

LS = os.linesep


class TextStringItemTest(unittest.TestCase):
    """
    Test suite for the `TextStringItem` class.
    """

    def should_constructor_raise_value_error_if_none_given(self):
        """
        Test that constructor raises ValueError if None is given.
        """

        # given
        input_arg = None

        # then
        self.assertRaises(ValueError, TextStringItem, input_arg)

    def should_constructor_raise_value_error_if_not_text_given(self):
        """
        Test that constructor raises ValueError if not a Text instance is given.
        """

        # given
        input_arg = Document()

        # then
        self.assertRaises(ValueError, TextStringItem, input_arg)

    def should_constructor_raise_value_error_if_not_text_with_string_given(self):
        """
        Test that constructor raises ValueError if not a Text instance containing a string is given.
        """

        # given
        input_arg = Text([Text("nested")])

        # then
        self.assertRaises(ValueError, TextStringItem, input_arg)

    @staticmethod
    def should_update_not_call_updater_is_none():
        """
        Test that update does not modify the text contents if updater is None.
        """

        # given
        result = Text("foo")
        expected = deepcopy(result)
        updater = None

        # when
        TextStringItem(result).update(updater)

        # then
        compare(expected, result)

    @staticmethod
    def should_update_not_call_updater_is_not_callable():
        """
        Test that update does not modify the text contents if updater is not callable.
        """

        # given
        result = Text("foo")
        expected = deepcopy(result)
        updater = 2

        # when
        TextStringItem(result).update(updater)

        # then
        compare(expected, result)

    @staticmethod
    def should_update_call_updater():
        """
        Test that update does modifies the text contents according to the given updater.
        """

        # given
        result = Text("bar")
        prefix = "foo_"
        expected = Text("foo_bar")

        def updater(text_to_update):
            """
            Updater, which simply prefixes each character sequence with "foo_" prefix.
            :param text_to_update: the text to update
            :return: the updated text
            """
            return prefix + text_to_update

        # when
        TextStringItem(result).update(updater)

        # then
        compare(expected, result)

    @staticmethod
    def should_set_new_text_content():
        """
        Test that set updates string contents.
        """

        # given
        item = TextStringItem(Text("foo"))
        expected = "dummy"

        # when
        item.set(expected)

        # then
        compare(expected, item.get())

    @staticmethod
    def should_get_return_text_content():
        """
        Test that get returns string contents.
        """

        # given
        text = Text("foo")
        expected = "foo"

        # when
        result = TextStringItem(text).get()

        # then
        compare(expected, result)


class TextListItemTest(unittest.TestCase):
    """
    Test suite for the `TextListItem` class.
    """

    def should_constructor_raise_index_error_if_negative_index_given(self):
        """
        Test that constructor raises IndexError if negative list index is given.
        """

        # given
        input_list = [Document(), Text("foo"), Text([Text("nested")]), None]
        input_index = -4

        # then
        self.assertRaises(IndexError, TextListItem, input_list, input_index)

    def should_constructor_raise_index_error_if_too_large_index_given(self):
        """
        Test that constructor raises IndexError if list index exceeding list length is given.
        """

        # given
        input_list = [Document(), Text("foo"), Text([Text("nested")]), None]
        input_index = 4

        # then
        self.assertRaises(IndexError, TextListItem, input_list, input_index)

    def should_constructor_raise_value_error_if_none_given(self):
        """
        Test that constructor raises ValueError if given index points to None.
        """

        # given
        input_list = [Document(), "foo", Text("bar"), None]
        input_index = 3

        # then
        self.assertRaises(ValueError, TextListItem, input_list, input_index)

    def should_constructor_raise_value_error_if_not_string_given(self):
        """
        Test that constructor raises ValueError if given index points to an element,
        which is not a string.
        """

        # given
        input_list = [Document(), "foo", Text("bar"), None]
        input_index = 2

        # then
        self.assertRaises(ValueError, TextListItem, input_list, input_index)

    @staticmethod
    def should_update_not_call_updater_is_none():
        """
        Test that update does not modify the text contents if updater is None.
        """

        # given
        result = [Text("foo"), "bar", Text([Text("dummy")])]
        index = 1
        expected = deepcopy(result)
        updater = None

        # when
        TextListItem(result, index).update(updater)

        # then
        compare(expected, result)

    @staticmethod
    def should_update_not_call_updater_is_not_callable():
        """
        Test that update does not modify the text contents if updater is not callable.
        """

        # given
        result = [Text("foo"), "bar", Text([Text("dummy")])]
        index = 1
        expected = deepcopy(result)
        updater = 2

        # when
        TextListItem(result, index).update(updater)

        # then
        compare(expected, result)

    @staticmethod
    def should_update_call_updater():
        """
        Test that update does modifies the text contents according to the given updater.
        """

        # given
        result = [Text("foo"), "bar", Text([Text("dummy")])]
        index = 1
        expected = [Text("foo"), "foo_bar", Text([Text("dummy")])]
        prefix = "foo_"

        def updater(text_to_update):
            """
            Updater, which simply prefixes each character sequence with "foo_" prefix.
            :param text_to_update: the text to update
            :return: the updated text
            """
            return prefix + text_to_update

        # when
        TextListItem(result, index).update(updater)

        # then
        compare(expected, result)

    @staticmethod
    def should_set_new_text_content():
        """
        Test that set updates string contents.
        """

        # given
        text_list = [Text("foo"), "bar", Text([Text("dummy")])]
        list_item = TextListItem(text_list, 1)
        expected = "dummy"

        # when
        list_item.set(expected)

        # then
        compare(expected, list_item.get())

    @staticmethod
    def should_get_return_text_content():
        """
        Test that get returns string contents.
        """

        # given
        text_list = [Text("foo"), "bar", Text([Text("dummy")])]
        index = 1
        expected = "bar"

        # when
        result = TextListItem(text_list, index).get()

        # then
        compare(expected, result)


class TextContentWalkerTest(unittest.TestCase):
    """
    Test suite for the `TextContentWalker` class.
    """

    @staticmethod
    def should_update_do_nothing_if_updater_is_none():
        """
        Test that update does not modify the text contents if updater is not given.
        """

        # given
        text = Text([Text("nested"), "top-level", Text(["list-level", Text(["low-level"])])])
        expected = Text([Text("nested"), "top-level", Text(["list-level", Text(["low-level"])])])

        # when
        result = TextContentWalker(text).update().get()

        # then
        compare(expected, result)

    @staticmethod
    def should_update_do_nothing_if_updater_is_not_function():
        """
        Test that update does not modify the text contents if updater is not a function.
        """

        # given
        text = Text([Text("nested"), "top-level", Text(["list-level", Text(["low-level"])])])
        expected = Text([Text("nested"), "top-level", Text(["list-level", Text(["low-level"])])])
        updater = 1

        # when
        result = TextContentWalker(text).update(updater).get()

        # then
        compare(expected, result)

    @staticmethod
    def should_update_replace_tuple_with_list():
        """
        Test that update replaces a tuple containing nested texts with a list,
        as an underlying iterator makes it mutable.
        """

        # given
        text = Text((Text("nested"), "top-level", Text(["list-level", Text(["low-level"])]),))
        expected = Text([Text("nested"), "top-level", Text(["list-level", Text(["low-level"])])])

        # when
        result = TextContentWalker(text).update(lambda x: x).get()

        # then
        compare(expected, result)

    @staticmethod
    def should_update_contents_recursively():
        """
        Test that update updates all text contents by applying provided updater recursively.
        """

        # given
        prefix = "foo_"

        def updater(text_to_update):
            """
            Updater, which simply prefixes each character sequence with "foo_" prefix.
            :param text_to_update: the text to update
            :return: the updated text
            """
            return prefix + text_to_update

        text = Text([Text("nested"), "top-level", Text(["list-level", Text(["low-level"])])])
        expected = Text([Text("%snested" % prefix), "%stop-level" % prefix,
                         Text(["%slist-level" % prefix, Text(["%slow-level" % prefix])])])

        # when
        result = TextContentWalker(text).update(updater).get()

        # then
        compare(expected, result)

    @staticmethod
    def should_update_ignore_non_text_items():
        """
        Test that update updates all text contents by applying provided updater recursively
        and ignores all those text items, which are neither strings nor Text instances.
        """

        # given
        suffix = "_updated"

        def updater(text_to_update):
            """
            Updater, which simply suffixes each character sequence with "_updater" suffix.
            :param text_to_update: the text to update
            :return: the updated text
            """
            return text_to_update + suffix

        text = Text(["Emphasis, aka italics, with ", Text("asterisks", italics=True),
                     " or ", Text("underscores", italics=True), ".", LS,
                     "Strong emphasis, aka bold, with ", Text("asterisks", bold=True),
                     " or ", Text("underscores", bold=True), ".", LS, Paragraph("content"),
                     "Combined emphasis with ", Text(("asterisks and ",
                                                      Text("underscores", italics=True)),
                                                     bold=True), ".", LS, Document(),
                     "Strikethrough uses two tildes. ", Text("Scratch this.",
                                                             strikethrough=True), LS,
                     Text(["Check last one ", Text("soo deep...", strikethrough=True)],
                          italics=True,
                          bold=True)])

        expected = Text(
            ["Emphasis, aka italics, with %s" % suffix, Text("asterisks%s" % suffix, italics=True),
             " or %s" % suffix, Text("underscores%s" % suffix, italics=True), ".%s" % suffix,
             LS + "%s" % suffix,
             "Strong emphasis, aka bold, with %s" % suffix, Text("asterisks%s" % suffix, bold=True),
             " or %s" % suffix, Text("underscores%s" % suffix, bold=True), ".%s" % suffix,
             LS + "%s" % suffix, Paragraph("content"),
             "Combined emphasis with %s" % suffix, Text(("asterisks and %s" % suffix,
                                                         Text("underscores%s" % suffix,
                                                              italics=True)),
                                                        bold=True), ".%s" % suffix,
             LS + "%s" % suffix, Document(),
             "Strikethrough uses two tildes. %s" % suffix, Text("Scratch this.%s" % suffix,
                                                                strikethrough=True),
             LS + "%s" % suffix,
             Text(
                 ["Check last one %s" % suffix, Text("soo deep...%s" % suffix, strikethrough=True)],
                 italics=True,
                 bold=True)])

        # when
        result = TextContentWalker(text).update(updater).get()

        # then
        compare(expected, result)

    @staticmethod
    def should_get_as_string_return_test_content():
        """
        Test that get_as_string retrieves pure text contents of the given Text.
        """

        # given
        text = Text(["Emphasis, aka italics, with ", Text("asterisks", italics=True),
                     " or ", Text("underscores", italics=True), ".", LS,
                     "Strong emphasis, aka bold, with ", Text("asterisks", bold=True),
                     " or ", Text("underscores", bold=True), ".", LS, Paragraph("content"),
                     "Combined emphasis with ", Text(("asterisks and ",
                                                      Text("underscores", italics=True)),
                                                     bold=True), ".", LS, Document(),
                     "Strikethrough uses two tildes. ", Text("Scratch this.",
                                                             strikethrough=True), LS,
                     Text(["Check last one ", Text("soo deep...", strikethrough=True)],
                          italics=True,
                          bold=True)])

        expected = LS.join(["Emphasis, aka italics, with asterisks or underscores.",
                            "Strong emphasis, aka bold, with asterisks or underscores.",
                            "Combined emphasis with asterisks and underscores.",
                            "Strikethrough uses two tildes. Scratch this.",
                            "Check last one soo deep..."])

        # when
        result = TextContentWalker(text).get_as_string()

        # then
        compare(expected, result)


def main():
    """
    The main function to run the tests.
    """
    unittest.defaultTestLoader.testMethodPrefix = 'should'
    unittest.main()


if __name__ == '__main__':
    main()
