"""
Module for the unit tests covering the src.model.model2docx module.
"""

import os
import unittest

from testfixtures import compare

from src.common.model import Document, Header, Paragraph, BlockQuote, Text, ListItem, List, \
    Link, Image, Code, Table, TableAlignment, TableVAlignment, HorizontalRule, TableCell, \
    TableCellStyle
from src.model.model2docx import Model2MdParser, Model2MdExtendedParser
from test.tools.mocks import MockParser, Spy

LS = os.linesep


class Model2MdParserTest(unittest.TestCase):
    """
    The test case class.
    """

    def setUp(self):
        self.parser = Model2MdParser()

    def should_process_header_return_empty_string_for_no_header(self):
        """
        Test that process_header returns an empty string if not a Header instance is given.
        """

        # given
        header = Document()
        expected = ''

        # when
        result = self.parser.process_header(header)

        # then
        compare(expected, result)

    def should_process_header_return_empty_string_for_empty_header(self):
        """
        Test that process_header returns an empty string if an empty Header is given.
        """

        # given
        header = Header()
        expected = ''

        # when
        result = self.parser.process_header(header)

        # then
        compare(expected, result)

    def should_process_header_return_empty_string_for_header_with_empty_string(self):
        """
        Test that process_header returns an empty string if
        a Header containing an empty string is given.
        """

        # given
        header = Header('')
        expected = ''

        # when
        result = self.parser.process_header(header)

        # then
        compare(expected, result)

    def should_process_header_return_header_level_one_for_header_with_no_level_specified(self):
        """
        Test that process_header converts Header containing text, with no level specified,
        to markdown header level one.
        """

        # given
        header = Header('Foo Text')
        expected = LS + '# Foo Text' + LS

        # when
        result = self.parser.process_header(header)

        # then
        compare(expected, result)

    def should_process_header_return_header_for_header_with_level_in_range(self):
        """
        Test that process_header converts Header containing text, with level between 1 and 6,
        to markdown header with the corresponding level.
        """

        # given
        header = Header('Foo Text', 4)
        expected = LS + '#### Foo Text' + LS

        # when
        result = self.parser.process_header(header)

        # then
        compare(expected, result)

    def should_process_header_return_header_level_one_for_header_with_level_too_low(self):
        """
        Test that process_header converts Header containing text, with level lower than 1,
        to markdown header level one.
        """

        # given
        header = Header('Foo Text', -3)
        expected = LS + '# Foo Text' + LS

        # when
        result = self.parser.process_header(header)

        # then
        compare(expected, result)

    def should_process_header_return_header_level_six_for_header_with_level_too_high(self):
        """
        Test that process_header converts Header containing text, with level higher than 6,
        to markdown header level six.
        """

        # given
        header = Header('Foo Text', 111)
        expected = LS + '###### Foo Text' + LS

        # when
        result = self.parser.process_header(header)

        # then
        compare(expected, result)

    def should_process_paragraph_return_empty_string_for_no_paragraph(self):
        """
        Test that process_paragraph returns an empty string if not a Paragraph instance is given.
        """

        # given
        paragraph = Document()
        expected = ''

        # when
        result = self.parser.process_paragraph(paragraph)

        # then
        compare(expected, result)

    def should_process_paragraph_return_empty_string_for_empty_paragraph(self):
        """
        Test that process_paragraph returns an empty string if empty Paragraph is given.
        """

        # given
        paragraph = Paragraph()
        expected = ''

        # when
        result = self.parser.process_paragraph(paragraph)

        # then
        compare(expected, result)

    def should_process_paragraph_return_empty_string_for_paragraph_with_empty_string(self):
        """
        Test that process_paragraph returns an empty string
        if Paragraph with an empty string is given.
        """

        # given
        paragraph = Paragraph('')
        expected = ''

        # when
        result = self.parser.process_paragraph(paragraph)

        # then
        compare(expected, result)

    def should_process_paragraph_return_string_representation_of_paragraph_content(self):
        """
        Test that process_paragraph returns a string representation of the paragraph content
        if Paragraph with an incompatible object is given.
        """

        # given
        paragraph = Paragraph(Document())
        expected = 'Document(parts=())' + LS

        # when
        result = self.parser.process_paragraph(paragraph)

        # then
        compare(expected, result)

    @staticmethod
    def should_process_paragraph_return_result_of_parsing_text_content():
        """
        Test that process_paragraph returns a result of parsing text content
        by applying process_text on it.
        """

        # given
        parse_text_result = 'parse text result'
        parser = MockParser(parse_text_result)
        paragraph = Paragraph(Text())
        expected = parse_text_result + LS

        # when
        result = parser.process_paragraph(paragraph)

        # then
        compare(expected, result)

    @staticmethod
    def should_process_paragraph_add_extra_newline_character_if_separate_is_true():
        """
        Test that process_paragraph returns a result of parsing text content
        by applying process_text on it and adds extra newline character
        if `separate` parameter is set True.
        """

        # given
        parse_text_result = 'parse text result'
        parser = MockParser(parse_text_result)
        paragraph = Paragraph(Text(), separate=True)
        expected = parse_text_result + 2 * LS

        # when
        result = parser.process_paragraph(paragraph)

        # then
        compare(expected, result)

    def should_process_block_quote_return_empty_string_for_no_block_quote(self):
        """
        Test that process_block_quote returns an empty string if not a BlockQuote instance is given.
        """

        # given
        block_quote = Document()
        expected = ''

        # when
        result = self.parser.process_block_quote(block_quote)

        # then
        compare(expected, result)

    def should_process_block_quote_return_empty_string_for_empty_block_quote(self):
        """
        Test that process_block_quote returns an empty string if an empty BlockQuote is given.
        """

        # given
        block_quote = BlockQuote()
        expected = ''

        # when
        result = self.parser.process_block_quote(block_quote)

        # then
        compare(expected, result)

    def should_process_block_quote_return_empty_string_for_block_quote_with_empty_string(self):
        """
        Test that process_block_quote returns an empty string
        if a BlockQuote with an empty string is given.
        """

        # given
        block_quote = BlockQuote('')
        expected = ''

        # when
        result = self.parser.process_block_quote(block_quote)

        # then
        compare(expected, result)

    def should_process_block_quote_return_string_representation_of_block_quote_content(self):
        """
        Test that process_block_quote returns string representation of BlockQuote content
        if an incompatible object is given.
        """

        # given
        block_quote = BlockQuote(Document())
        expected = LS + '> Document(parts=())' + LS

        # when
        result = self.parser.process_block_quote(block_quote)

        # then
        compare(expected, result)

    @staticmethod
    def should_process_block_quote_wrap_parsed_text_content():
        """
        Test that process_block_quote returns BlockQuote content wrapped in '>' characters
        if text content is given.
        """

        # given
        parse_text_result = 'parse text result'
        parser = MockParser(parse_text_result + LS + parse_text_result)
        block_quote = BlockQuote(Text())
        expected = '%(br)s> %(result)s%(br)s> %(result)s%(br)s' % {'br': LS,
                                                                   'result': parse_text_result}

        # when
        result = parser.process_block_quote(block_quote)

        # then
        compare(expected, result)

    def should_process_text_return_empty_string_for_no_text(self):
        """
        Test that process_text returns an empty string if not a Text instance is given.
        """

        # given
        text = Document()
        expected = ''

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_return_empty_string_for_empty_text(self):
        """
        Test that process_text returns an empty string if an empty Text is given.
        """

        # given
        text = Text()
        expected = ''

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_return_given_string(self):
        """
        Test that process_text returns the given string, if a string is given.
        """

        # given
        text = "Regular text"
        expected = "Regular text"

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_return_given_string_for_text_instance(self):
        """
        Test that process_text returns the given string, if Text is instantiated using a string.
        """

        # given
        text = Text("Regular text")
        expected = "Regular text"

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_wrap_italics_in_underscores(self):
        """
        Test that process_text wraps an underlying italic-styled text in underscore characters.
        """

        # given
        text = Text("Text in italics", italics=True)
        expected = "_Text in italics_"

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_wrap_bold_in_stars(self):
        """
        Test that process_text wraps an underlying bold-styled text in double stars.
        """

        # given
        text = Text("Text in bold", bold=True)
        expected = "**Text in bold**"

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_wrap_strikethrough_in_tildas(self):
        """
        Test that process_text wraps an underlying crossed-out text in double tilde characters.
        """

        # given
        text = Text("Text crossed out", strikethrough=True)
        expected = "~~Text crossed out~~"

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_wrap_code_in_quote(self):
        """
        Test that process_text wraps an underlying code-styled text in single quotes.
        """

        # given
        text = Text("Code snippet", code=True)
        expected = "`Code snippet`"

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_wrap_superscript_in_sup_tag(self):
        """
        Test that process_text wraps an underlying superscript text in <sup> tag.
        """

        # given
        text = Text("Text in superscript", superscript=True)
        expected = "<sup>Text in superscript</sup>"

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_wrap_subscript_in_sub_tag(self):
        """
        Test that process_text wraps an underlying subscript text in <sub> tag.
        """

        # given
        text = Text("Text in subscript", subscript=True)
        expected = "<sub>Text in subscript</sub>"

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_convert_all_texts_recursively(self):
        """
        Test that process_text converts all contained Text instances recursively.
        """

        # given
        text = Text(["Emphasis, aka italics, with ", Text("asterisks", italics=True),
                     " or ", Text("underscores", italics=True), ".", LS,
                     "Strong emphasis, aka bold, with ", Text("asterisks", bold=True),
                     " or ", Text("underscores", bold=True), ".", LS,
                     "Combined emphasis with ", Text(("asterisks and ",
                                                      Text("underscores", italics=True)),
                                                     bold=True), ".", LS,
                     "Strikethrough uses two tildes. ", Text("Scratch this.",
                                                             strikethrough=True)])
        expected = LS.join([
            "Emphasis, aka italics, with _asterisks_ or _underscores_.",
            "Strong emphasis, aka bold, with **asterisks** or **underscores**.",
            "Combined emphasis with **asterisks and _underscores_**.",
            "Strikethrough uses two tildes. ~~Scratch this.~~"])

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_text_convert_all_items_recursively(self):
        """
        Test that process_text converts all contained objects recursively
        by applying _process_item on each.
        """

        # given
        text = Text(["Emphasis, aka italics, with ", Text("asterisks", italics=True),
                     " or ", Text("underscores", italics=True), ".", LS,
                     "Strong emphasis, aka bold, with ", Text("asterisks", bold=True),
                     " or ", Text("underscores", bold=True), ".", Paragraph("!?"), LS,
                     "Combined emphasis with ", Text(("asterisks and ",
                                                      Text("underscores", italics=True)),
                                                     bold=True), ".", LS,
                     "Strikethrough uses two tildes. ", Text("Scratch this.",
                                                             strikethrough=True), Document(), LS,
                     Text(["Check last one ", Text("soo deep...", strikethrough=True)],
                          italics=True,
                          bold=True)])
        expected = LS.join([
            "Emphasis, aka italics, with _asterisks_ or _underscores_.",
            "Strong emphasis, aka bold, with **asterisks** or **underscores**.!?" + LS,
            "Combined emphasis with **asterisks and _underscores_**.",
            "Strikethrough uses two tildes. ~~Scratch this.~~",
            "_**Check last one ~~soo deep...~~**_"])

        # when
        result = self.parser.process_text(text)

        # then
        compare(expected, result)

    def should_process_list_return_empty_string_for_no_list(self):
        """
        Test that process_list returns an empty string if not a List instance is given.
        """

        # given
        input_list = Document()
        expected = ''

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_return_line_separator_for_empty_list(self):
        """
        Test that process_list returns a line separator if an empty List is given.
        """

        # given
        input_list = List()
        expected = LS

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_prefix_unordered_list_items_with_stars(self):
        """
        Test that process_list prefixes all list items of an unordered list with `*` characters.
        """

        # given
        input_list = List(["list item", "another item"])
        expected = LS + '* list item' + LS + '* another item' + LS

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_prefix_unordered_list_item_instances_with_stars(self):
        """
        Test that process_list prefixes all list items and ListItem instances
        of an unordered list with `*` characters.
        """

        # given
        input_list = List([ListItem("list item"), "another item"])
        expected = LS + '* list item' + LS + '* another item' + LS

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_prefix_ordered_list_items_with_digits(self):
        """
        Test that process_list prefixes all list items of an ordered list with digits.
        """

        # given
        input_list = List([ListItem("list item"), "another item"], ordered=True)
        expected = LS + '1. list item' + LS + '1. another item' + LS

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_with_nested_lists_with_items_only(self):
        """
        Test that process_list correctly handles nested ordered and unordered lists
        containing list items only.
        """

        # given
        input_list = List(["first unordered level 0",
                           ListItem("second unordered level 0",
                                    [List(
                                        ["first ordered level 1",
                                         ListItem(
                                             "second ordered level 1",
                                             [List([
                                                 "first unordered level 2"])]),
                                         ListItem(
                                             "third ordered level 1",
                                             [List(
                                                 [
                                                     "first ordered level 2",
                                                     "second ordered level 2"],
                                                 ordered=True)])],
                                        ordered=True)]),
                           "third unordered level 0"])
        expected = LS + LS.join(["* first unordered level 0",
                                 "* second unordered level 0",
                                 "",
                                 "  1. first ordered level 1",
                                 "  1. second ordered level 1",
                                 "",
                                 "    * first unordered level 2",
                                 "  1. third ordered level 1",
                                 "",
                                 "    1. first ordered level 2",
                                 "    1. second ordered level 2",
                                 "* third unordered level 0"]) + LS

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_with_nested_lists_with_items_and_content(self):
        """
        Test that process_list correctly handles nested ordered and unordered lists
        containing list items and some content.
        """

        # given
        input_list = List([ListItem("First ordered list item"),
                           ListItem("Another item",
                                    [List([ListItem("Unordered sub-list.")])]),
                           ListItem("Actual numbers don't matter, just that it's a number",
                                    [List([ListItem("Ordered sub-list")], ordered=True)]),
                           ListItem("And another item.",
                                    [Paragraph("You can have properly indented paragraphs within "
                                               "list items. Notice the blank line above, and the "
                                               "leading spaces (at least one, but we'll use three "
                                               "here to also align the raw Markdown)."),
                                     Text(
                                         "To have a line break without a paragraph, you will need "
                                         "to use two trailing spaces." +
                                         LS + "Note that this line is separate, but within "
                                              "the same paragraph." + LS +
                                         "(This is contrary to the typical GFM line break "
                                         "behaviour, where trailing spaces are not required.)")])],
                          ordered=True)
        expected = LS + LS.join(["1. First ordered list item",
                                 "1. Another item",
                                 "",
                                 r"  * Unordered sub\-list.",
                                 "1. Actual numbers don't matter, just that it's a number",
                                 "",
                                 r"  1. Ordered sub\-list",
                                 "1. And another item.",
                                 "",
                                 "   You can have properly indented paragraphs within list "
                                 "items. Notice the blank line above, and the leading spaces "
                                 "(at least one, but we'll use three here to also align the "
                                 "raw Markdown).",
                                 "",
                                 "   To have a line break without a paragraph, you will need "
                                 "to use two trailing spaces.  ",
                                 "   Note that this line is separate, but within the same "
                                 "paragraph.  ",
                                 "   (This is contrary to the typical GFM line break "
                                 "behaviour, where trailing spaces are not required.)"])

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_ignore_incompatible_items(self):
        """
        Test that process_list ignores those list items, which hold incompatible types.
        """

        # given
        input_list = List(["list item", Document(), "another item"])
        expected = LS + '* list item' + LS + '* another item' + LS

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_ignore_incompatible_content(self):
        """
        Test that process_list ignores those list item contents, which hold incompatible types.
        """

        # given
        input_list = List(["list item", "another item",
                           ListItem("third",
                                    [Document(), "foo" + LS + "bar"])])
        expected = LS + LS.join(
            ['* list item', '* another item', '* third', '   foo  ' + LS + '   bar'])

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_wrap_items_in_tuple(self):
        """
        Test that process_list automatically wraps list items in a tuple.
        """

        # given
        input_list = List("list item")
        expected = LS + '* list item' + LS

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_list_wrap_item_contents_in_tuple(self):
        """
        Test that process_list automatically wraps list item contents in a tuple.
        """

        # given
        input_list = List([ListItem("list item", "contents")])
        expected = LS + '* list item' + LS + '   contents'

        # when
        result = self.parser.process_list(input_list)

        # then
        compare(expected, result)

    def should_process_link_return_empty_string_for_no_link(self):
        """
        Test that process_link returns an empty string if not a Link instance is given.
        """

        # given
        link = Document()
        expected = ''

        # when
        result = self.parser.process_link(link)

        # then
        compare(expected, result)

    def should_process_link_return_empty_string_for_empty_link(self):
        """
        Test that process_link returns an empty string if an empty Link is given.
        """

        # given
        link = Link()
        expected = ''

        # when
        result = self.parser.process_link(link)

        # then
        compare(expected, result)

    def should_process_link_return_empty_string_for_empty_alias(self):
        """
        Test that process_link returns an empty string if an empty link alias is given.
        """

        # given
        link = Link('', 'foo target')
        expected = ''

        # when
        result = self.parser.process_link(link)

        # then
        compare(expected, result)

    def should_process_link_return_empty_string_for_empty_target(self):
        """
        Test that process_link returns an empty string if an empty link target is given.
        """

        # given
        link = Link('foo label')
        expected = ''

        # when
        result = self.parser.process_link(link)

        # then
        compare(expected, result)

    def should_process_link_return_markdown_link(self):
        """
        Test that process_link returns a markdown link if both alias and target are given.
        """

        # given
        link = Link('foo label', 'foo target')
        expected = '[foo label][foo target]'

        # when
        result = self.parser.process_link(link)

        # then
        compare(expected, result)

    def should_process_image_return_empty_string_for_no_image(self):
        """
        Test that process_image returns an empty string if not an Image instance is given.
        """

        # given
        image = Document()
        expected = ''

        # when
        result = self.parser.process_image(image)

        # then
        compare(expected, result)

    def should_process_image_return_empty_string_for_empty_image(self):
        """
        Test that process_image returns an empty string if an empty Image is given.
        """

        # given
        image = Image()
        expected = ''

        # when
        result = self.parser.process_image(image)

        # then
        compare(expected, result)

    def should_process_image_return_empty_string_for_empty_label(self):
        """
        Test that process_image returns an empty string if an empty Image label is given.
        """

        # given
        image = Image("foo source")
        expected = ''

        # when
        result = self.parser.process_image(image)

        # then
        compare(expected, result)

    def should_process_image_return_labeled_source_if_no_tooltip_given(self):
        """
        Test that process_image returns markdown representation of image source and label
        if no tooltip is given.
        """

        # given
        image = Image('foo source', 'foo label')
        expected = '![foo label](foo source)'

        # when
        result = self.parser.process_image(image)

        # then
        compare(expected, result)

    def should_process_image_return_labeled_source_with_tooltip(self):
        """
        Test that process_image returns markdown representation of image source, label and tooltip
        if all parameters are given.
        """

        # given
        image = Image('foo source', 'foo label', 'image tooltip')
        expected = '![foo label](foo source "image tooltip")'

        # when
        result = self.parser.process_image(image)

        # then
        compare(expected, result)

    def should_process_code_return_empty_string_for_no_code(self):
        """
        Test that process_code returns an empty string if not a Code instance is given.
        """

        # given
        code = Document()
        expected = ''

        # when
        result = self.parser.process_code(code)

        # then
        compare(expected, result)

    def should_process_code_return_empty_string_for_empty_code(self):
        """
        Test that process_code returns an empty string if an empty Code block is given.
        """

        # given
        code = Code()
        expected = ''

        # when
        result = self.parser.process_code(code)

        # then
        compare(expected, result)

    def should_process_code_return_code_block_with_no_language(self):
        """
        Test that process_code returns markdown code block no matter whether a language is provided.
        """

        # given
        code = Code('this is the text')
        expected = LS + '```' + LS + 'this is the text' + LS + '```' + LS

        # when
        result = self.parser.process_code(code)

        # then
        compare(expected, result)

    def should_process_code_return_code_block_with_language(self):
        """
        Test that process_code returns markdown code block also when a language is provided.
        """

        # given
        code = Code('this is the text', 'python')
        expected = LS + '```python' + LS + 'this is the text' + LS + '```' + LS

        # when
        result = self.parser.process_code(code)

        # then
        compare(expected, result)

    def should_process_table_return_empty_string_for_no_table(self):
        """
        Test that process_table returns an empty string if not a Table instance is given.
        """

        # given
        table = Document()
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_return_empty_string_for_empty_table(self):
        """
        Test that process_table returns an empty string if an empty Table is given.
        """

        # given
        table = Table()
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_return_empty_string_for_single_row(self):
        """
        Test that process_table returns an empty string if Table contains less than two rows.
        """

        # given
        table = Table([["row"]])
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_return_empty_string_for_no_columns(self):
        """
        Test that process_table returns an empty string if Table contains no columns.
        """

        # given
        table = Table([[], [], []])
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_return_empty_string_for_no_collection(self):
        """
        Test that process_table returns an empty string if Table is not instantiated
        with a collection.
        """

        # given
        table = Table("not a collection")
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_single_column(self):
        """
        Test that process_table correctly processes table with a single column,
        where each of the cells is automatically wrapped in a cell list.
        """

        # given
        table = Table(["row1", ["row2"], "row3"])
        expected = LS + LS.join(["row1", "---", "row2", "row3"]) + LS

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_inconsistent_number_of_columns_by_adjusting_to_header(self):
        """
        Test that process_table correctly processes table with an inconsistent number of columns
        by adjusting column number to the length of the header.
        """

        # given
        table = Table([["Table with", "inconsistent", "number", "of cells"],
                       ["render", "also", "nicely"],
                       ["but", "they", "may"],
                       ["lose", "some", "very", "important", "data"]])
        expected = LS + LS.join(["Table with | inconsistent | number | of cells",
                                 "--- | --- | --- | ---",
                                 "render | also | nicely | ",
                                 "but | they | may | ",
                                 "lose | some | very | important"]) + LS

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_add_default_alignments_for_missing_alignments(self):
        """
        Test that process_table correctly processes table with an inconsistent number of
        column alignments by adding default column alignment for each missing column.
        """

        # given
        table = Table([["Table", "with", "alignment"],
                       ["and a", "single", "row"]], TableAlignment.RIGHT)
        expected = LS + LS.join(["Table | with | alignment",
                                 "---: | --- | ---",
                                 "and a | single | row"]) + LS

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_column_alignments(self):
        """
        Test that process_table correctly processes table with column alignment collection.
        """

        # given
        table = Table([["Tables", "Are", "Cool"],
                       ["col 3 is", "right-aligned", "$1600"],
                       ["col 2 is", "centered", "$12"],
                       ["zebra stripes", "are neat", "$1"]],
                      (TableAlignment.NONE, TableAlignment.CENTER, TableAlignment.RIGHT))
        expected = LS + LS.join(["Tables | Are | Cool",
                                 "--- | :---: | ---:",
                                 r"col 3 is | right\-aligned | $1600",
                                 "col 2 is | centered | $12",
                                 "zebra stripes | are neat | $1"]) + LS

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_styled_texts(self):
        """
        Test that process_table correctly processes table with styled texts.
        """

        # given
        table = Table([["Markdown", "Less", "Pretty"],
                       [Text("Still", italics=True), Text("renders", code=True),
                        Text("nicely", bold=True)],
                       ["1", "2", "3"]])
        expected = LS + LS.join(["Markdown | Less | Pretty",
                                 "--- | --- | ---",
                                 "_Still_ | `renders` | **nicely**",
                                 "1 | 2 | 3"]) + LS

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_multiline_cells(self):
        """
        Test that process_table correctly processes table with multiline cells
        by adding HTML separator tag for each new line.
        """

        # given
        table = Table([["Multi", "Table"],
                       ["multiline cell" + LS + "with extra data", "single line"]])
        expected = LS + LS.join(["Multi | Table",
                                 "--- | ---",
                                 "multiline cell<br/>with extra data | single line"]) + LS

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_horizontal_rule_return_empty_string_for_no_horizontal_rule(self):
        """
        Test that process_horizontal_rule returns an empty string
        if not a HorizontalRule instance is given.
        """

        # given
        horizontal_rule = Document()
        expected = ''

        # when
        result = self.parser.process_horizontal_rule(horizontal_rule)

        # then
        compare(expected, result)

    def should_process_horizontal_rule_instance(self):
        """
        Test that process_horizontal_rule correctly generates markdown representation
        of a HorizontalRule instance.
        """

        # given
        horizontal_rule = HorizontalRule()
        expected = LS + '---' + LS

        # when
        result = self.parser.process_horizontal_rule(horizontal_rule)

        # then
        compare(expected, result)


class Model2MdExtendedParserTest(unittest.TestCase):
    """
    The test case class for Model2MdExtendedParser class.
    """

    def setUp(self):
        parser_fallback = Model2MdParser()
        self.parser = Model2MdExtendedParser(parser_fallback)
        self.fallback_parser_spy = Spy(parser_fallback)

    def should_process_header_call_fallback_parser(self):
        """
        Test that process_header delegates parse method call to Model2MdParser.parse_header.
        """

        # given
        header = Header()
        tracked_method = "parse_header"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_header(header)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_paragraph_call_fallback_parser(self):
        """
        Test that process_paragraph delegates parse method call to Model2MdParser.parse_paragraph.
        """

        # given
        paragraph = Paragraph()

        tracked_method = "parse_paragraph"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_paragraph(paragraph)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_block_quote_call_fallback_parser(self):
        """
        Test that process_block_quote delegates parse method call to
        Model2MdParser.parse_block_quote.
        """

        # given
        block_quote = BlockQuote()

        tracked_method = "parse_block_quote"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_block_quote(block_quote)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_text_call_fallback_parser(self):
        """
        Test that process_text delegates parse method call to Model2MdParser.parse_text.
        """

        # given
        text = Text()

        tracked_method = "parse_text"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_text(text)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_list_call_fallback_parser(self):
        """
        Test that process_list delegates parse method call to Model2MdParser.parse_list.
        """

        # given
        input_list = List()

        tracked_method = "parse_list"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_list(input_list)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_list_item_call_fallback_parser(self):
        """
        Test that process_list_item delegates parse method call to Model2MdParser.parse_list_item.
        """

        # given
        list_item = ListItem()

        tracked_method = "parse_list_item"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_list_item(list_item, 0, True)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_link_call_fallback_parser(self):
        """
        Test that process_link delegates parse method call to Model2MdParser.parse_link.
        """

        # given
        link = Link()

        tracked_method = "parse_link"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_link(link)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_image_call_fallback_parser(self):
        """
        Test that process_image delegates parse method call to Model2MdParser.parse_image.
        """

        # given
        image = Image()

        tracked_method = "parse_image"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_image(image)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_code_call_fallback_parser(self):
        """
        Test that process_code delegates parse method call to Model2MdParser.parse_code.
        """

        # given
        code = Code()

        tracked_method = "parse_code"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_code(code)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))

    def should_process_table_return_empty_string_for_no_table(self):
        """
        Test that process_table returns an empty string if not a Table instance is given.
        """

        # given
        table = Document()
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_return_empty_string_for_empty_table(self):
        """
        Test that process_table returns an empty string if an empty Table is given.
        """

        # given
        table = Table()
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_not_call_fallback_parser(self):
        """
        Test that process_image does not delegate parse method call to Model2MdParser.parse_table.
        """

        # given
        table = Table()

        tracked_method = "parse_table"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_table(table)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))

    def should_process_table_return_empty_string_for_single_row(self):
        """
        Test that process_table returns an empty string if Table contains less than two rows.
        """

        # given
        table = Table([["row"]])
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_return_empty_string_for_no_columns(self):
        """
        Test that process_table returns an empty string if Table contains no columns.
        """

        # given
        table = Table([[], [], []])
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_return_empty_string_for_no_collection(self):
        """
        Test that process_table returns an empty string if Table is not instantiated
        with a collection.
        """

        # given
        table = Table("not a collection")
        expected = ''

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_single_column(self):
        """
        Test that process_table correctly processes table with a single column,
        where each of the cells is automatically wrapped in a cell list.
        """

        # given
        table = Table(["row1", ["row2"], "row3"])
        expected = LS + '<table><thead><tr><th>row1</th></tr></thead><tbody><tr><td>row2</td>' \
                        '</tr><tr><td>row3</td></tr></tbody></table><br>'

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_inconsistent_number_of_columns_by_rendering_as_is(self):
        """
        Test that process_table correctly processes table with an inconsistent number of columns
        by converting them as-is to the HTML output.
        """

        # given
        table = Table([["Table with", "inconsistent", "number", "of cells"],
                       ["render", "also", "nicely"],
                       ["but", "they", "may"],
                       ["lose", "some", "very", "important", "data"]])
        expected = LS + "<table><thead><tr><th>Table with</th><th>inconsistent</th><th>number" \
                        "</th><th>of cells</th></tr></thead>" \
                        "<tbody><tr><td>render</td><td>also</td><td>nicely</td></tr>" \
                        "<tr><td>but</td><td>they</td><td>may</td></tr>" \
                        "<tr><td>lose</td><td>some</td><td>very</td><td>important</td>" \
                        "<td>data</td></tr></tbody></table><br>"

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_add_default_alignments_for_missing_alignments(self):
        """
        Test that process_table correctly processes table with an inconsistent number of
        column alignments by adding default column alignment for each missing column.
        """

        # given
        table = Table([["Table", "with", "alignment"],
                       ["and a", "single", "row"]], TableAlignment.RIGHT)
        expected = LS + '<table><thead><tr><th style="text-align:right">Table</th><th>with' \
                        '</th><th>alignment</th></tr></thead>' \
                        '<tbody><tr><td style="text-align:right">and a</td><td>single</td>' \
                        '<td>row</td></tr></tbody></table><br>'

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_column_alignments(self):
        """
        Test that process_table correctly processes table with column alignment collection.
        """

        # given
        table = Table([["Tables", "Are", "Cool"],
                       ["col 3 is", "right-aligned", "$1600"],
                       ["col 2 is", "centered", "$12"],
                       ["zebra stripes", "are neat", "$1"]],
                      (TableAlignment.NONE, TableAlignment.CENTER, TableAlignment.RIGHT))
        expected = LS + '<table><thead><tr><th>Tables</th><th style="text-align:center">Are' \
                        '</th><th style="text-align:right">Cool</th></tr></thead><tbody><tr>' \
                        r'<td>col 3 is</td><td style="text-align:center">right\-aligned</td>' \
                        '<td style="text-align:right">$1600</td></tr>' \
                        '<tr><td>col 2 is</td><td style="text-align:center">centered</td>' \
                        '<td style="text-align:right">$12</td></tr>' \
                        '<tr><td>zebra stripes</td><td style="text-align:center">are neat</td>' \
                        '<td style="text-align:right">$1</td></tr></tbody></table><br>'

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_styled_texts(self):
        """
        Test that process_table correctly processes table with styled texts.
        """

        # given
        table = Table([["Markdown", "Less", "Pretty", "With Styles"],
                       [Text("Still", italics=True), Text("renders", code=True),
                        Text("nicely", bold=True), Text("indeed", strikethrough=True)],
                       [Text(["1", Text("2", subscript=True)]), "2",
                        Text(["3", Text("4", superscript=True)]), ""]])

        expected = LS + '<table><thead><tr><th>Markdown</th><th>Less</th><th>Pretty</th>' \
                        '<th>With Styles</th></tr></thead>' \
                        '<tbody><tr><td><em>Still</em></td><td><code>renders</code></td>' \
                        '<td><strong>nicely</strong></td><td><s>indeed</s></td></tr>' \
                        '<tr><td>1<sub>2</sub></td><td>2</td><td>3<sup>4</sup></td>' \
                        '<td></td></tr></tbody></table><br>'

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_multiline_cells(self):
        """
        Test that process_table correctly processes table with multiline cells
        by adding HTML separator tag for each new line.
        """

        # given
        table = Table([["Multi", "Table"],
                       ["multiline cell" + LS + "with extra data", "single line"]])
        expected = LS + '<table><thead><tr><th>Multi</th><th>Table</th></tr></thead>' \
                        '<tbody><tr><td>multiline cell<br/>with extra data</td><td>single line' \
                        '</td></tr></tbody></table><br>'

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_with_rowspans_and_colspans(self):
        """
        Test that process_table correctly processes table with rows and columns spanned.
        """

        # given
        table = Table([["Table", "With", "Rowspan", "Colspan"],
                       ["1x1",
                        TableCell("2x2", rowspan=2, colspan=2,
                                  style=TableCellStyle(valign=TableVAlignment.MIDDLE,
                                                       align=TableAlignment.CENTER)),
                        TableCell("1x2", rowspan=2,
                                  style=TableCellStyle(valign=TableVAlignment.TOP,
                                                       align=TableAlignment.LEFT))],
                       ["1x1"],
                       [TableCell("1x1", style=TableCellStyle(valign=TableVAlignment.BOTTOM)),
                        "1x1",
                        TableCell("2x1", colspan=2,
                                  style=TableCellStyle(align=TableAlignment.RIGHT))],
                       [TableCell("3x1", colspan=3),
                        TableCell("1x1",
                                  style=TableCellStyle(valign=TableVAlignment.BASELINE))]])
        expected = LS + '<table><thead><tr><th>Table</th><th>With</th><th>Rowspan</th>' \
                        '<th>Colspan</th></tr></thead>' \
                        '<tbody><tr><td>1x1</td><td rowspan=2 colspan=2 ' \
                        'style="text-align:center;vertical-align:middle">2x2</td>' \
                        '<td rowspan=2 style="text-align:left;vertical-align:top">1x2</td>' \
                        '</tr><tr><td>1x1</td></tr>' \
                        '<tr><td style="vertical-align:bottom">1x1</td><td>1x1</td>' \
                        '<td colspan=2 style="text-align:right">2x1</td></tr>' \
                        '<tr><td colspan=3>3x1</td><td style="vertical-align:baseline">1x1' \
                        '</td></tr></tbody></table><br>'

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_table_ignore_invalid_cell_contents(self):
        """
        Test that process_table ignores invalid cell contents and renders an empty cell instead.
        """

        # given
        table = Table([["Table", "With", "Fancy", "texts"],
                       [Text(["more lines ", "in one ", "cell"]),
                        Text(999), Table(), "<cell>"]])
        expected = LS + '<table><thead><tr><th>Table</th><th>With</th><th>Fancy</th><th>texts' \
                        '</th></tr></thead><tbody><tr><td>more lines in one cell</td>' \
                        r'<td></td><td></td><td>\<cell\></td></tr></tbody></table><br>'

        # when
        result = self.parser.process_table(table)

        # then
        compare(expected, result)

    def should_process_horizontal_rule_call_fallback_parser(self):
        """
        Test that process_horizontal_rule delegates parse method call to
        Model2MdParser.parse_horizontal_rule.
        """

        # given
        horizontal_rule = HorizontalRule()

        tracked_method = "parse_horizontal_rule"
        self.fallback_parser_spy.track(tracked_method)

        # when
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method, 0))
        self.parser.process_horizontal_rule(horizontal_rule)

        # then
        self.assertTrue(self.fallback_parser_spy.verify(tracked_method))


def main():
    """
    The main function to run the tests.
    """
    unittest.defaultTestLoader.testMethodPrefix = 'should'
    unittest.main()


if __name__ == '__main__':
    main()
