# -*- coding: utf-8 -*-
"""
Module for the unit tests covering the src.model.model_preprocessor module.
"""
import os
import unittest

from testfixtures import compare

from src.common.model import TableCell, Text
from src.model.model_preprocessor import ModelPreprocessor
from test.tools.mocks import Spy

LS = os.linesep


class ModelPreprocessorTest(unittest.TestCase):
    """
    Test suite for the `ModelPreprocessor` class.
    """

    def setUp(self):
        self.visitor = ModelPreprocessor()

    def should_visit_table_cell_return_if_no_table_cell(self):
        """
        Test that visit_table_cell immediately returns if not a TableCell instance is given.
        """

        # given
        visitor_spy = Spy(self.visitor)
        visitor_spy.track("new_walker")
        table_cell = "foo"

        # when
        self.visitor.visit_table_cell(table_cell)

        # then
        visitor_spy.verify("new_walker", 0)

    def should_visit_table_cell_call_text_walker_if_table_cell_given(self):
        """
        Test that visit_table_cell instantiates and calls TextWalker if TableCell instance is given.
        """

        # given
        visitor_spy = Spy(self.visitor)
        visitor_spy.track("new_walker")
        table_cell = TableCell()

        # when
        self.visitor.visit_table_cell(table_cell)

        # then
        visitor_spy.verify("new_walker")

    def should_visit_table_cell_replace_all_circumstances_in_string_content(self):
        """
        Test that visit_table_cell replaces all O K O L I C Z N O Ś C I P O M I A R Ó W
        with the one without spaces for TableCell containing a string.
        """

        # given
        table_cell = TableCell("top-level O K O L I C Z N O Ś C I P O M I A R Ó W top")
        expected = TableCell(Text("top-level OKOLICZNOŚCI POMIARÓW top"))

        # when
        self.visitor.visit_table_cell(table_cell)

        # then
        compare(expected, table_cell)

    def should_visit_table_cell_replace_all_circumstances_in_text_instance(self):
        """
        Test that visit_table_cell replaces all O K O L I C Z N O Ś C I P O M I A R Ó W
        with the one without spaces for TableCell containing Text instance.
        """

        # given
        table_cell = TableCell(
            Text([Text("nested"), "top-level O K O L I C Z N O Ś C I P O M I A R Ó W top", Text(
                ["list-levelO K O L I C Z N O Ś C I P O M I A R Ó W",
                 Text(["O K O L I C Z N O Ś C I P O M I A R Ó Wlow-level"])])]))
        expected = TableCell(
            Text([Text("nested"), "top-level OKOLICZNOŚCI POMIARÓW top", Text(
                ["list-levelOKOLICZNOŚCI POMIARÓW",
                 Text(["OKOLICZNOŚCI POMIARÓWlow-level"])])]))

        # when
        self.visitor.visit_table_cell(table_cell)

        # then
        compare(expected, table_cell)


def main():
    """
    The main function to run the tests.
    """
    unittest.defaultTestLoader.testMethodPrefix = 'should'
    unittest.main()


if __name__ == '__main__':
    main()
