"""
Package to collect mocks.
"""
from collections import defaultdict
from copy import deepcopy

from src.common.model import Item
from src.model.model2docx import Model2MdParser


class MockParser(Model2MdParser):
    """
    Mock of Model2MdParser.
    """

    def __init__(self, expected_result):
        """
        Instantiate the mock.
        :param expected_result: expected result of the stubbed method
        """
        super(MockParser, self).__init__()
        self.expected_result = expected_result
        self.process_text = self.process_text_stub

    def process_text_stub(self, _text):
        """
        Stub of process_text method for testing purposes.
        :param _text: parameter to match method signature
        :return: predefined text (parse_text_result\nparse_text_result)
        """
        return self.expected_result


class MockItem(Item):
    """
    Mock of Item.
    """

    def __init__(self, data=None):
        """
        Instantiate the mock.
        :param data: item data
        """
        self.data = data

    def accept(self, visitor):
        """
        Simply return the item.
        :param visitor: visitor to accept
        :return: this item
        """
        return self


class Spy(object):
    """
    Create a spy to track the given target object instance.
    """

    def __init__(self, target):
        """
        Instantiate the spy.
        :param target: an instance to create spy for
        """
        self.target = target
        self.execution_count = defaultdict(int)

    def track(self, *method_names):
        """
        Track the given methods by creating a stub, which tracks a number of method calls.
        :param method_names: names of the methods to track
        """
        _ = [self.__track(method_name) for method_name in method_names]

    def __track(self, method_name):
        """
        Track the given method by creating a stub, which tracks a number of method calls.
        :param method_name: name of the method to track
        """
        try:
            tracked_method = getattr(deepcopy(self.target), method_name)

            def stub(*args, **kwargs):
                """
                Stub to replace the tracked method.
                :param args: method arguments
                :param kwargs: named method arguments
                :return tracked method call result
                """
                self.execution_count[method_name] += 1
                return tracked_method(*args, **kwargs)

            setattr(self.target, method_name, stub)

        except AttributeError:
            raise ValueError(
                "Cannot track method '%s' as it is not defined in the target object" % method_name)

    def verify(self, method_name, expected_call_count=1):
        """
        Verify whether the given method has been called an amount of times
        specified by expected_call_count.
        :param method_name: name of the method to verify
        :param expected_call_count: an expected amount of method calls
        :return: True in case the expected method call count is reached, False otherwise
        """
        return self.execution_count[method_name] == expected_call_count
