# -*- coding: utf-8 -*-
"""
Module for the unit tests covering the src.input.drk2model module.
"""

import os
import unittest

from testfixtures import compare

from src.common.model import Header, Paragraph, Document, Text, Table, TableCell, \
    HorizontalRule
from src.config import Measurement
from src.input.drk2model import Block, HeaderLevelThree, HeaderLevelOne, HeaderLevelTwo, \
    Drk2ModelParser

LS = os.linesep  # shortcut for the line separator


class Drk2ModelParserTest(unittest.TestCase):
    """
    The test case class.
    """

    def __init__(self, methodName='runTest'):
        """
        The class initializer.
        MaxDiff is set to None here in order to display the full differences
        in case of failing assertions.
        :param methodName: the unit test method name
        """
        super(Drk2ModelParserTest, self).__init__(methodName)
        self.maxDiff = None
        self.parser = Drk2ModelParser()

    def should_parse_return_empty_document_for_none_text(self):
        """
        Test that parse returns an empty Document in case None is given.
        """

        # given
        input_text = None
        expected = Document()

        # when
        result = self.parser.parse(input_text)

        # then
        compare(expected, result)

    def should_parse_return_empty_document_for_no_text(self):
        """
        Test that parse returns an empty Document in case an empty string is given.
        """

        # given
        input_text = ''
        expected = Document()

        # when
        result = self.parser.parse(input_text)

        # then
        compare(expected, result)

    def should_parse_return_document_filled_with_data_for_given_text(self):
        """
        Test that parse returns a Document filled with data corresponding to the given text.
        """

        # given
        input_text = """
BŚP Usługi BHP Krzysztof Pala

0

5

0



003/DFO/13

ŰSPAWALNIA !1 !- !ZT-1 V–

ŰLinia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–

ŰSektor !1, !op. !10-20 U–

Ű !- !zgrzewacz !- Z–

đ------------------------------

ŰStwierdzenie !zgodności/niezgodności !wyników !z !wymaganiami:đ

Poziom !ekspozycji !na !hałas !nie !przekracza !wartości !dopuszczalnej. (–

Maksymalny !poziom !dźwięku !A !nie !przekracza !wartości !dopuszczalnej. '–

Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości !dopuszczalnej. (–

------------------------------

 !

Ű** !HAŁAS !** !đ



Data !pomiarów...........:07.01.2013 #
"""

        expected = Document([
            Header("SPAWALNIA 1 - ZT-1", level=1),
            Header("Linia zrobotyzowana - ZASTRZAŁ PRZEDNI LEWY 312/420", level=2),
            Paragraph(Text([Text(["Sektor 1, op. 10-20"], bold=True),
                            ' ',
                            Text(["- zgrzewacz -"], bold=True)])),
            HorizontalRule(),
            Paragraph(Text([
                Text(
                    ["Stwierdzenie zgodności/niezgodności wyników z "
                     "wymaganiami:"],
                    bold=True),
                LS,
                Text(
                    ["Poziom ekspozycji na hałas nie przekracza "
                     "wartości dopuszczalnej."]),
                LS,
                Text(
                    ["Maksymalny poziom dźwięku A nie przekracza "
                     "wartości dopuszczalnej."]),
                LS,
                Text(
                    ["Szczytowy poziom dźwięku C nie przekracza "
                     "wartości dopuszczalnej."])])),
            HorizontalRule(),
            Header("HAŁAS", level=3),
            Paragraph(Text([
                Text(["Data pomiarów...........:"
                      "07.01.2013"])]))]), "003/DFO/13"

        # when
        result = self.parser.parse(input_text)

        # then
        compare(expected, result)

    def should_remove_empty_lines_if_first_non_empty_line_is_odd(self):
        """
        Test that remove_empty_lines removes empty lines in case first non-empty line is odd.
        """

        # given
        input_text = """BŚP Usługi BHP Krzysztof Pala

0

5

0



003/DFO/13

SPAWALNIA !1 !- !ZT-1 V–
(just in case an even line is not empty)
Linia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–"""

        expected = ["BŚP Usługi BHP Krzysztof Pala",
                    "0",
                    "5",
                    "0",
                    "",
                    "003/DFO/13",
                    "SPAWALNIA !1 !- !ZT-1 V–",
                    "(just in case an even line is not empty)",
                    "Linia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–"]

        # when
        result = self.parser.remove_empty_lines(input_text)

        # then
        compare(expected, result)

    def should_remove_empty_lines_if_first_non_empty_line_is_even(self):
        """
        Test that remove_empty_lines removes empty lines in case first non-empty line is odd.
        """

        # given
        input_text = """
BŚP Usługi BHP Krzysztof Pala

0

5

0



003/DFO/13

SPAWALNIA !1 !- !ZT-1 V–
(just in case an odd line is not empty)
Linia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–"""

        expected = ["BŚP Usługi BHP Krzysztof Pala",
                    "0",
                    "5",
                    "0",
                    "",
                    "003/DFO/13",
                    "SPAWALNIA !1 !- !ZT-1 V–",
                    "(just in case an odd line is not empty)",
                    "Linia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–"]

        # when
        result = self.parser.remove_empty_lines(input_text)

        # then
        compare(expected, result)

    def should_trim_line_begin_remove_unsupported_characters_from_line_begin(self):
        """
        Test that trim_line_begin removes characters matching ur"^\x0f*\x12*".
        """

        # given
        input_line = "Linia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–"
        expected = "Linia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–"

        # when
        self.assertNotEqual(expected, input_line)  # input_line contains \x12 on the beginning
        result = self.parser.trim_line_begin(input_line)

        # then
        compare(expected, result)

    def should_remove_header_remove_four_first_lines(self):
        """
        Test that remove_header removes four first lines of the given text lines.
        """

        # given
        input_lines = ["BŚP Usługi BHP Krzysztof Pala",
                       "0",
                       "5",
                       "0",
                       "",
                       "003/DFO/13",
                       "ŰSPAWALNIA !1 !- !ZT-1 V–",
                       "ŰLinia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–"]

        expected = ["003/DFO/13",
                    "ŰSPAWALNIA !1 !- !ZT-1 V–",
                    "ŰLinia !zrobotyzowana !- !ZASTRZAŁ !PRZEDNI !LEWY !312/420 5–"]

        # when
        result = self.parser.remove_header(input_lines)

        # then
        compare(expected, result)

    def should_get_blocks_convert_given_text_into_logical_blocks(self):
        """
        Test that get_blocks converts the given text lines into Block instances.
        """

        # given
        input_lines = [
            "003/DFO/13",
            "ŰSPAWALNIA !1 !- !ZT-1 V–",
            "ŰStanowisko !z !wtryskarką !M - 1 M–",
            "Ű !- !operator !wtryskarki !- P–",
            "Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości !dopuszczalnej. (–",
            "------------------------------",
            " !",
            "Ű** !HAŁAS !** !đ",
            "",
            "Data !pomiarów...........:07.01.2013 #",
            "ČÉ$ÔÉLÔÉ'ÔÉ(ÔÉ&ÔÉ(ÔÉ(ÔÉ'ÔÉ,ÔÉ)Ë",
            "ĚL.P.Ě ,Wykonywane !zadanie .ĚCzas #ĚCzas $ĚWynikiĚ !Równo- !Ě !Maksym.ĚSzczyt.Ě"
            "!Poz.ekspoz.ĚKrotność !Ě",
            "ĐÉ$ÖÉLÖÉ'ÖÉ(ÖÉ&ÖÉ(ÖÉ(ÖÉ'ÖÉ,ÖÉ)Ň",
            "",
            "Podana !niepewność !wyników !pomiarów !jest !rozszerzoną !niepewnością",
            "złożoną !dla !poziomu !ufności !95% !(k=1.65).",
            "003/DFO/13",
            "ŰSPAWALNIA !1 !- !ZT-1 V–"]

        expected = [
            Block(type=Paragraph, lines=["003/DFO/13"]),
            Block(type=HeaderLevelOne, lines=["ŰSPAWALNIA !1 !- !ZT-1 V–"]),
            Block(type=HeaderLevelTwo, lines=["ŰStanowisko !z !wtryskarką !M - 1 M–"]),
            Block(type=Paragraph, lines=["Ű !- !operator !wtryskarki !- P–",
                                         "Szczytowy !poziom !dźwięku !C !nie !przekracza "
                                         "!wartości !dopuszczalnej. (–"]),
            Block(type=HorizontalRule, lines=["------------------------------"]),
            Block(type=Paragraph, lines=[" !"]),
            Block(type=HeaderLevelThree, lines=["Ű** !HAŁAS !** !đ"]),
            Block(type=Paragraph, lines=["", "Data !pomiarów...........:07.01.2013 #"]),
            Block(type=Table, lines=[
                "ČÉ$ÔÉLÔÉ'ÔÉ(ÔÉ&ÔÉ(ÔÉ(ÔÉ'ÔÉ,ÔÉ)Ë",
                "ĚL.P.Ě ,Wykonywane !zadanie .ĚCzas #ĚCzas $ĚWynikiĚ !Równo- !Ě !Maksym.ĚSzczyt.Ě"
                "!Poz.ekspoz.ĚKrotność !Ě",
                "ĐÉ$ÖÉLÖÉ'ÖÉ(ÖÉ&ÖÉ(ÖÉ(ÖÉ'ÖÉ,ÖÉ)Ň"]),
            Block(type=Paragraph, lines=[
                "",
                "Podana !niepewność !wyników !pomiarów !jest !rozszerzoną !niepewnością",
                "złożoną !dla !poziomu !ufności !95% !(k=1.65).",
                "003/DFO/13"]),
            Block(type=HeaderLevelOne, lines=["ŰSPAWALNIA !1 !- !ZT-1 V–"])]

        # when
        result = self.parser.get_blocks(input_lines)

        # then
        compare(expected, result)

    @staticmethod
    def should_parse_blocks_convert_valid_blocks_using_corresponding_parse_methods():
        """
        Test that parse_blocks converts the given blocks by applying parse_ methods
        corresponding to the block types.
        """

        # given
        def _dummy_parse(block):
            """
            Method stub for the parse_* methods.
            :param block: the block to parse
            :return the block type
            """
            return block.type

        parser = Drk2ModelParser()
        parser.parse_paragraph = _dummy_parse
        parser.parse_header_level_one = _dummy_parse
        parser.parse_header_level_two = _dummy_parse
        parser.parse_header_level_three = _dummy_parse
        parser.parse_table = _dummy_parse

        input_blocks = [Block(Table), Block(Table), Block(Paragraph), Block(HeaderLevelOne),
                        Block(Paragraph)]
        expected = [Table, Table, Paragraph, HeaderLevelOne, Paragraph]

        # when
        result = parser.parse_blocks(input_blocks)

        # then
        compare(expected, result)

    @staticmethod
    def should_parse_blocks_ignore_invalid_blocks():
        """
        Test that parse_blocks converts the given blocks by applying parse_ methods
        corresponding to the block types and ignores all blocks of invalid type.
        """

        # given
        def _dummy_parse(block):
            """
            Method stub for the parse_* methods.
            :param block: the block to parse
            :return the block type
            """
            return block.type

        parser = Drk2ModelParser()
        parser.parse_paragraph = _dummy_parse
        parser.parse_header_level_one = _dummy_parse
        parser.parse_header_level_two = _dummy_parse
        parser.parse_header_level_three = _dummy_parse
        parser.parse_table = _dummy_parse

        input_blocks = [Block(Table), Block(Table), Block(Paragraph), Block(HeaderLevelThree),
                        Block(Document), Block(Text), Block(Paragraph), Block(Text),
                        Block(HeaderLevelTwo)]
        expected = [Table, Table, Paragraph, HeaderLevelThree, Paragraph, HeaderLevelTwo]

        # when
        result = parser.parse_blocks(input_blocks)

        # then
        compare(expected, result)

    def should_parse_horizontal_rule_return_none_for_invalid_block(self):
        """
        Test that parse_horizontal_rule returns None in case a block of
        a different type than HorizontalRule is given.
        """

        # given
        input_block = Block(type=Paragraph, lines=["---------"])
        expected = None

        # when
        result = self.parser.parse_horizontal_rule(input_block)

        # then
        compare(expected, result)

    def should_parse_horizontal_rule_convert_horizontal_rule(self):
        """
        Test that parse_horizontal_rule converts block of type HorizontalRule to
        a HorizontalRule instance.
        """

        # given
        input_block = Block(type=HorizontalRule, lines=["---------"])
        expected = HorizontalRule()

        # when
        result = self.parser.parse_horizontal_rule(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_one_return_none_for_no_header(self):
        """
        Test that parse_header_level_one returns None in case a block of type HeaderLevelOne
        with an empty collection is given.
        """

        # given
        input_block = Block(type=HeaderLevelOne, lines=[])
        expected = None

        # when
        result = self.parser.parse_header_level_one(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_one_return_none_for_header_not_matching_pattern(self):
        """
        Test that parse_header_level_one returns None in case a block of type HeaderLevelOne
        with contents not matching header pattern is given.
        """

        # given
        input_block = Block(type=HeaderLevelOne, lines=["Űđ"])
        expected = None

        # when
        result = self.parser.parse_header_level_one(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_one_return_none_for_empty_header(self):
        """
        Test that parse_header_level_one returns None in case a block of type HeaderLevelOne
        with no contents is given.
        """

        # given
        input_block = Block(type=HeaderLevelOne, lines=[""])
        expected = None

        # when
        result = self.parser.parse_header_level_one(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_one_return_none_for_invalid_block(self):
        """
        Test that parse_header_level_one returns None in case a block of type
        different than HeaderLevelOne is given.
        """

        # given
        input_block = Block(type=Paragraph, lines=["Ű** !HAŁAS !** !đ"])
        expected = None

        # when
        result = self.parser.parse_header_level_one(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_one_for_valid_block(self):
        """
        Test that parse_header_level_one converts a valid block of type HeaderLevelOne
        to the Header instance.
        """

        # given
        input_block = Block(type=HeaderLevelOne, lines=["Ű** !HAŁAS !** !đ"])
        expected = Header("** HAŁAS **", level=1)

        # when
        result = self.parser.parse_header_level_one(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_two_return_none_for_no_header(self):
        """
        Test that parse_header_level_two returns None in case a block of type HeaderLevelTwo
        with an empty collection is given.
        """

        # given
        input_block = Block(type=HeaderLevelTwo, lines=[])
        expected = None

        # when
        result = self.parser.parse_header_level_two(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_two_return_none_for_header_not_matching_pattern(self):
        """
        Test that parse_header_level_two returns None in case a block of type HeaderLevelTwo
        with contents not matching header pattern is given.
        """

        # given
        input_block = Block(type=HeaderLevelTwo, lines=["Űđ"])
        expected = None

        # when
        result = self.parser.parse_header_level_two(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_two_return_none_for_empty_header(self):
        """
        Test that parse_header_level_two returns None in case a block of type HeaderLevelTwo
        with no contents is given.
        """

        # given
        input_block = Block(type=HeaderLevelTwo, lines=[""])
        expected = None

        # when
        result = self.parser.parse_header_level_two(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_two_return_none_for_invalid_block(self):
        """
        Test that parse_header_level_two returns None in case a block of type
        different than HeaderLevelTwo is given.
        """

        # given
        input_block = Block(type=Paragraph, lines=["Ű** !HAŁAS !** !đ"])
        expected = None

        # when
        result = self.parser.parse_header_level_two(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_two_for_valid_block(self):
        """
        Test that parse_header_level_two converts a valid block of type HeaderLevelTwo
        to the Header instance.
        """

        # given
        input_block = Block(type=HeaderLevelTwo, lines=["Ű** !HAŁAS !** !đ"])
        expected = Header("** HAŁAS **", level=2)

        # when
        result = self.parser.parse_header_level_two(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_three_return_none_for_no_header(self):
        """
        Test that parse_header_level_three returns None in case a block of type HeaderLevelThree
        with an empty collection is given.
        """

        # given
        input_block = Block(type=HeaderLevelThree, lines=[])
        expected = None

        # when
        result = self.parser.parse_header_level_three(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_three_return_none_for_header_not_matching_pattern(self):
        """
        Test that parse_header_level_three returns None in case a block of type HeaderLevelThree
        with contents not matching header pattern is given.
        """

        # given
        input_block = Block(type=HeaderLevelThree, lines=["Ű !HAŁAS !** !đ"])
        expected = None

        # when
        result = self.parser.parse_header_level_three(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_three_return_none_for_empty_header(self):
        """
        Test that parse_header_level_three returns None in case a block of type HeaderLevelThree
        with no contents is given.
        """

        # given
        input_block = Block(type=HeaderLevelThree, lines=[""])
        expected = None

        # when
        result = self.parser.parse_header_level_three(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_three_return_none_for_invalid_block(self):
        """
        Test that parse_header_level_three returns None in case a block of type
        different than HeaderLevelThree is given.
        """

        # given
        input_block = Block(type=Paragraph, lines=["Ű** !HAŁAS !** !đ"])
        expected = None

        # when
        result = self.parser.parse_header_level_three(input_block)

        # then
        compare(expected, result)

    def should_parse_header_level_three_for_valid_block(self):
        """
        Test that parse_header_level_three converts a valid block of type HeaderLevelThree
        to the Header instance.
        """

        # given
        input_block = Block(type=HeaderLevelThree, lines=["Ű** !HAŁAS !** !đ"])
        expected = Header("HAŁAS", level=3)

        # when
        result = self.parser.parse_header_level_three(input_block)

        # then
        compare(expected, result)

    def should_parse_paragraph_return_none_for_no_paragraph(self):
        """
        Test that parse_paragraph returns None in case no paragraph lines are given.
        """
        # given
        input_block = Block(type=Paragraph, lines=[])
        expected = None

        # when
        result = self.parser.parse_paragraph(input_block)

        # then
        compare(expected, result)

    def should_parse_paragraph_return_none_for_empty_paragraph(self):
        """
        Test that parse_paragraph returns None in case an empty paragraph line is given.
        """
        # given
        input_block = Block(type=Paragraph, lines=[""])
        expected = None

        # when
        result = self.parser.parse_paragraph(input_block)

        # then
        compare(expected, result)

    def should_parse_paragraph_return_none_for_invalid_block(self):
        """
        Test that parse_paragraph returns None in case a block of different type
        than Paragraph is given.
        """
        # given
        input_block = Block(type=Header, lines=["Ű** !HAŁAS !** !đ"])
        expected = None

        # when
        result = self.parser.parse_paragraph(input_block)

        # then
        compare(expected, result)

    def should_parse_paragraph_for_valid_block(self):
        """
        Test that parse_paragraph converts a Paragraph block with a few lines
        to a Paragraph instance containing single Text instance,
        which consists of Text instances per each sentence (see _is_new_line),
        each with invalid characters removed (see _extract_text).
        """
        # given
        input_paragraph = Block(type=Paragraph,
                                lines=["Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości"
                                       " !dopuszczalnej. (–",
                                       "------------------------------",
                                       " !"])
        expected = Paragraph(Text([Text(["Szczytowy poziom dźwięku C nie przekracza wartości"
                                         " dopuszczalnej."]),
                                   ' ',
                                   Text(["------------------------------"])]))

        # when
        result = self.parser.parse_paragraph(input_paragraph)

        # then
        compare(expected, result)

    def should_parse_paragraph_correctly_extract_superscripts(self):
        """
        Test that parse_paragraph converts a Paragraph block with a few lines
        to a Paragraph instance containing single Text instance,
        which consists of Text instances per each sentence (see _is_new_line),
        each with invalid characters removed (see _extract_text).

        In addition, superscript character sequences are wrapped in a single Text instance
        with parameter `superscript` set True.
        """
        # given
        input_paragraph = Block(type=Paragraph,
                                lines=[
                                    "Rozszerzona !niepewność !złożona !(poziom !ufności "
                                    "!95%, !k=2)",
                                    "dla !ekspozycji !dziennej ![m/s^2ř]"
                                    "..............................:±0.16 $"])
        expected = Paragraph(Text([Text(["Rozszerzona niepewność złożona (poziom ufności "
                                         "95%, k=2)"]),
                                   " ",
                                   Text(["dla ekspozycji dziennej [m/s",
                                         Text(["2"], superscript=True),
                                         "]..............................:±0.16"])]))

        # when
        result = self.parser.parse_paragraph(input_paragraph)

        # then
        compare(expected, result)

    def should_parse_paragraph_create_new_line_for_chars_newline(self):
        """
        Test that parse_paragraph converts a Paragraph block with a few lines
        to a Paragraph instance containing single Text instance,
        which consists of Text instances per each sentence (see _is_new_line),
        each with invalid characters removed (see _extract_text).

        In addition, for each character from CHARS_NEWLINE tuple a new line
        (i.e. a line separator followed by a new Text instance) is created.
        """
        # given
        input_paragraph = Block(type=Paragraph,
                                lines=["Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości"
                                       " !dopuszczalnej. (–",
                                       "*szczytowy !poziom !",
                                       " !"])
        expected = Paragraph(Text([Text(["Szczytowy poziom dźwięku C nie przekracza wartości"
                                         " dopuszczalnej."]),
                                   LS,
                                   Text(["*szczytowy poziom"])]))

        # when
        result = self.parser.parse_paragraph(input_paragraph)

        # then
        compare(expected, result)

    def should_parse_paragraph_not_create_new_line_for_lowercase_letter(self):
        """
        Test that parse_paragraph converts a Paragraph block with a few lines
        to a Paragraph instance containing single Text instance,
        which consists of Text instances per each sentence (see _is_new_line),
        each with invalid characters removed (see _extract_text).

        A paragraph line starting with a lowercase character should never create a new line.
        """
        # given
        input_paragraph = Block(type=Paragraph,
                                lines=["Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości"
                                       " !dopuszczalnej. (–",
                                       "szczytowy !poziom !",
                                       " !"])
        expected = Paragraph(Text([Text(["Szczytowy poziom dźwięku C nie przekracza wartości"
                                         " dopuszczalnej."]),
                                   ' ',
                                   Text(["szczytowy poziom"])]))

        # when
        result = self.parser.parse_paragraph(input_paragraph)

        # then
        compare(expected, result)

    def should_parse_paragraph_not_create_new_line_for_uppercase_non_polish_character(self):
        """
        Test that parse_paragraph converts a Paragraph block with a few lines
        to a Paragraph instance containing single Text instance,
        which consists of Text instances per each sentence (see _is_new_line),
        each with invalid characters removed (see _extract_text).

        A paragraph line starting with an uppercase non-polish character should create a new line.
        """
        # given
        input_paragraph = Block(type=Paragraph,
                                lines=["Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości"
                                       " !dopuszczalnej. (–",
                                       "Vszczytowy !poziom !",
                                       " !"])
        expected = Paragraph(Text([Text(["Szczytowy poziom dźwięku C nie przekracza wartości"
                                         " dopuszczalnej."]),
                                   ' ',
                                   Text(["Vszczytowy poziom"])]))

        # when
        result = self.parser.parse_paragraph(input_paragraph)

        # then
        compare(expected, result)

    def should_parse_paragraph_create_new_line_for_uppercase_and_polish_characters(self):
        """
        Test that parse_paragraph converts a Paragraph block with a few lines
        to a Paragraph instance containing single Text instance,
        which consists of Text instances per each sentence (see _is_new_line),
        each with invalid characters removed (see _extract_text).

        A paragraph line starting with an uppercase polish character
        followed by a polish character should create a new line.
        """
        # given
        input_paragraph = Block(type=Paragraph,
                                lines=["Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości"
                                       " !dopuszczalnej. (–",
                                       "Łszczytowy !poziom !",
                                       " !"])
        expected = Paragraph(Text([Text(["Szczytowy poziom dźwięku C nie przekracza wartości"
                                         " dopuszczalnej."]),
                                   LS,
                                   Text(["Łszczytowy poziom"])]))

        # when
        result = self.parser.parse_paragraph(input_paragraph)

        # then
        compare(expected, result)

    def should_parse_paragraph_create_new_line_for_pronoun_and_polish_characters(self):
        """
        Test that parse_paragraph converts a Paragraph block with a few lines
        to a Paragraph instance containing single Text instance,
        which consists of Text instances per each sentence (see _is_new_line),
        each with invalid characters removed (see _extract_text).

        A paragraph line starting with a single uppercase polish character
        followed by space and a polish character should create a new line.
        """
        # given
        input_paragraph = Block(type=Paragraph,
                                lines=["Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości"
                                       " !dopuszczalnej. (–",
                                       "W !szczytowy !poziom !",
                                       " !"])
        expected = Paragraph(Text([Text(["Szczytowy poziom dźwięku C nie przekracza wartości"
                                         " dopuszczalnej."]),
                                   LS,
                                   Text(["W szczytowy poziom"])]))

        # when
        result = self.parser.parse_paragraph(input_paragraph)

        # then
        compare(expected, result)

    def should_parse_paragraph_not_create_new_line_for_pronoun_and_non_polish_characters(self):
        """
        Test that parse_paragraph converts a Paragraph block with a few lines
        to a Paragraph instance containing single Text instance,
        which consists of Text instances per each sentence (see _is_new_line),
        each with invalid characters removed (see _extract_text).

        A paragraph line starting with a single uppercase polish character
        followed by space and a non-polish character should never create a new line.
        """
        # given
        input_paragraph = Block(type=Paragraph,
                                lines=["Szczytowy !poziom !dźwięku !C !nie !przekracza !wartości"
                                       " !dopuszczalnej. (–",
                                       "W !xszczytowy !poziom !",
                                       " !"])
        expected = Paragraph(Text([Text(["Szczytowy poziom dźwięku C nie przekracza wartości"
                                         " dopuszczalnej."]),
                                   ' ',
                                   Text(["W xszczytowy poziom"])]))

        # when
        result = self.parser.parse_paragraph(input_paragraph)

        # then
        compare(expected, result)

    def should_parse_table_return_none_for_no_table(self):
        """
        Test that parse_table returns None in case no table lines are given.
        """
        # given
        input_block = Block(type=Table, lines=[])
        expected = None

        # when
        result = self.parser.parse_table(input_block)

        # then
        compare(expected, result)

    def should_parse_table_return_none_for_empty_table(self):
        """
        Test that parse_table returns None in case an empty table line is given.
        """
        # given
        input_block = Block(type=Table, lines=[""])
        expected = None

        # when
        result = self.parser.parse_table(input_block)

        # then
        compare(expected, result)

    def should_parse_table_return_none_for_invalid_block(self):
        """
        Test that parse_table returns None in case a block of different type
        than Table is given.
        """
        # given
        input_block = Block(type=Header, lines=["Ű** !HAŁAS !** !đ"])
        expected = None

        # when
        result = self.parser.parse_table(input_block)

        # then
        compare(expected, result)

    def should_parse_table_for_valid_block(self):
        """
        Test that parse_table converts a Table block with a few lines
        to a Table instance containing TableCells grouped in rows after applying
        _split_to_cells and _merge_rows methods. Again, text will be cleared from
        invalid characters (see _strip and _fix_spaces), as well as super-/ and subscripts
        will be extracted.
        """
        # given
        input_table = Block(type=Table,
                            lines=["CÉ$ÔÉLÔÉ'ÔÉ(ÔÉ&ÔÉ(ÔÉ(ÔÉ'ÔÉ,ÔÉ)Ë",
                                   "ĚL.P.Ě ,Wykonywane !zadanie .ĚCzas #ĚCzas $ĚWynikiĚ !Równo- !Ě "
                                   "!Maksym.ĚSzczyt.Ě !Poz.ekspoz.ĚKrotność !Ě",
                                   'Ě $Ě LĚtrwaniaĚtrwania !Ějedno-Ě !ważny "Ě !poziom !Ěpoziom !Ě '
                                   '!na !hałas #Ěwartości !Ě',
                                   'Ě $Ě LĚzadaniaĚpomiarówĚstkoweĚ !poziom !Ě !dźwiękuĚdźwiękuĚ '
                                   '!dla !8 !godz.Ědopuszcz.Ě',
                                   'Ě $Ě LĚ "Tm #Ě (Ě &Ě !dźwiękuĚ $A #Ě #C #Ě #LőEX,8hů #Ě '
                                   '!LőEX,8hů "Ě',
                                   'Ě $Ě LĚ ![min] !Ě ![min] "Ě ![dB] !Ě !A ![dB] !Ě "[dB] "Ě '
                                   '"[dB] !Ě $[dB] $Ě )Ě',
                                   "ÍÉ$ÎÉLÎÉ'ÎÉ(ÎÉ&ÎÉ(ÎÉ(ÎÉ'ÎÉ,ÎÉ)D",
                                   'Ě !1 "Ě !Obsługa !stanowisk !linii !zrobotyzowanej %Ě !450 #Ě '
                                   '!15 %Ě !79.4 !Ě !78.8 #Ě "87 $Ě !111 #Ě $78.5 $Ě "0.22 #Ě',
                                   "Ě $Ě !w !sektorze !1: !zakładanie !detali !do !gniazd \"Ě #- "
                                   "#Ě (Ě !78.8 !Ě (Ě (Ě 'Ě $(+)2.1 \"Ě )Ě–",
                                   "Ě $Ě !robotów !/operacje !10-20/, !nadzorowanie %Ě 'Ě (Ě !78.1 "
                                   "!Ě (Ě (Ě 'Ě ,Ě )Ě–",
                                   "Ě $Ě !procesów !automatycznego !zgrzewania, (Ě 'Ě (Ě &Ě (Ě (Ě "
                                   "'Ě ,Ě )Ě–",
                                   "Ě $Ě !prace !pomocnicze. :Ě 'Ě (Ě &Ě (Ě (Ě 'Ě ,Ě )Ě–",
                                   "ÐÉ$ÖÉLÖÉ'ÖÉ(ÖÉ&ÖÉ(ÖÉ(ÖÉ'ÖÉ,ÖÉ)N])"])

        expected = Table([
            [TableCell(Text(["L.P."])),
             TableCell(Text(["Wykonywane zadanie"])),
             TableCell(Text(["Czas", LS, "trwania", LS, "zadania", LS, "Tm", LS, "[min]"])),
             TableCell(Text(["Czas", LS, "trwania", LS, "pomiarów", LS, LS, "[min]"])),
             TableCell(Text(["Wyniki", LS, "jedno-", LS, "stkowe", LS, LS, "[dB]"])),
             TableCell(Text(["Równo-", LS, "ważny", LS, "poziom", LS, "dźwięku", LS, "A [dB]"])),
             TableCell(Text(["Maksym.", LS, "poziom", LS, "dźwięku", LS, "A", LS, "[dB]"])),
             TableCell(Text(["Szczyt.", LS, "poziom", LS, "dźwięku", LS, "C", LS, "[dB]"])),
             TableCell(Text(["Poz.ekspoz.", LS, "na hałas", LS, "dla 8 godz.", LS, "L",
                             Text(["EX,8h"], subscript=True),
                             LS, "[dB]"])),
             TableCell(Text(["Krotność", LS, "wartości", LS, "dopuszcz.", LS, "L",
                             Text(["EX,8h"], subscript=True)]))],
            [TableCell(Text(["1"])),
             TableCell(Text(["Obsługa stanowisk linii zrobotyzowanej", LS,
                             "w sektorze 1: zakładanie detali do gniazd", LS,
                             "robotów /operacje 10-20/, nadzorowanie", LS,
                             "procesów automatycznego zgrzewania,", LS,
                             "prace pomocnicze."])),
             TableCell(Text(["450", LS, "-"])),
             TableCell(Text(["15"])),
             TableCell(Text(["79.4", LS, "78.8", LS, "78.1"])),
             TableCell(Text(["78.8"])),
             TableCell(Text(["87"])),
             TableCell(Text(["111"])),
             TableCell(Text(["78.5", LS, "(+)2.1"])),
             TableCell(Text(["0.22"]))]
        ])

        # when
        result = self.parser.parse_table(input_table)

        # then
        compare(expected, result)

    def should_parse_table_interpret_column_sep_to_span_multiple_rows_for_vibration(self):
        """
        Test that parse_table converts a Table block with a few lines
        to a Table instance containing TableCells grouped in rows after applying
        _split_to_cells and _merge_rows methods. Again, text will be cleared from
        invalid characters (see _strip and _fix_spaces), as well as super-/ and subscripts
        will be extracted.

        In addition, in case a table contains COLUMN_SEP characters, rows should be grouped
        together by marking them with rowspan.
        """
        # given
        input_table = Block(type=Table,
                            lines=["CÉ&ÔÉZÔÉ,ÔÉ-ÔÉ+ÔÉ/Ë",
                                   "Ě !L.P. !Ě ZĚ !Czas 'Ě !Oś !układu #Ě $aőhwů $Ě !Całkowita %Ě",
                                   'Ě &Ě (O !K !O !L !I !C !Z !N !O !Ś !C !I #P !O !M !I !A !R !Ó '
                                   '!W )Ě !ekspozycji !Ě !współrzęd- "Ě +Ě !wartość !drgań !Ě',
                                   'Ě &Ě ZĚ ![min] &Ě !nych (Ě #[m/s^2ř] "Ě #[m/s^2ř] &Ě',
                                   'ÍÉ&ÎÉZÎÉ,ÎÉ-ÎÉ+ÎÉ/D',
                                   'Ě "1 #Ě !Obróbka !blacharska !- !zacieranie !i !szlifowanie ,Ě '
                                   '#40 \'Ě &X &Ě "2.12 %Ě $6.5 (Ě',
                                   "Ě &Ě !obrabianych !elementów !szlifierką !pneumatyczną ,Ě "
                                   ",ÍÉ-ÎÉ+D /Ě",
                                   'Ě &Ě !oscylacyjną !DYNABRADE. CĚ ,Ě &Y &Ě "1.66 %Ě /Ě',
                                   "Ě &Ě ZĚ ,ÍÉ-ÎÉ+D /Ě",
                                   'Ě &Ě ZĚ ,Ě &Z &Ě "5.94 %Ě /Ě',
                                   "ÐÉ&ÖÉZÖÉ,ÖÉ-ÖÉ+ÖÉ/N"])

        expected = Table([
            [TableCell(Text(["L.P."])),
             TableCell(Text(["O K O L I C Z N O Ś C I P O M I A R Ó W"])),
             TableCell(Text(["Czas", LS, "ekspozycji", LS, "[min]"])),
             TableCell(Text(["Oś układu", LS, "współrzęd-", LS, "nych"])),
             TableCell(Text(
                 ["a", Text(["hw"], subscript=True), LS, LS, "[m/s", Text(["2"], superscript=True),
                  "]"])),
             TableCell(Text(
                 ["Całkowita", LS, "wartość drgań", LS, "[m/s", Text(["2"], superscript=True),
                  "]"]))],
            [TableCell(Text(["1"]), rowspan=3),
             TableCell(Text(["Obróbka blacharska - zacieranie i szlifowanie", LS,
                             "obrabianych elementów szlifierką pneumatyczną", LS,
                             "oscylacyjną DYNABRADE."]), rowspan=3),
             TableCell(Text(["40"]), rowspan=3),
             TableCell(Text(["X"])),
             TableCell(Text(["2.12"])),
             TableCell(Text(["6.5"]), rowspan=3)],
            [TableCell(Text(["Y"])),
             TableCell(Text(["1.66"]))],
            [TableCell(Text(["Z"])),
             TableCell(Text(["5.94"]))]
        ])

        # when
        result = self.parser.parse_table(input_table)

        # then
        compare(expected, result)

    def should_parse_table_interpret_column_sep_to_span_multiple_rows_for_noise(self):
        """
        Test that parse_table converts a Table block with a few lines
        to a Table instance containing TableCells grouped in rows after applying
        _split_to_cells and _merge_rows methods. Again, text will be cleared from
        invalid characters (see _strip and _fix_spaces), as well as super-/ and subscripts
        will be extracted.

        In addition, in case a table contains COLUMN_SEP characters, rows should be grouped
        together by marking them with rowspan.
        """
        # given
        input_table = Block(type=Table,
                            lines=["ČÉ$ÔÉLÔÉ'ÔÉ(ÔÉ&ÔÉ(ÔÉ(ÔÉ'ÔÉ,ÔÉ)Ë",
                                   "ĚL.P.Ě ,Wykonywane !zadanie .ĚCzas #ĚCzas $ĚWynikiĚ !Równo- !Ě"
                                   " !Maksym.ĚSzczyt.Ě !Poz.ekspoz.ĚKrotność !Ě",
                                   'Ě $Ě LĚtrwaniaĚtrwania !Ějedno-Ě !ważny "Ě !poziom !Ěpoziom !Ě'
                                   ' !na !hałas #Ěwartości !Ě',
                                   "Ě $Ě LĚzadaniaĚpomiarówĚstkoweĚ !poziom !Ě !dźwiękuĚdźwiękuĚ"
                                   " !dla !8 !godz.Ědopuszcz.Ě",
                                   'Ě $Ě LĚ "Tm #Ě (Ě &Ě !dźwiękuĚ $A #Ě #C #Ě #LőEX,8hů #Ě'
                                   ' !LőEX,8hů "Ě',
                                   'Ě $Ě LĚ ![min] !Ě ![min] "Ě ![dB] !Ě !A ![dB] !Ě "[dB] "Ě'
                                   ' "[dB] !Ě $[dB] $Ě )Ě',
                                   "ÍÉ$ÎÉLÎÉ'ÎÉ(ÎÉ&ÎÉ(ÎÉ(ÎÉ'ÎÉ,ÎÉ)Ď",
                                   'Ě !1 "Ě !Prace !w !Warsztacie !Utrzymania !Ruchu: \'Ě !300 #Ě'
                                   ' !15 %Ě !79.1 !Ě !78.8 #Ě "85 $Ě !105 #Ě $84.1 $Ě "0.81 #Ě',
                                   'Ě $Ě "- !przygotowanie !materiałów !i !narzędzi, $Ě #- #Ě (Ě'
                                   ' !78.3 !Ě (Ě (Ě \'Ě $(+)1.3 "Ě )Ě–',
                                   'Ě $Ě $drobne !naprawy !mechaniczne !podzespołów, !Ě \'Ě (Ě'
                                   ' !79.0 !Ě (Ě (Ě \'Ě ,Ě )Ě–',
                                   "Ě $Ě $ręczna !obróbka !ślusarska, !wiercenie %Ě 'Ě (Ě &Ě (Ě (Ě"
                                   " 'Ě ,Ě )Ě–",
                                   "Ě $Ě $otworów !na !wiertarce !kolumnowej; (Ě 'Ě (Ě &Ě (Ě (Ě"
                                   " 'Ě ,Ě )Ě–",
                                   "Ě $Ě $prace !pomocnicze, !prace !montażowe 'Ě 'Ě (Ě &Ě (Ě (Ě"
                                   " 'Ě ,Ě )Ě–",
                                   "Ě $Ě $w !liniach !i !gniazdach !jednostki !prod. #Ě 'Ě (Ě &Ě"
                                   " (Ě (Ě 'Ě ,Ě )Ě–",
                                   "Ě $Ě $Spawalnia !1, <Ě 'Ě (Ě &Ě (Ě (Ě 'Ě ,Ě )Ě–",
                                   "ÍÉ$ÎÉLÎÉ'ÎÉ(ÎÉ&ÎÉ(ÎÉ(ÎÉ'Ď ,Ě )Ě",
                                   'Ě !2 "Ě "- !cięcie !i !szlifowanie !elementów *Ě !60 $Ě !10'
                                   ' %Ě !90.2 !Ě !90.2 #Ě "100 #Ě !111 #Ě ,Ě )Ě',
                                   "Ě $Ě $konstrukcyjnych !(kształtowników, (Ě #- #Ě (Ě !89.5 !Ě"
                                   " (Ě (Ě 'Ě ,Ě )Ě–",
                                   "Ě $Ě $profili) !szlifierką !elektryczną !kątową \"Ě 'Ě (Ě"
                                   " !90.7 !Ě (Ě (Ě 'Ě ,Ě )Ě–",
                                   "Ě $Ě $MAKITA !9565 !H, :Ě 'Ě (Ě &Ě (Ě (Ě 'Ě ,Ě )Ě–",
                                   "ÍÉ$ÎÉLÎÉ'ÎÉ(ÎÉ&ÎÉ(ÎÉ(ÎÉ'Ď ,Ě )Ě",
                                   'Ě !3 "Ě "- !spawanie !profili !w !osłonie !gazów !Ar+CO2 !Ě'
                                   ' !90 $Ě !15 %Ě !86.8 !Ě !86.2 #Ě "93 $Ě !123 #Ě ,Ě )Ě',
                                   "Ě $Ě $(spawarka !MIG !C250). 4Ě #- #Ě (Ě !85.6 !Ě (Ě (Ě 'Ě ,Ě"
                                   " )Ě–",
                                   "Ě $Ě LĚ 'Ě (Ě !86.0 !Ě (Ě (Ě 'Ě ,Ě )Ě–",
                                   "ĐÉ$ÖÉLÖÉ'ÖÉ(ÖÉ&ÖÉ(ÖÉ(ÖÉ'ÖÉ,ÖÉ)Ň"])

        expected = Table([
            [TableCell(Text(["L.P."])),
             TableCell(Text(["Wykonywane zadanie"])),
             TableCell(Text(["Czas", LS, "trwania", LS, "zadania", LS, "Tm", LS, "[min]"])),
             TableCell(Text(["Czas", LS, "trwania", LS, "pomiarów", LS, LS, "[min]"])),
             TableCell(Text(["Wyniki", LS, "jedno-", LS, "stkowe", LS, LS, "[dB]"])),
             TableCell(Text(["Równo-", LS, "ważny", LS, "poziom", LS, "dźwięku", LS, "A [dB]"])),
             TableCell(Text(["Maksym.", LS, "poziom", LS, "dźwięku", LS, "A", LS, "[dB]"])),
             TableCell(Text(["Szczyt.", LS, "poziom", LS, "dźwięku", LS, "C", LS, "[dB]"])),
             TableCell(Text(["Poz.ekspoz.", LS, "na hałas", LS, "dla 8 godz.", LS, "L",
                             Text(["EX,8h"], subscript=True),
                             LS, "[dB]"])),
             TableCell(Text(["Krotność", LS, "wartości", LS, "dopuszcz.", LS, "L",
                             Text(["EX,8h"], subscript=True)]))],
            [TableCell(Text(["1"])),
             TableCell(Text(["Prace w Warsztacie Utrzymania Ruchu:", LS,
                             "- przygotowanie materiałów i narzędzi,", LS,
                             "drobne naprawy mechaniczne podzespołów,", LS,
                             "ręczna obróbka ślusarska, wiercenie", LS,
                             "otworów na wiertarce kolumnowej;", LS,
                             "prace pomocnicze, prace montażowe", LS,
                             "w liniach i gniazdach jednostki prod.", LS,
                             "Spawalnia 1,"])),
             TableCell(Text(["300", LS, "-"])),
             TableCell(Text(["15"])),
             TableCell(Text(["79.1", LS, "78.3", LS, "79.0"])),
             TableCell(Text(["78.8"])),
             TableCell(Text(["85"])),
             TableCell(Text(["105"])),
             TableCell(Text(["84.1", LS, "(+)1.3"]), rowspan=3),
             TableCell(Text(["0.81"]), rowspan=3)],
            [TableCell(Text(["2"])),
             TableCell(Text(["- cięcie i szlifowanie elementów", LS,
                             "konstrukcyjnych (kształtowników,", LS,
                             "profili) szlifierką elektryczną kątową", LS,
                             "MAKITA 9565 H,"])),
             TableCell(Text(["60", LS, "-"])),
             TableCell(Text(["10"])),
             TableCell(Text(["90.2", LS, "89.5", LS, "90.7"])),
             TableCell(Text(["90.2"])),
             TableCell(Text(["100"])),
             TableCell(Text(["111"]))],
            [TableCell(Text(["3"])),
             TableCell(Text(["- spawanie profili w osłonie gazów Ar+CO2", LS,
                             "(spawarka MIG C250)."])),
             TableCell(Text(["90", LS, "-"])),
             TableCell(Text(["15"])),
             TableCell(Text(["86.8", LS, "85.6", LS, "86.0"])),
             TableCell(Text(["86.2"])),
             TableCell(Text(["93"])),
             TableCell(Text(["123"]))]
        ])

        # when
        result = self.parser.parse_table(input_table)

        # then
        compare(expected, result)

    def should_parse_table_interpret_column_sep_to_span_multiple_rows_for_dust(self):
        """
        Test that parse_table converts a Table block with a few lines
        to a Table instance containing TableCells grouped in rows after applying
        _split_to_cells and _merge_rows methods. Again, text will be cleared from
        invalid characters (see _strip and _fix_spaces), as well as super-/ and subscripts
        will be extracted.

        In addition, in case a table contains COLUMN_SEP characters, rows should be grouped
        together by marking them with rowspan.
        """
        # given
        input_table = Block(type=Table,
                            lines=["ČÉ6ÔÉHÔÉ0ÔÉ)ÔÉ0ÔÉ.Ë",
                                   "Ě #Nazwa !czynnika, $Ě 'Okoliczności !poboru !próbek 'Ě "
                                   "!Numery !próbek \"Ě !StężeniaĚ #Stężenie %Ě #Cch/NDSch \"Ě",
                                   "Ě #stężenia !dopuszcz. !Ě HĚ 0Ě !jedn. #Ě #chwilowe !Cch !Ě .Ě",
                                   "Ě 6Ě HĚ 0Ě )Ě 0Ě .Ě",
                                   "Ě 6Ě HĚ 0Ě ![mg/m^3ř] !Ě $[mg/m^3ř] %Ě .Ě",
                                   "ÍÉ6ÎÉHÎÉ0ÎÉ)ÎÉ0ÎÉ.Ď",
                                   "Ě !Tetrachloroeten &Ě !Załadunek !skrzynek !z !detalami *Ě "
                                   "&002/rs $Ě !109.8 #Ě \"Cch=109.8 %Ě #0.65 'Ě",
                                   "Ě !(czterochloroetylen, !Ě !do !myjki, !nadzorowanie !procesu "
                                   ")Ě 0Ě )Ě \"±24.4 )Ě .Ě",
                                   "Ě !perchloroetylen) %Ě !odtłuszczania, !wyładunek !po ,Ě 0Ě )Ě "
                                   "0Ě .Ě",
                                   "Ě !NDSch !: !170 !mg/mô3 #Ě !odtłuszczeniu. 9Ě 0Ě )Ě 0Ě .Ě",
                                   "Ě 6ÍÉHÎÉ0ÎÉ)ÎÉ0ÎÉ.Ď",
                                   "Ě 6Ě 1j.w. 3Ě &003/rs $Ě !118.4 #Ě \"Cch=118.4 %Ě #0.70 'Ě",
                                   "Ě 6Ě HĚ 0Ě )Ě \"±26.3 )Ě .Ě",
                                   "ĐÉ6ÖÉHÖÉ0ÖÉ)ÖÉ0ÖÉ.Ň])"])

        expected = Table([
            [TableCell(Text(["Nazwa czynnika,", LS, "stężenia dopuszcz."])),
             TableCell(Text(["Okoliczności poboru próbek"])),
             TableCell(Text(["Numery próbek"])),
             TableCell(Text(["Stężenia", LS, "jedn.", LS, LS, "[mg/m",
                             Text(["3"], superscript=True), "]"])),
             TableCell(Text(["Stężenie", LS, "chwilowe Cch", LS, LS, "[mg/m",
                             Text(["3"], superscript=True), "]"])),
             TableCell(Text(["Cch/NDSch"]))],
            [TableCell(Text(["Tetrachloroeten", LS, "(czterochloroetylen,", LS, "perchloroetylen)",
                             LS, "NDSch : 170 mg/m", Text(["3"], superscript=True)]), rowspan=2),
             TableCell(Text(["Załadunek skrzynek z detalami", LS, "do myjki, nadzorowanie procesu",
                             LS, "odtłuszczania, wyładunek po", LS, "odtłuszczeniu."])),
             TableCell(Text(["002/rs"])),
             TableCell(Text(["109.8"])),
             TableCell(Text(["Cch=109.8", LS, "±24.4"])),
             TableCell(Text(["0.65"]))],
            [TableCell(Text(["j.w."])),
             TableCell(Text(["003/rs"])),
             TableCell(Text(["118.4"])),
             TableCell(Text(["Cch=118.4", LS, "±26.3"])),
             TableCell(Text(["0.70"]))]
        ])

        # when
        result = self.parser.parse_table(input_table)

        # then
        compare(expected, result)

    def should_parse_table_ignore_table_continuation_characters(self):
        """
        Test that parse_table converts a Table block with a few lines
        to a Table instance containing TableCells grouped in rows after applying
        _split_to_cells and _merge_rows methods. Again, text will be cleared from
        invalid characters (see _strip and _fix_spaces), as well as super-/ and subscripts
        will be extracted.

        In addition, lines containing table continuation character sequence should be ignored.
        """
        # given
        input_table = Block(type=Table,
                            lines=["ČÉ6ÔÉHÔÉ0ÔÉ)ÔÉ0ÔÉ.Ë",
                                   "Ě #Nazwa !czynnika, $Ě 'Okoliczności !poboru !próbek 'Ě "
                                   "!Numery !próbek \"Ě !StężeniaĚ #Wskaźnik(i) \"Ě #Cw/NDS %Ě",
                                   "Ě #metoda !pomiaru, $Ě HĚ 0Ě !jedn. #Ě #narażenia $Ě #lub (Ě",
                                   "Ě #stężenia !dopuszcz.,Ě HĚ 0Ě )Ě $oraz !Xg %Ě #Xg/NDS %Ě",
                                   "Ě #uwagi .Ě HĚ 0Ě )Ě $lub !Xgw %Ě .Ě",
                                   "Ě 6Ě HĚ 0Ě ![mg/m^3ř] !Ě $[mg/m^3ř] %Ě .Ě",
                                   "ÍÉ6ÎÉHÎÉ0ÎÉ)ÎÉ0ÎÉ.Ď",
                                   "Ě !Tlenki !żelaza (Ě 2j.w. 2Ě &004/s %Ě !0.18 $Ě #Cw=0.18 &Ě "
                                   "#0.04 'Ě",
                                   "Ě !- !dymy /Ě !Czas !ekspozycji ![min]..: !480 +Ě 0Ě )Ě #±0.04 "
                                   "(Ě .Ě",
                                   "Ě !- !w !przelicz. !na !Fe \"Ě HĚ 0Ě )Ě 0Ě .Ě",
                                   "Ě !-met. !dozymetryczna- !Ě HĚ 0Ě )Ě 0Ě .Ě–",
                                   "Ě !NDS !: !5 !mg/mô3 'Ě HĚ 0Ě )Ě 0Ě .Ě–",
                                   "ÍÉ6ÎÉHÎÉ0ÎÉ)ÎÉ0ÎÉ.Ď",
                                   "                                     - 2 -",
                                   "",
                                   "003/TSE/12",
                                   "ČÉ6ÔÉHÔÉ0ÔÉ)ÔÉ0ÔÉ.Ë",
                                   "Ě #Nazwa !czynnika, $Ě 'Okoliczności !poboru !próbek 'Ě "
                                   "!Numery !próbek \"Ě !StężeniaĚ #Wskaźnik(i) \"Ě #Cw/NDS %Ě",
                                   "Ě #metoda !pomiaru, $Ě HĚ 0Ě !jedn. #Ě #narażenia $Ě #lub (Ě",
                                   "Ě #stężenia !dopuszcz.,Ě HĚ 0Ě )Ě $oraz !Xg %Ě #Xg/NDS %Ě",
                                   "Ě #uwagi .Ě HĚ 0Ě )Ě $lub !Xgw %Ě .Ě",
                                   "Ě 6Ě HĚ 0Ě ![mg/m^3ř] !Ě $[mg/m^3ř] %Ě .Ě",
                                   "ÍÉ6ÎÉHÎÉ0ÎÉ)ÎÉ0ÎÉ.Ď",
                                   "Ě !Ditlenek !azotu 'Ě !Montaż !zaworów !tlenowych !w !HALI (Ě "
                                   "0Ě !< !0.19 \"Ě #Xgw= \"- &Ě %- (Ě",
                                   "Ě !-met. !stacjonarna- #Ě !SPRĘŻAREK: !cięcie !elementów !rur "
                                   "(Ě 0Ě !< !0.19 \"Ě 0Ě .Ě",
                                   "Ě !NDS !: !0.7 !mg/mô3 %Ě !przyłączeniowych !szlifierką "
                                   "!kątową, $Ě 0Ě !< !0.19 \"Ě #DGw= \"- & Ě .Ě",
                                   "Ě 6Ě !szlifowanie, !fazowanie !krawędzi !rur $Ě 0Ě !< !0.19 "
                                   "\"Ě #GGw= \"- &Ě .Ě–",
                                   "Ě 6Ě !szlifierką !kątową, !składanie !elementów !Ě 0Ě !< !0.19 "
                                   "\"Ě 0Ě .Ě–",
                                   "Ě 6Ě !zaworów, !skręcanie, !spawanie !elementów !Ě 0Ě !< !0.19 "
                                   "\"Ě 0Ě .Ě–",
                                   "Ě 6Ě !ze !stali !chromoniklowej !(316 !L) !metodą !Ě 0Ě )Ě 0Ě "
                                   ".Ě–",
                                   "Ě 6Ě !TIG !w !osłonie !argonu, !prace ,Ě 0Ě )Ě 0Ě .Ě–",
                                   "Ě 6Ě !pomocnicze. <Ě 0Ě )Ě 0Ě .Ě–",
                                   "Ě 6Ě !Czas !ekspozycji ![min]..: !390 +Ě 0Ě )Ě 0Ě .Ě–",
                                   "ÍÉ6ÎÉHÎÉ0ÎÉ)ÎÉ0ÎÉ.Ď"])

        expected = Table([
            [TableCell(Text(["Nazwa czynnika,", LS, "metoda pomiaru,", LS, "stężenia dopuszcz.,",
                             LS, "uwagi"])),
             TableCell(Text(["Okoliczności poboru próbek"])),
             TableCell(Text(["Numery próbek"])),
             TableCell(Text(["Stężenia", LS, "jedn.", LS, LS, LS, "[mg/m",
                             Text(["3"], superscript=True), "]"])),
             TableCell(Text(["Wskaźnik(i)", LS, "narażenia", LS, "oraz Xg", LS, "lub Xgw", LS,
                             "[mg/m", Text(["3"], superscript=True), "]"])),
             TableCell(Text(["Cw/NDS", LS, "lub", LS, "Xg/NDS"]))],
            [TableCell(Text(["Tlenki żelaza", LS, "- dymy", LS, "- w przelicz. na Fe", LS,
                             "-met. dozymetryczna-", LS, "NDS : 5 mg/m",
                             Text(["3"], superscript=True)])),
             TableCell(Text(["j.w.", LS, "Czas ekspozycji [min]..: 480"])),
             TableCell(Text(["004/s"])),
             TableCell(Text(["0.18"])),
             TableCell(Text(["Cw=0.18", LS, "±0.04"])),
             TableCell(Text(["0.04"]))],
            [TableCell(Text(["Ditlenek azotu", LS, "-met. stacjonarna-", LS,
                             "NDS : 0.7 mg/m", Text(["3"], superscript=True)])),
             TableCell(Text(["Montaż zaworów tlenowych w HALI", LS,
                             "SPRĘŻAREK: cięcie elementów rur", LS,
                             "przyłączeniowych szlifierką kątową,", LS,
                             "szlifowanie, fazowanie krawędzi rur", LS,
                             "szlifierką kątową, składanie elementów", LS,
                             "zaworów, skręcanie, spawanie elementów", LS,
                             "ze stali chromoniklowej (316 L) metodą", LS,
                             "TIG w osłonie argonu, prace", LS,
                             "pomocnicze.", LS, "Czas ekspozycji [min]..: 390"])),
             TableCell(Text([])),
             TableCell(Text(["< 0.19", LS, "< 0.19", LS, "< 0.19", LS, "< 0.19", LS, "< 0.19", LS,
                             "< 0.19"])),
             TableCell(Text(["Xgw= -", LS, LS, "DGw= -", LS, "GGw= -"])),
             TableCell(Text(["-"]))]
        ])

        # when
        result = self.parser.parse_table(input_table)

        # then
        compare(expected, result)

    @staticmethod
    def should_parse_table_interpret_colspan_rules_to_span_multiple_columns():
        """
        Test that parse_table converts a Table block with a few lines
        to a Table instance containing TableCells grouped in rows after applying
        _split_to_cells and _merge_rows methods. Again, text will be cleared from
        invalid characters (see _strip and _fix_spaces), as well as super-/ and subscripts
        will be extracted.

        In addition, in case a table is contained within COLSPAN_RULES,
        the corresponding columns should be grouped together by merging them.
        """
        # given
        parser = Drk2ModelParser()
        parser.last_measurement_type = Measurement.MEASUREMENT_ELECTRICAL_LIGHTING.get_value()
        input_table = Block(type=Table,
                            lines=["ČÉ$ÔÉSÔÉ,ÔÉ-ÔÉ0ÔÉ5Ë",
                                   "ĚL.P.Ě $Płaszczyzna !zadania !/ !zadanie !lub !czynność %Ě "
                                   "\"Średnie #ĚRównomiernośćĚ #Wymagania $Ě %Zgodność !z &Ě",
                                   "Ě $Ě SĚ !natężenie \"Ě \"natężenia \"Ě 0Ě %wymaganiami %Ě",
                                   "Ě $Ě SĚoświetlenia !Ě !oświetlenia !ÍÉ)ÔÉ&ÎÉ*ÔÉ*Ď",
                                   "Ě $Ě SĚ $Eőśrů %Ě $Eőminů %Ě #Eőśrů #Ě !Eőminů !Ě #Eőśrů $Ě "
                                   "\"Eőminů $Ě",
                                   "Ě $Ě SĚ ,Ě #------ $Ě )Ě------Ě *Ě !------ #Ě",
                                   "Ě $Ě SĚ #[lx] %Ě $Eőśrů &Ě \"[lx] #Ě !Eőśrů \"Ě *Ě \"Eőśrů %Ě",
                                   "ÍÉ$ÎÉSÎÉ,ÎÉ-ÎÉ)ÎÉ&ÎÉ*ÎÉ*Ď",
                                   "Ě !1 \"Ě !Stanowisko !z !wtryskarką !M-1 7Ě \"214 'Ě $0.66 %Ě "
                                   "!300 %Ě !0.6 \"Ě !niezgodneĚ !zgodne #Ě",
                                   "Ě $Ě \"- !pole !zadania: !nawijanie !drutu !powlekanego !na #Ě "
                                   "\"±12 'Ě $±0.05 $Ě )Ě &Ě #- &Ě #- &Ě–",
                                   "Ě $Ě $nawijarce, !obsługa !wtryskarki, !obsługa !praski \"Ě $- "
                                   "'Ě &- &Ě )Ě &Ě #- &Ě #- &Ě–",
                                   "Ě $Ě $mechanicznej CĚ $- 'Ě &- &Ě )Ě &Ě #- &Ě #- &Ě–",
                                   "ÍÉ$ÎÉSÎÉ,ÎÉ-ÎÉ)ÎÉ&ÎÉ*ÎÉ*Ď",
                                   "Ě !2 \"Ě !Stanowisko !z !wtryskarką !M-4 7Ě \"254 'Ě $0.97 %Ě "
                                   "!300 %Ě !0.6 \"Ě !niezgodneĚ !zgodne #Ě",
                                   "Ě $Ě \"- !pole !zadania: !nawijanie !drutu !powlekanego !na #Ě "
                                   "\"±14 'Ě $±0.08 $Ě )Ě &Ě #- &Ě #- &Ě–",
                                   "Ě $Ě $nawijarce, !obsługa !wtryskarki 2Ě $- 'Ě &- &Ě )Ě &Ě #- "
                                   "&Ě #- &Ě–",
                                   "ÍÉ$ÎÉSÎÉ,ÎÉ-ÎÉ)ÎÉ&ÎÉ*ÎÉ*Ď",
                                   "Ě !3 \"Ě !Stanowisko !z !wtryskarką !M-10 6Ě \"172 'Ě $0.75 %Ě "
                                   "!300 %Ě !0.6 \"Ě !niezgodneĚ !zgodne #Ě",
                                   "Ě $Ě \"- !pole !zadania: !obsługa !wtryskarki /Ě \"±10 'Ě "
                                   "$±0.06 $Ě )Ě &Ě #- &Ě #- &Ě–",
                                   "ĐÉ$ÖÉSÖÉ,ÖÉ-ÖÉ)ÖÉ&ÖÉ*ÖÉ*Ň"])

        expected = Table([
            [TableCell(Text(["L.P."]), rowspan=2),
             TableCell(Text(["Płaszczyzna zadania / zadanie lub czynność"]), rowspan=2),
             TableCell(Text(["Średnie", LS, "natężenie", LS, "oświetlenia", LS, "E",
                             Text(["śr"], subscript=True), LS, LS, "[lx]"]), rowspan=2),
             TableCell(Text(["Równomierność", LS, "natężenia", LS, LS, "E",
                             Text(["min"], subscript=True), LS, "------", LS, "E",
                             Text(["śr"], subscript=True)]), rowspan=2),
             TableCell(Text(["Wymagania"])),
             TableCell(Text([])),
             TableCell(Text(["Zgodność z", LS, "wymaganiami"])),
             TableCell(Text([]))],
            [
                TableCell(Text(["E", Text(["śr"], subscript=True), LS, LS, "[lx]"])),
                TableCell(Text(["E", Text(["min"], subscript=True), LS, "------", LS, "E",
                                Text(["śr"], subscript=True)])),
                TableCell(Text(["E", Text(["śr"], subscript=True)])),
                TableCell(Text(["E", Text(["min"], subscript=True), LS, "------", LS, "E",
                                Text(["śr"], subscript=True)]))],
            [TableCell(Text(["1"])),
             TableCell(Text(["Stanowisko z wtryskarką M-1", LS,
                             "- pole zadania: nawijanie drutu powlekanego na", LS,
                             "nawijarce, obsługa wtryskarki, obsługa praski", LS, "mechanicznej"])),
             TableCell(Text(["214", LS, "±12", LS, "-", LS, "-"])),
             TableCell(Text(["0.66", LS, "±0.05", LS, "-", LS, "-"])),
             TableCell(Text(["300"])),
             TableCell(Text(["0.6"])),
             TableCell(Text(["niezgodne", LS, "-", LS, "-", LS, "-"])),
             TableCell(Text(["zgodne", LS, "-", LS, "-", LS, "-"]))],
            [TableCell(Text(["2"])),
             TableCell(Text(["Stanowisko z wtryskarką M-4", LS,
                             "- pole zadania: nawijanie drutu powlekanego na", LS,
                             "nawijarce, obsługa wtryskarki"])),
             TableCell(Text(["254", LS, "±14", LS, "-"])),
             TableCell(Text(["0.97", LS, "±0.08", LS, "-"])),
             TableCell(Text(["300"])),
             TableCell(Text(["0.6"])),
             TableCell(Text(["niezgodne", LS, "-", LS, "-"])),
             TableCell(Text(["zgodne", LS, "-", LS, "-"]))],
            [TableCell(Text(["3"])),
             TableCell(Text(["Stanowisko z wtryskarką M-10", LS,
                             "- pole zadania: obsługa wtryskarki"])),
             TableCell(Text(["172", LS, "±10"])),
             TableCell(Text(["0.75", LS, "±0.06"])),
             TableCell(Text(["300"])),
             TableCell(Text(["0.6"])),
             TableCell(Text(["niezgodne", LS, "-"])),
             TableCell(Text(["zgodne", LS, "-"]))]
        ])

        # when
        result = parser.parse_table(input_table)

        # then
        compare(expected, result)


def main():
    """
    The main function to run the tests.
    """
    unittest.defaultTestLoader.testMethodPrefix = 'should'
    unittest.main()


if __name__ == '__main__':
    main()
